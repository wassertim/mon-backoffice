package service.ws

import play.api.libs.ws.WS.WSRequestHolder
import play.api.http.{ContentTypeOf, Writeable}
import play.api.libs.concurrent.Execution.Implicits._
import scala.concurrent.Future
import play.api.libs.ws.Response
import org.jsoup.Jsoup
import exception.ServiceError
import play.api.libs.json.Json
import play.api.Logger

class WSRequestHolderWrapper(
                              url: String,
                              headers: Map[String, Seq[String]] = Map(),
                              queryString: Map[String, Seq[String]] = Map())
  extends WSRequestHolder(url, headers, queryString, None, None, None, None, None) {
  override def post[T](body: T)(implicit wrt: Writeable[T], ct: ContentTypeOf[T]): Future[Response] = super.post(body).map(response => handle(response, Some(body), "POST"))

  override def get() = super.get().map(response => handle(response, None, "GET"))

  override def withQueryString(parameters: (String, String)*): WSRequestHolder =
    new WSRequestHolderWrapper(url, headers, queryString = parameters.foldLeft(queryString) {
      case (m, (k, v)) => m + (k -> (v +: m.get(k).getOrElse(Nil)))
    })

  def handle[T](response: Response, requestBody: Option[T], serviceMethod: String): Response = {
    val statusCode = response.getAHCResponse.getStatusCode
    if (statusCode != 200) {
      val body = Jsoup.parse(response.body)
      val content = body.select("#content")
      if (content.size > 0) {
        val p = content.select("p")
        val blocks = (0 to p.size - 1).map {
          index =>
            p.get(index).text
        }
        if (blocks.size == 3)
          throw new ServiceError(blocks(0), blocks(1), blocks(2), serviceMethod, url, requestBody match {
            case Some(rb) => Some(rb.toString)
            case _ => None
          })
        else
          throw new RuntimeException(content.text)
      }
      else {
        throw new RuntimeException(response.body)
      }
    } else {
      val json = if(serviceMethod == "GET") {
        Json.obj(
          "method" -> "GET",
          "url" -> url,
          "params" -> queryString.map{
            v => Json.obj(
              v._1 -> v._2.head
            )
          }
        )

      } else {
        Json.obj(
          "method" -> "POST",
          "url" -> url,
          "params" -> (requestBody match {
            case Some(rb) => rb.toString
            case _ => ""
          })
        )
      }
      Logger.info(json.toString())
      response
    }
  }
}
