package service.http

import service.common.BaseService
import play.api.libs.json._
import play.api.libs.concurrent.Execution.Implicits._
import org.joda.time.DateTime
import model._
import helper.json.ArticleItemReadWrites._
import scala.concurrent.Future
import model.PageModel
import model.ArticleItem
import model.ArticleModel

class ArticleDataService(baseUrl: String) extends BaseService(baseUrl) with service.common.ArticleDataService {
  def getByReport(
                   reportId: Int,
                   period: Int,
                   isVerifiedExcluded: Boolean,
                   pageSize: Int,
                   pageNumber: Int) = {
    val json = Json.obj(
      "ReportId" -> reportId,
      "QueuePeriod" -> period,
      "ExcludeCheckedItems" -> isVerifiedExcluded,
      "PageSize" -> pageSize,
      "PageIndex" -> (pageNumber - 1),
      "Order" -> Json.arr(Json.obj("Field" -> 0, "Order" -> 1))
    )
    service("GetByReport").withQueryString("criteria" -> json.toString).get().map {
      response =>
        Json.parse(response.body).as[PageModelWithCheckedCount[ArticleItem]]
    }
  }

  def getByReportCount(reportId: Int, period: Int, excludeCheckedItems: Boolean) = {
    val json = Json.obj(
      "ReportId" -> reportId,
      "ExcludeCheckedItems" -> excludeCheckedItems,
      "QueuePeriod" -> period
    )
    service("GetByReportCount").post(json).map {
      response => response.body.toInt
    }
  }

  def getDeletedFromReportCount(reportId: Int, period: Int) = {
    val json = Json.obj(
      "ReportId" -> reportId,
      "QueuePeriod" -> period
    )
    service("GetDeletedFromReportCount").post(json).map {
      response => response.body.toInt
    }
  }

  def getDeletedFromReport(reportId: Int, period: Int, pageSize: Int, pageNumber: Int) = {
    val json = Json.obj(
      "ReportId" -> reportId,
      "QueuePeriod" -> period,
      "PageSize" -> pageSize,
      "PageIndex" -> (pageNumber - 1)
    )
    service("GetDeletedFromReport").post(json).map {
      response => Json.parse(response.body).as[PageModel[ArticleItem]]
    }
  }

  def getMore(articleId: Int, emotion: Int, source: Int, sort: Int, pageSize: Int, pageNumber: Int, orderField: Int = 0, orderDirection: Int = 1) = {
    val json = Json.obj(
      "ArticleId" -> articleId,
      "PageSize" -> pageSize,
      "Level" -> 0,
      "Read" -> 0,
      "Quotation" -> 0,
      "Filter" -> JsNull,
      "PageIndex" -> (pageNumber - 1),
      "Emotion" -> emotion,
      "Source" -> source,
      "Order" -> Json.arr(
        Json.obj(
          "Field" -> orderField, //ArticleData
          "Order" -> orderDirection
        )
      )
    )
    service("GetMore").post(json).map {
      response =>
        Json.parse(response.body).as[PageModel[ArticleItem]]
    }
  }

  def getMoreWithReprint(articleId: Int, emotion: Int, source: Int, sort: Int, pageSize: Int, pageNumber: Int) = {
    val json = Json.obj(
      "ArticleId" -> articleId,
      "PageSize" -> pageSize,
      "PageIndex" -> (pageNumber - 1),
      "Emotion" -> emotion,
      "Source" -> source,
      "Order" -> Json.arr(
        Json.obj(
           "Field" -> 0, //ArticleData
           "Order" -> 1  //Desc
        )
      )
    )
    service("GetMoreWithReprint").post(json).map {
      response =>
        Json.parse(response.body).as[PageModel[ArticleItem]]
    }
  }

  def getById(articleId: Int, reportId: Option[Int] = None) = {
    _getById(articleId, reportId).map {
      str => Json.parse(str).as[ArticleModel]
    }
  }

  private def _getById(articleId: Int, reportId: Option[Int] = None): Future[String] = {
    val json = Json.obj(
      "Id" -> articleId,
      "ReportId" -> reportId
    )
    service("GetById").post(json).map {
      response => response.body
    }
  }

  def getMoreCount(articleId: Int) = {
    val json = Json.obj("ArticleId" -> articleId)
    service("GetMoreCount").post(json).map {
      response => response.body.toInt
    }
  }

  def getByObject(objectId: Int, source: Int, emotion: Int, query: Option[String], pageSize: Int, pageNumber: Int) = {
    val ticks = DateTime.now().minusDays(14).getMillis
    val ticksNow = DateTime.now.getMillis
    val json = Json.obj(
      "StartDate" -> ("/Date("+ticks.toString+")/"),
      "EndDate" -> ("/Date("+ticksNow.toString+")/"),
      "PageSize" -> pageSize,
      "PageIndex" -> (pageNumber - 1),
      "Quotation" -> 0,
      "Emotion" -> emotion,
      "Read" -> 0,
      "Source" -> source,
      "Filter" -> query,
      "ObjectId" -> objectId,
      "Order" -> Json.arr(
        Json.obj(
          "Field" -> 0,
          "Order" -> 1
        )
      )
    )
    service("GetByObject").post(json).map {
      response => Json.parse(response.body).as[PageModel[ArticleItem]]
    }
  }

  def getMoreCount(clusterId: Int, mainArticleId: Int) = {
    val json = Json.obj(
      "ClusterId" -> clusterId,
      "ArticleId" -> mainArticleId
    )
    service("GetMoreCount").post(json).map {
      response => response.body.toInt
    }
  }

  def getDeletedFromCluster(clusterId: Int, pageSize: Int, pageNumber: Int) = {
    val json = Json.obj(
      "ClusterId" -> clusterId,
      "PageSize" -> pageSize,
      "PageIndex" -> (pageNumber - 1)
    )
    service("GetDeletedFromCluster").post(json).map {
      response => Json.parse(response.body).as[PageModel[ArticleItem]]
    }
  }

  def getItemById(articleId: Int, reportId: Option[Int]) = _getById(articleId, reportId).map {
    str => Json.parse(str).as[ArticleItem]
  }
}
