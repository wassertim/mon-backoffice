package service.http

import service.common.BaseService
import play.api.libs.json.Json
import play.api.libs.concurrent.Execution.Implicits._

class ArticleEditService(baseUrl: String) extends BaseService(baseUrl: String) with service.common.ArticleEditService {
  def restoreToReport(articleId: Int, reportId: Int, userName: String, reason: Int) = {
    val json = Json.obj(
      "ReportId" -> reportId,
      "ArticleId" -> articleId,
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      )
    )
    service("RestoreToReport").post(json).map {
      response => response.body
    }
  }
  def deleteFromReport(articleId: Int, reportId: Int, userName: String, reason: Int) = {
    val json = Json.obj(
      "ReportId" -> reportId,
      "ArticleId" -> articleId,
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      )
    )
    service("DeleteFromReport").post(json).map {
      response => response.body
    }
  }

  def restoreEverywhere(articleId: Int, userName: String, reason: Int) = {
    val json = Json.obj(
      "ArticleId" -> articleId,
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      )
    )
    service("RestoreEverywhere").post(json)
  }
  def deleteEverywhere(articleId: Int, userName: String, reason: Int) = {
    val json = Json.obj(
      "ArticleId" -> articleId,
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      )
    )
    service("DeleteEverywhere").post(json).map {
      response => response.body
    }
  }

  def setImage(articleId: Int, imageId: String, userName: String, reason: Int) = {
    val json = Json.obj(
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      ),
      "ArticleId" -> articleId,
      "ImageId" -> imageId
    )
    service("SetImage").post(json).map(r=>r.body)
  }

  def deleteFromCluster(articleId: Int, clusterId: Int) = {
    val json = Json.obj(
      "ArticleId" -> articleId,
      "ClusterId" -> clusterId
    )
    service("DeleteFromCluster").post(json).map {
      response => response.body
    }
  }
}
