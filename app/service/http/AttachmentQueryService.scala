package service.http

import service.common.BaseService
import play.api.libs.concurrent.Execution.Implicits._

class AttachmentQueryService(baseUrl: String) extends BaseService(baseUrl) with service.common.AttachmentQueryService {
  def getPdfAttachments(articleId: Int) = service("GetPdfAttachments")
    .withQueryString("articleId" -> articleId.toString).get().map {
    response => toCamel(response.body)
  }
}
