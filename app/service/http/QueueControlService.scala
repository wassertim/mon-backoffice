package service.http

import service.common.BaseService
import play.api.libs.concurrent.Execution.Implicits._

class QueueControlService(baseUrl: String) extends BaseService(baseUrl) with service.common.QueueControlService {
  def forceReportRefresh(reportId: Int) = {
    service("ForceReportRefresh").withQueryString("reportId" -> reportId.toString).get().map {
      response =>
        response.body
    }
  }
}
