package service.http

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits._
import service.common.BaseService
import play.api.libs.json.Json
import model.Report
import model.Report.reads

class ReportQueueService(baseUrl: String) extends BaseService(baseUrl) with service.common.ReportQueueService {

  def getQueueStatistic(userName: String): Future[String] = service("GetQueueStatistic")
    .withQueryString("login" -> userName).get().map(r => toCamel(r.body))

  def getReport(userName: String) = service("GetReport")
    .withQueryString("login" -> userName).get().map{
    r =>
      if (r.body.isEmpty)
        None
      else {
        val report = Json.parse(r.body).as[Report]
        Some(report)
      }
  }


}
