package service.http

import service.common.BaseService
import model.Config
import play.api.libs.concurrent.Execution.Implicits._
import scala.concurrent.Future

class OperatorQueueEditService(baseUrl: String) extends BaseService(baseUrl) with service.common.OperatorQueueEditService {
  def addConfigurationToQueue(config: Config, userName: String): Future[String] = {
    service("AddConfigurationToQueue").withQueryString(
      "terminalConfigurationId" -> config.id,
      "priority" -> config.priority.toString,
      "login" -> userName
    ).get().map(response => response.body)
  }

  def deleteConfigurationFromQueue(id: String, userName: String) = {
    service("DeleteConfigurationFromQueue").withQueryString("terminalConfigurationIds" -> id, "login" -> userName)
      .get().map {
      response => response.body
    }
  }

  def saveReport(userName: String) = {
    service("SaveReport").withQueryString("login" -> userName).get().map {
      response => response.body
    }
  }
}
