package service.http

import service.common.BaseService
import play.api.libs.concurrent.Execution.Implicits._

class ConfigurationService(baseUrl: String) extends BaseService(baseUrl) with service.common.ConfigurationService {
  def getAllPublishTerminalConfiguration = service("GetAllPublishTerminalConfiguration").get().map {
    response => toCamel(response.body)
  }
}
