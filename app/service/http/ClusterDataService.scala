package service.http

import service.common.BaseService
import play.api.libs.json.Json
import play.api.libs.concurrent.Execution.Implicits._
import model.PageModelWithCheckedCount.clusterItemReads
import model.{ClusterItem, PageModelWithCheckedCount}

class ClusterDataService(baseUrl: String) extends BaseService(baseUrl) with service.common.ClusterDataService {
  def getByReport(reportId: Int, period: Int, pageSize: Int, pageNumber: Int, isVerifiedExcluded: Boolean) = {
    val json = Json.obj(
      "ReportId" -> reportId,
      "QueuePeriod" -> period,
      "PageSize" -> pageSize,
      "PageIndex" -> (pageNumber - 1),
      "ExcludeCheckedItems" -> isVerifiedExcluded
    )
    service("GetByReport").post(json).map {
      response =>
        Json.parse(response.body).as[PageModelWithCheckedCount[ClusterItem]]
    }
  }

  def getClusterCount(reportId: Int, period: Int, isVerifiedExcluded: Boolean) = {
    val json = Json.obj(
      "ReportId" -> reportId,
      "ExcludeCheckedItems" -> isVerifiedExcluded,
      "QueuePeriod" -> period
    )
    service("GetClusterCount").post(json).map {
      response =>
        (Json.parse(response.body) \ "Count").as[Int]
    }
  }

  def getDeletedFromReportCount(reportId: Int, period: Int) = {
    val json = Json.obj(
      "ReportId" -> reportId,
      "QueuePeriod" -> period
    )
    service("GetDeletedFromReportCount").post(json).map {
      response => response.body.toInt
    }
  }

  def getDeletedFromReport(reportId: Int, period: Int, pageSize: Int, pageNumber: Int) = {
    val json = Json.obj(
      "ReportId" -> reportId,
      "QueuePeriod" -> period,
      "PageSize" -> pageSize,
      "PageIndex" -> (pageNumber - 1)
    )
    service("GetDeletedFromReport").post(json).map {
      response => Json.parse(response.body).as[PageModelWithCheckedCount[ClusterItem]]
    }
  }

  def getById(clusterId: Int) = {
    val json = Json.obj("ClusterId" -> clusterId)
    service("GetById").post(json).map {
      response =>
        toCamel(response.body)
    }
  }

  def getImages(clusterId: Int, pageSize: Int, pageNumber: Int) = {
    val json = Json.obj(
      "ClusterId" -> clusterId,
      "PageSize" -> pageSize,
      "PageIndex" -> (pageNumber - 1)
    )
    service("GetImages").post(json).map {
      response =>
        toCamel(response.body)
    }
  }
}
