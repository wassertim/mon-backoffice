package service.http

import service.common.BaseService
import play.api.libs.json.Json
import play.api.libs.concurrent.Execution.Implicits._

class ImageOperationService(baseUrl: String) extends BaseService(baseUrl) with service.common.ImageOperationService {
  def uploadImage(image: Seq[Byte], mimeType: String) = {
    val json = Json.obj(
      "image" -> image.map(b=>Json.toJson(b & 0xff)),
      "mimeType" -> mimeType
    )
    service("UploadImage").post(json).map {
      response =>
        (Json.parse(response.body) \ "UploadImageResult").as[String]
    }
  }
}
