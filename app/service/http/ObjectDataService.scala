package service.http

import service.common.BaseService
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import model._
import ObjectItem.reads
import ObjectAtomInfo.reads

class ObjectDataService(baseUrl: String) extends BaseService(baseUrl) with service.common.ObjectDataService {
  def getByArticle(articleId: Int) = {
    val json = Json.obj(
      "ArticleId" -> articleId
    )
    service("GetByArticle").post(json).map {
      response => Json.parse(response.body).as[Seq[ObjectItem]]
    }
  }

  def getObjectById(objectId: Int) = {
    val json = Json.obj(
      "ObjectId" -> objectId
    )
    service("GetObjectById").post(json).map {
      response => toCamel(response.body)
    }
  }

  def getObjectAtomInfo(articleId: Int, objectId: Int) = {
    val json = Json.obj(
      "ArticleId" -> articleId,
      "ObjectId" -> objectId
    )
    service("GetObjectAtomInfo").post(json).map {
      response => Json.parse(response.body).as[ObjectAtomInfo]
    }
  }
}
