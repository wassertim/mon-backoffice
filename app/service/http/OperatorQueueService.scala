package service.http

import service.common.BaseService
import play.api.libs.concurrent.Execution.Implicits._

class OperatorQueueService(baseUrl: String) extends BaseService(baseUrl) with service.common.OperatorQueueService {
  def getConfigurationsByReportId(reportId: Int) = service("GetConfigurationsByReportId")
  .withQueryString("reportId" -> reportId.toString).get().map {
    response => toCamel(response.body)
  }

  def getConfigurationsOnControl = service("GetConfigurationsOnControl").get().map {
    response => toCamel(response.body)
  }
}

