package service.http

import play.api.libs.ws.WS
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits._


class UserService(baseUrl: String) extends service.common.UserService {
  def service(url: String) = WS.url(baseUrl + url)

  def verifyUser(name: String, password: String): Future[Boolean] = service("VerifyUser")
    .withQueryString(
      "name" -> name,
      "password" -> password
    ).get().map {
    response =>
      response.body match {
        case "true" => true
        case _ => false
      }
  }
}
