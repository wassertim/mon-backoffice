package service.http

import service.common.BaseService
import play.api.libs.json.Json
import play.api.libs.concurrent.Execution.Implicits._

class ClusterEditService(baseUrl: String) extends BaseService(baseUrl) with service.common.ClusterEditService {
  def deleteFromReport(clusterId: Int, reportId: Int, userName: String, reason: Int) = {
    val json = Json.obj(
      "ClusterId" -> clusterId,
      "ReportId" -> reportId,
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      )
    )
    service("DeleteFromReport").post(json)
  }

  def deleteEverywhere(clusterId: Int, userName: String, reason: Int) = {
    val json = Json.obj(
      "ClusterId" -> clusterId,
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      )
    )
    service("DeleteClusterEverywhere").post(json)
  }

  def restoreClusterEverywhere(clusterId: Int, userName: String, reason: Int) = {
    val json = Json.obj(
      "ClusterId" -> clusterId,
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      )
    )
    service("RestoreClusterEverywhere").post(json)
  }

  def restoreToReport(clusterId: Int, reportId: Int, userName: String, reason: Int) = {
    val json = Json.obj(
      "ClusterId" -> clusterId,
      "ReportId" -> reportId,
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      )
    )
    service("RestoreToReport").post(json)
  }

  def setMainArticleId(clusterId: Int, articleId: Int) = {
    val json = Json.obj(
      "ClusterId" -> clusterId,
      "ArticleId" -> articleId
    )
    service("SetMainArticleId").post(json).map {
      response => response.body
    }
  }

  def setImage(clusterId: Int, imageId: String, userName: String, reason: Int) = {
    val json = Json.obj(
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      ),
      "ClusterId" -> clusterId,
      "ImageId" -> imageId
    )
    service("SetImage").post(json).map(r=>r.body)
  }

  def setTitle(clusterId: Int, title: String, userName: String, reason: Int) = {
    val json = Json.obj(
      "AuditContext" -> Json.obj(
        "ItemTypeId" -> 1,
        "Username" -> userName,
        "Action" -> 2,
        "Mode" -> 2,
        "Reason" -> reason
      ),
      "ClusterId" -> clusterId,
      "Title" -> title
    )
    service("SetTitle").post(json).map {
      response => response.body
    }
  }

  def restoreArticleToCluster(articleId: Int, clusterId: Int) = {
    val json = Json.obj(
      "ArticleId" -> articleId,
      "ClusterId" -> clusterId
    )
    service("RestoreArticleToCluster").post(json).map {
      response => response.body
    }
  }
}
