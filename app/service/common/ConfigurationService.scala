package service.common

import scala.concurrent.Future

trait ConfigurationService {
  def getAllPublishTerminalConfiguration: Future[String]
}
