package service.common

import scala.concurrent.Future

trait ArticleEditService {
  def deleteFromCluster(articleId: Int, clusterId: Int): Future[String]

  def setImage(articleId: Int, imageId: String, userName: String, reason: Int): Future[String]

  def restoreEverywhere(articleId: Int, userName: String, reason: Int)

  def restoreToReport(articleId: Int, reportId: Int, userName: String, reason: Int)

  def deleteEverywhere(articleId: Int, userName: String, reason: Int)

  def deleteFromReport(articleId: Int, reportId: Int, userName: String, reason: Int)
}
