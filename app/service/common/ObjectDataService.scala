package service.common

import scala.concurrent.Future
import model.{ObjectAtomInfo, ObjectItem}

trait ObjectDataService {
  def getObjectAtomInfo(articleId: Int, objectId: Int): Future[ObjectAtomInfo]

  def getObjectById(objectId: Int): Future[String]

  def getByArticle(articleId: Int): Future[Seq[ObjectItem]]
}
