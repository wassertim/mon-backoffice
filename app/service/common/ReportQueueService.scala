package service.common
import scala.concurrent.Future
import model.Report

trait ReportQueueService {
  def getQueueStatistic(userName: String): Future[String]
  def getReport(userName: String): Future[Option[Report]]
}
