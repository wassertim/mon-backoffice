package service.common

import scala.concurrent.Future
import model._
import model.PageModel
import model.ArticleItem
import model.ArticleModel


trait ArticleDataService {
  def getDeletedFromCluster(clusterId: Int, pageSize: Int, pageNumber: Int): Future[PageModel[ArticleItem]]

  def getMoreCount(clusterId: Int, mainArticleId: Int): Future[Int]

  def getByObject(objectId: Int, source: Int, emotion: Int, query: Option[String], pageSize: Int, pageNumber: Int): Future[PageModel[ArticleItem]]

  def getMoreCount(articleId: Int): Future[Int]

  def getById(articleId: Int, reportId: Option[Int] = None): Future[ArticleModel]

  def getItemById(articleId: Int, reportId: Option[Int] = None): Future[ArticleItem]

  def getMore(articleId: Int, emotion: Int, source: Int, sort: Int, pageSize: Int, pageNumber: Int, orderField: Int = 0, orderDirection: Int = 1): Future[PageModel[ArticleItem]]

  def getMoreWithReprint(articleId: Int, emotion: Int, source: Int, sort: Int, pageSize: Int, pageNumber: Int): Future[PageModel[ArticleItem]]

  def getDeletedFromReport(reportId: Int, period: Int, pageSize: Int, pageNumber: Int): Future[PageModel[ArticleItem]]

  def getDeletedFromReportCount(reportId: Int, period: Int): Future[Int]

  def getByReportCount(reportId: Int, period: Int, excludeCheckedItems: Boolean): Future[Int]

  def getByReport(
    reportId: Int,
    period: Int,
    isVerifiedExcluded: Boolean = false,
    pageSize: Int = 20,
    pageNumber: Int = 1): Future[PageModelWithCheckedCount[ArticleItem]]
}
