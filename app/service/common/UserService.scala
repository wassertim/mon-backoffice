package service.common

import scala.concurrent.Future
import model.AuthData

trait UserService {
  def verifyUser(auth: AuthData): Future[Boolean] = verifyUser(auth.name, auth.password)
  def verifyUser(name: String, password: String): Future[Boolean]
}
