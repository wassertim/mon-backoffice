package service.common
import service.ws.WSRequestHolderWrapper


class BaseService(baseUrl: String) {
  def service(url: String) = new WSRequestHolderWrapper(baseUrl+url)

  def toCamel(json: String): String = {
    "[\"A-Za-z0-9]*[\"][:]".r.replaceAllIn(json, m => {
      val s = m.matched.substring(1)
      if (s.isEmpty) {
        m.matched
      } else {
        val result = s.charAt(0).toLower + s.substring(1)
        "\"" + result
      }
    })
  }
}
