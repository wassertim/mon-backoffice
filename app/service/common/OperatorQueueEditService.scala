package service.common

import model.Config
import scala.concurrent.Future

trait OperatorQueueEditService {
  def saveReport(userName: String): Future[String]

  def deleteConfigurationFromQueue(id: String, userName: String): Future[String]

  def addConfigurationToQueue(config: Config, userName: String): Future[String]
}
