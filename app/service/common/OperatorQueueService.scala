package service.common

import scala.concurrent.Future

trait OperatorQueueService {
  def getConfigurationsOnControl: Future[String]

  def getConfigurationsByReportId(reportId: Int): Future[String]
}
