package service.common

import scala.concurrent.Future
import model.{ClusterItem, PageModelWithCheckedCount}

trait ClusterDataService {
  def getImages(clusterId: Int, pageSize: Int, pageNumber: Int): Future[String]

  def getById(clusterId: Int): Future[String]

  def getDeletedFromReport(reportId: Int, period: Int, pageSize: Int, pageNumber: Int): Future[PageModelWithCheckedCount[ClusterItem]]

  def getDeletedFromReportCount(reportId: Int, period: Int): Future[Int]

  def getClusterCount(reportId: Int, period: Int, isVerifiedExcluded: Boolean): Future[Int]

  def getByReport(reportId: Int, period: Int, pageSize: Int, pageNumber: Int, isVerifiedExcluded: Boolean): Future[PageModelWithCheckedCount[ClusterItem]]

}
