package service.common

import scala.concurrent.Future

trait AttachmentQueryService {
  def getPdfAttachments(articleId: Int): Future[String]

}
