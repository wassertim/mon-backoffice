package service.common

import scala.concurrent.Future

trait QueueControlService {
  def forceReportRefresh(reportId: Int): Future[String]

}
