package service.common

import scala.concurrent.Future

trait ClusterEditService {
  def restoreArticleToCluster(articleId: Int, clusterId: Int): Future[String]

  def setTitle(clusterId: Int, title: String, userName: String, reason: Int): Future[String]

  def setImage(clusterId: Int, imageId: String, userName: String, reason: Int): Future[String]

  def setMainArticleId(clusterId: Int, articleId: Int): Future[String]

  def restoreClusterEverywhere(clusterId: Int, userName: String, reason: Int)

  def restoreToReport(clusterId: Int, reportId: Int, userName: String, reason: Int)

  def deleteEverywhere(clusterId: Int, userName: String, reason: Int)

  def deleteFromReport(clusterId: Int, reportId: Int, userName: String, reason: Int)

}
