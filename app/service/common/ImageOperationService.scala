package service.common

import scala.concurrent.Future

trait ImageOperationService {
  def uploadImage(image: Seq[Byte], mimeType: String): Future[String]
}
