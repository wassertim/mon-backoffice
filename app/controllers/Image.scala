package controllers

import play.api.mvc.{BodyParser, Action, Controller}
import helper.Authenticated
import play.api.libs.concurrent.Execution.Implicits._

import javax.inject.Inject
import org.apache.commons.io.FileUtils
import javax.imageio.ImageIO
import java.io.{IOException, File}
import java.awt.image.DataBufferByte
import play.api.libs.iteratee.Iteratee
import play.api.libs.concurrent.Promise
import play.api.libs.json.Json


class Image @Inject()(imageOperationService: service.common.ImageOperationService) extends Controller {
  def uploadImage = Authenticated.async(parse.multipartFormData) {
    request =>

      imageOperationService.uploadImage(FileUtils.readFileToByteArray(request.body.files(0).ref.file), ".jpg").map{
        guid => Ok(Json.obj(
          "url" -> s"http://v-stg-app11/ExternalServices/Monitor.Image.Web/Image/GetById/$guid/G1/1",
          "id" -> guid
        ))
      }
  }

  val slowBodyParser = BodyParser( request => Iteratee.foldM[Array[Byte],Int](0)((c,_) => Promise.timeout({ println("got a chunk!"); c+1} ,500) ).map(Right(_)))

  def extractBytes (ImageName: String): Seq[Byte] = {
    // open image
    val imgPath = new File(ImageName)
    val bufferedImage = ImageIO.read(imgPath)
    // get DataBufferBytes from Raster
    val raster = bufferedImage .getRaster
    val data   =  raster.getDataBuffer.asInstanceOf[DataBufferByte]
    data.getData
  }
}
