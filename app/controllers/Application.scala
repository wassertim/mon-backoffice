package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json.Json
import Play.current

class Application extends Controller {
  lazy val prodWrapper = Play.current.configuration.getString("services.prodWrapper.host").get
  lazy val imageHost = Play.current.configuration.getString("services.images.host").get
  def config = Action {
    implicit request =>
      Ok("var backofficeGlobals = " + Json.obj(
        "pdfUrl" -> s"$prodWrapper/api/prod/GetPdf",
        "imageHost" -> imageHost
      ).toString).as("text/javascript")
  }

  def index = Action {
    val pathToApp = play.Play.application().path().getAbsolutePath
    val file = if (Play.isProd) {
      "public/dist/index.html"
    } else {
      "index.html"
    }
    Ok.sendFile(
      content = new java.io.File(s"$pathToApp/$file"),
      fileName = _ => "index.html",
      inline = true
    )
  }

  def javascriptRoutes = Action {
    implicit request =>
      Ok(
        Routes.javascriptRouter("jsRoutes")(
          routes.javascript.Security.signIn,
          routes.javascript.Security.signOut,
          routes.javascript.ReportQueue.getQueueStatistic,
          routes.javascript.ReportQueue.getReport,
          routes.javascript.ReportQueue.forceReportRefresh,
          routes.javascript.Article.getByReport,
          routes.javascript.Article.getCounters,
          routes.javascript.Article.getDeletedFromReportCount,
          routes.javascript.Article.getDeletedFromReport,
          routes.javascript.Article.deleteFromReport,
          routes.javascript.Article.deleteEverywhere,
          routes.javascript.Article.restoreToReport,
          routes.javascript.Article.restoreEverywhere,
          routes.javascript.Article.getFilteredListForArticles,
          routes.javascript.Article.getById,
          routes.javascript.Article.getItemById,
          routes.javascript.Article.getCountersForMoreArticles,
          routes.javascript.Article.getByObject,
          routes.javascript.Article.setImage,
          routes.javascript.Article.getPdfAttachments,
          routes.javascript.Cluster.getByReport,
          routes.javascript.Cluster.getCounters,
          routes.javascript.Cluster.getDeletedFromReportCount,
          routes.javascript.Cluster.deleteFromReport,
          routes.javascript.Cluster.deleteEverywhere,
          routes.javascript.Cluster.restoreToReport,
          routes.javascript.Cluster.restoreEverywhere,
          routes.javascript.Cluster.getDeletedFromReport,
          routes.javascript.Cluster.getById,
          routes.javascript.Cluster.getImages,
          routes.javascript.Cluster.setImage,
          routes.javascript.Cluster.setTitle,
          routes.javascript.ClusterArticle.getCounters,
          routes.javascript.ClusterArticle.getDeletedCount,
          routes.javascript.ClusterArticle.getRelevantList,
          routes.javascript.ClusterArticle.setMainArticleId,
          routes.javascript.ClusterArticle.deleteArticles,
          routes.javascript.ClusterArticle.getDeletedFromCluster,
          routes.javascript.ClusterArticle.restoreToCluster,
          routes.javascript.Object.getByArticle,
          routes.javascript.Object.getObjectById,
          routes.javascript.Object.getObjectAtomInfo,
          routes.javascript.Image.uploadImage,
          routes.javascript.ReportQueue.getConfigurationsOnControl,
          routes.javascript.ReportQueue.addConfigurationToQueue,
          routes.javascript.ReportQueue.deleteConfigurationFromQueue,
          routes.javascript.ReportQueue.saveReport,
          routes.javascript.Configuration.getAllPublishTerminalConfiguration,
          routes.javascript.Configuration.getConfigurationsByReportId
        )
      ).as("text/javascript")
  }
}