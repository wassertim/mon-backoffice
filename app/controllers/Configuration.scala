package controllers

import play.api.mvc.Controller
import helper.Authenticated
import com.google.inject.Inject
import play.api.libs.concurrent.Execution.Implicits._
import service.common._

class Configuration @Inject()(operatorQueueService: OperatorQueueService, configurationService: ConfigurationService) extends Controller {
  def getConfigurationsByReportId(reportId: Int) = Authenticated.async {
    request =>
      operatorQueueService.getConfigurationsByReportId(reportId).map {
        response =>
          Ok(response)
      }
  }

  def getAllPublishTerminalConfiguration = Authenticated.async {
    request =>
      configurationService.getAllPublishTerminalConfiguration.map {
        response => Ok(response)
      }
  }
}
