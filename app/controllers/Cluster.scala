package controllers

import play.api.mvc.Controller
import com.google.inject.Inject
import helper.{FutureHelper, Authenticated}
import service.common.{ClusterEditService, ClusterDataService}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import FutureHelper.await

class Cluster @Inject()(clusterDataService: ClusterDataService, clusterEditService: ClusterEditService) extends Controller {

  def getByReport(reportId: Int, period: Int, pageSize: Int = 20, pageNumber: Int = 1, isVerifiedExcluded: Boolean = false) = Authenticated.async {
    request =>
      clusterDataService.getByReport(reportId, period, pageSize, pageNumber, isVerifiedExcluded).map {
        itemList =>
          Ok(Json.toJson(itemList))
      }
  }

  def getById(clusterId: Int) = Authenticated.async {
    request =>
      clusterDataService.getById(clusterId).map {
        response =>
          Ok(response)
      }
  }

  def getCounters(reportId: Int, period: Int) = Authenticated.async {
    request =>
      clusterDataService.getClusterCount(reportId, period, isVerifiedExcluded = true).map {
        withoutVerifiedCount =>
          val totalCount = await(clusterDataService.getClusterCount(reportId, period, isVerifiedExcluded = false))
          Ok(Json.obj(
            "totalCount" -> totalCount,
            "verifiedCount" -> (totalCount - withoutVerifiedCount)
          ))
      }
  }

  def getDeletedFromReport(reportId: Int, period: Int, pageSize: Int = 20, pageNumber: Int = 1) = Authenticated.async {
    request =>
      clusterDataService.getDeletedFromReport(reportId, period, pageSize, pageNumber).map {
        itemList => Ok(Json.toJson(itemList))
      }
  }

  def getDeletedFromReportCount(reportId: Int, period: Int) = Authenticated.async {
    request =>
      clusterDataService.getDeletedFromReportCount(reportId, period).map {
        response =>
          Ok(response.toString)
      }
  }

  def deleteFromReport(reportId: Int) = Authenticated(parse.json) {
    request =>
      val clusterIds = request.body.as[Seq[Int]]
      clusterIds.foreach(id => clusterEditService.deleteFromReport(id, reportId, request.user.userName, 14))
      Ok("ok")
  }

  def deleteEverywhere() = Authenticated(parse.json) {
    request =>
      val clusterIds = request.body.as[Seq[Int]]
      clusterIds.foreach(id => clusterEditService.deleteEverywhere(id, request.user.userName, 14))
      Ok("ok")
  }

  def restoreToReport(reportId: Int) = Authenticated(parse.json) {
    request =>
      val clusterIds = request.body.as[Seq[Int]]
      clusterIds.foreach(id => clusterEditService.restoreToReport(id, reportId, request.user.userName, 14))
      Ok("")
  }

  def restoreEverywhere() = Authenticated(parse.json) {
    request =>
      val clusterIds = request.body.as[Seq[Int]]
      clusterIds.foreach(id => clusterEditService.restoreClusterEverywhere(id, request.user.userName, 14))
      Ok("")
  }

  def getImages(clusterId: Int) = Authenticated.async {
    request =>
      clusterDataService.getImages(clusterId, pageSize = 20, pageNumber = 1).map {
        response => Ok(response)
      }
  }

  def setTitle(clusterId: Int) = Authenticated.async(parse.json) {
    request =>
      val title = (request.body \ "title").as[String]
      clusterEditService.setTitle(clusterId, title, request.user.userName, 14).map {
        response => Ok(response)
      }
  }

  def setImage(clusterId: Int) = Authenticated.async(parse.json) {
    request =>
      val imageId = (request.body \ "imageId").as[String]
      clusterEditService.setImage(clusterId, imageId, request.user.userName, 14).map {
        response => Ok(response)
      }
  }
}
