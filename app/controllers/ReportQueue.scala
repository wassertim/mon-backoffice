package controllers

import play.api.mvc.Controller
import com.google.inject.Inject
import play.api.libs.concurrent.Execution.Implicits._
import helper.Authenticated
import service.common._
import model.Config
import model.Config.jsonReads
import play.api.libs.json.Json

class ReportQueue @Inject()(
                             reportQueueService: ReportQueueService,
                             operatorQueueService: OperatorQueueService,
                             configurationService: ConfigurationService,
                             operatorQueueEditService: OperatorQueueEditService,
                             queueControlService: QueueControlService) extends Controller {
  def getQueueStatistic = Authenticated.async {
    request =>
      reportQueueService.getQueueStatistic(request.user.userName).map {
        response =>
          Ok(response)
      }
  }

  def getReport = Authenticated.async {
    request =>
      reportQueueService.getReport(request.user.userName).map {
        report => Ok(Json.toJson(report))
      }
  }

  def addConfigurationToQueue() = Authenticated.async(parse.json) {
    request =>
      val config = request.body.as[Config]
      operatorQueueEditService.addConfigurationToQueue(config, request.user.userName).map {
        response => Ok("ok")
      }
  }

  def deleteConfigurationFromQueue(id: String) = Authenticated.async {
    request =>
      operatorQueueEditService.deleteConfigurationFromQueue(id, request.user.userName).map {
        response => Ok(response)
      }
  }

  def getConfigurationsOnControl = Authenticated.async {
    request =>
      operatorQueueService.getConfigurationsOnControl.map {
        response => Ok(response)
      }
  }

  def forceReportRefresh(reportId: Int) = Authenticated.async {
    request =>
      queueControlService.forceReportRefresh(reportId).map {
        response => Ok(response)
      }
  }
  def saveReport = Authenticated.async {
    request =>
      operatorQueueEditService.saveReport(request.user.userName).map {
        response => Ok(response)
      }
  }
}
