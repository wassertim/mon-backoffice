package controllers

import play.api.mvc.Controller
import com.google.inject.Inject
import helper.Authenticated
import service.common.{ArticleEditService, ClusterEditService, ArticleDataService}
import play.api.libs.json.Json
import play.api.libs.concurrent.Execution.Implicits._
import helper.FutureHelper.await
import model.PageModel.articleItemWrites

class ClusterArticle @Inject()(articleDataService: ArticleDataService, clusterEditService: ClusterEditService, articleEditService: ArticleEditService) extends Controller{

  def getCounters(clusterId: Int, mainArticleId: Int) = Authenticated.async {
    request =>
      articleDataService.getMoreCount(clusterId, mainArticleId).map {
        totalCount => Ok(Json.obj("totalCount" -> totalCount, "verifiedCount" -> 0))
      }
  }

  def getDeletedCount(clusterId: Int) = Authenticated.async {
    request =>
      articleDataService.getDeletedFromCluster(clusterId, pageSize = 10, pageNumber = 1).map {
        itemList => Ok(itemList.totalCount.toString)
      }
  }

  def setMainArticleId(clusterId: Int) = Authenticated.async(parse.json) {
    request =>
      val articleId = (request.body \ "articleId").as[Int]
      clusterEditService.setMainArticleId(clusterId, articleId).map {
        response => Ok(response)
      }
  }

  def deleteArticles(clusterId: Int) = Authenticated(parse.json) {
    request =>
      val articleIds = request.body.as[Seq[Int]]
      articleIds.foreach(id => articleEditService.deleteFromCluster(id, clusterId))
      Ok("ok")
  }

  def getRelevantList(mainArticleId: Int) = Authenticated.async {
    request =>
      articleDataService.getMore(mainArticleId, 0, 0, 0, 20, 1, 1, 1).map {
        response =>
          val mainArticle = await(articleDataService.getItemById(mainArticleId))
          val itemList = response.copy(items = mainArticle +: response.items)
          Ok(Json.toJson(itemList))
      }
  }

  def getDeletedFromCluster(clusterId: Int) = Authenticated.async {
    request =>
      articleDataService.getDeletedFromCluster(clusterId, 100, 1).map {
        response => Ok(Json.toJson(response))
      }
  }

  def restoreToCluster(clusterId: Int) = Authenticated(parse.json) {
    request =>
      val articleIds = request.body.as[Seq[Int]]
      articleIds.foreach(id => clusterEditService.restoreArticleToCluster(id, clusterId))
      Ok("ok")
  }
}
