package controllers

import play.api.mvc.Controller
import javax.inject.Inject
import service.common.ObjectDataService
import helper.Authenticated
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import model.ObjectItem.writes
import model.ObjectAtomInfo.writes
import play.api.cache.Cached
import play.api.Play.current

class Object @Inject()(objectDataService: ObjectDataService) extends Controller {
  def getByArticle(articleId: Int) = Authenticated.async {
    request =>
      objectDataService.getByArticle(articleId).map {
        objectList => Ok(Json.toJson(objectList))
      }
  }
  def getObjectById(objectId: Int) = Authenticated.async {
    request =>
      objectDataService.getObjectById(objectId).map {
        response => Ok(response)
      }
  }
  def getObjectAtomInfo(articleId: Int, objectId: Int) = Cached("objectInfo." + articleId + "_" + objectId, 604800)(Authenticated.async {
    request =>
      objectDataService.getObjectAtomInfo(articleId, objectId).map {
        objectAtomInfo =>
          Ok(Json.toJson(objectAtomInfo))
      }

  })
}
