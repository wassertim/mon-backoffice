package controllers

import play.api.mvc.Controller
import helper.Authenticated
import play.api.libs.concurrent.Execution.Implicits._
import com.google.inject.Inject
import play.api.libs.json.Json
import model.PageModel.articleItemWrites
import service.common._
import helper.FutureHelper.await

class Article @Inject()(articleDataService: ArticleDataService, articleEditService: ArticleEditService, attachmentQuery: AttachmentQueryService) extends Controller {
  def getByReport(
                   reportId: Int,
                   period: Int,
                   isVerifiedExcluded: Boolean,
                   pageSize: Int,
                   pageNumber: Int) = Authenticated.async {
    request =>
      articleDataService.getByReport(reportId, period, isVerifiedExcluded, pageSize, pageNumber).map {
        response =>
          Ok(Json.toJson(response))
      }
  }

  def setImage(articleId: Int) = Authenticated.async(parse.json) {
    request =>
      val imageId = (request.body \ "imageId").as[String]
      articleEditService.setImage(articleId, imageId, request.user.userName, 14).map {
        response => Ok(response)
      }
  }

  def getById(articleId: Int, reportId: Option[Int] = None) = Authenticated.async {
    request =>
      articleDataService.getById(articleId, reportId).map {
        articleModel => Ok(Json.toJson(articleModel))
      }
  }

  def getItemById(articleId: Int, reportId: Option[Int] = None) = Authenticated.async {
    request =>
      articleDataService.getItemById(articleId, reportId).map {
        articleModel => Ok(Json.toJson(articleModel))
      }
  }

  def getCountersForMoreArticles(articleId: Int) = Authenticated.async {
    request =>
      articleDataService.getMoreCount(articleId).map {
        count => Ok(Json.obj("totalCount" -> count, "verifiedCount" -> 0))
      }
  }

  def getFilteredListForArticles(articleId: Int, emotion: Int = 0, source: Int = 0, sort: Int = 1, pageSize: Int = 20, pageNumber: Int = 1) = Authenticated.async {
    request =>
      val result = if (sort == 0) {
        articleDataService.getMoreWithReprint(articleId, emotion, source, sort, pageSize, pageNumber)
      } else {
        articleDataService.getMore(articleId, emotion, source, sort, pageSize, pageNumber)
      }
      result.map {
        itemList => Ok(Json.toJson(itemList))
      }
  }

  def getDeletedFromReport(reportId: Int, period: Int, pageSize: Int, pageNumber: Int) = Authenticated.async {
    request =>
      articleDataService.getDeletedFromReport(reportId, period, pageSize, pageNumber).map {
        itemList => Ok(Json.toJson(itemList))
      }
  }

  def getDeletedFromReportCount(reportId: Int, period: Int) = Authenticated.async {
    request =>
      articleDataService.getDeletedFromReportCount(reportId, period).map {
        count => Ok(count.toString)
      }
  }

  def restoreEverywhere() = Authenticated(parse.json) {
    request =>
      val articleIds = request.body.as[Seq[Int]]
      articleIds.foreach(id => articleEditService.restoreEverywhere(id, request.user.userName, 14))
      Ok("ok")
  }

  def restoreToReport(reportId: Int) = Authenticated(parse.json) {
    request =>
      val articleIds = request.body.as[Seq[Int]]
      articleIds.foreach(id => articleEditService.restoreToReport(id, reportId, request.user.userName, 14))
      Ok("ok")
  }

  def deleteFromReport(reportId: Int) = Authenticated(parse.json) {
    request =>
      val articleIds = request.body.as[Seq[Int]]
      articleIds.foreach {
        articleId =>
          articleEditService.deleteFromReport(articleId, reportId, request.user.userName, 14)
      }
      Ok("ok")
  }

  def getByObject(objectId: Int, sourceFilter: Int = 0, emotionFilter: Int = 0, query: Option[String], pageSize: Int = 10, pageNumber: Int = 1) = Authenticated.async {
    request =>
      articleDataService.getByObject(objectId, sourceFilter, emotionFilter, query, pageNumber, pageNumber).map {
        itemList => Ok(Json.toJson(itemList))
      }
  }

  def deleteEverywhere() = Authenticated(parse.json) {
    request =>
      val articleIds = request.body.as[Seq[Int]]
      articleIds.foreach(id => articleEditService.deleteEverywhere(id, request.user.userName, 14))
      Ok("ok")
  }

  def getCounters(reportId: Int, period: Int) = Authenticated.async {
    request =>
      articleDataService.getByReportCount(reportId, period, excludeCheckedItems = true).map {
        withoutVerifiedCount =>
          val totalCount = await(articleDataService.getByReportCount(reportId, period, excludeCheckedItems = false))
          Ok(Json.obj(
            "totalCount" -> totalCount,
            "verifiedCount" -> (totalCount - withoutVerifiedCount)
          ))
      }
  }

  def getPdfAttachments(articleId: Int) = Authenticated.async {
    request =>
      attachmentQuery.getPdfAttachments(articleId).map {
        response => Ok(response)
      }
  }
}
