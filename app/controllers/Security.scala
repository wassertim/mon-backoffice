package controllers

import play.api.mvc.{Controller, Action}
import com.google.inject.Inject
import play.api.libs.concurrent.Execution.Implicits._

import model.{User, AuthData}
import play.api.libs.json._
import helper.json.JsonReadsWrites
import JsonReadsWrites.{authDataReads, userWrites}

class Security @Inject()(userService: service.common.UserService) extends Controller {

  def signIn = Action.async(parse.json) {
    r =>
      val auth = r.body.as[AuthData]
      userService.verifyUser(auth).map {
        authenticated =>
          val js = Json.toJson(User(auth.name, authenticated))
          if (authenticated) {
            Ok(js).withSession("identity" -> js.toString())
          } else {
            Ok(js)
          }
      }
  }

  def signOut = Action {
    Ok("").withNewSession
  }
}
