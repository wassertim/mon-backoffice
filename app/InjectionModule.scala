import com.tzavellas.sse.guice.ScalaModule
import service.http._
import play.api.Play

class InjectionModule extends ScalaModule {
  val queueServer = Play.current.configuration.getString("services.queue.host").get
  val server = Play.current.configuration.getString("services.bridge.host").get
  val restWrapper = Play.current.configuration.getString("services.prodWrapper.host").get
  def configure() {
    bind[service.common.UserService].toInstance(new UserService(s"$server/Monitor.Security.Host/UserService.svc/json/"))
    bind[service.common.ReportQueueService].toInstance(new ReportQueueService(s"$server/Monitor.Configuration.Host/OperatorQueueService.svc/json/"))
    bind[service.common.ArticleDataService].toInstance(new ArticleDataService(s"$server/Monitor.Core.Host/ArticleDataService.svc/json/"))
    bind[service.common.OperatorQueueService].toInstance(new OperatorQueueService(s"$server/Monitor.Configuration.Host/OperatorQueueService.svc/json/"))
    bind[service.common.ArticleEditService].toInstance(new ArticleEditService(s"$server/Monitor.Core.Host/ArticleEditService.svc/json/"))
    bind[service.common.ObjectDataService].toInstance(new ObjectDataService(s"$server/Monitor.Core.Host/ObjectDataService.svc/json/"))
    bind[service.common.ImageOperationService].toInstance(new ImageOperationService(s"$server/Monitor.Image.Host/ImageOperationsService.svc/json/"))
    bind[service.common.ConfigurationService].toInstance(new ConfigurationService(s"$server/Monitor.Configuration.Host/ConfigurationService.svc/json/"))
    bind[service.common.OperatorQueueEditService].toInstance(new OperatorQueueEditService(s"$server/Monitor.Configuration.Host/OperatorQueueEditService.svc/json/"))
    bind[service.common.ClusterDataService].toInstance(new ClusterDataService(s"$server/Monitor.Core.Host/ClusterDataService.svc/json/"))
    bind[service.common.ClusterEditService].toInstance(new ClusterEditService(s"$server/Monitor.Core.Host/ClusterEditService.svc/json/"))
    bind[service.common.AttachmentQueryService].toInstance(new AttachmentQueryService(s"$restWrapper/api/prod/"))
    bind[service.common.QueueControlService].toInstance(new QueueControlService(s"$queueServer/Monitor.Cache.QueueControl.Host/QueueControlService.svc/json/"))
  }
}
