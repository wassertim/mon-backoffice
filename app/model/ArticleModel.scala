package model

import play.api.libs.json.{Writes, JsPath, Reads}
import play.api.libs.functional.syntax._

case class ArticleModel(
  id: Int,
  title: String,
  source: String,
  publishDate: String,
  imageUrl: String,
  imageId: String,
  hasOwnImage: Boolean,
  isRead: Boolean,
  url: Option[String],
  articlesInClusterCount: Int,
  sumAtomIndex: Double,
  highlightText: String,
  reprintCount: Int
 ) extends ArticleBase(
  id, title, source, publishDate, imageUrl, imageId, hasOwnImage, isRead, url, sumAtomIndex
)

object ArticleModel {
  implicit val reads: Reads[ArticleModel] = (
    (JsPath \ "Id").read[Int] and
      (JsPath \ "Title").read[String] and

      (JsPath \ "Source").read[String] and
      (JsPath \ "PublishDate").read[String] and
      (JsPath \ "ImageUrl").read[String] and
      (JsPath \ "ImageUrl").read[String].map {
        imageUrl =>
          val regex = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}".r
          regex.findFirstIn(imageUrl).getOrElse("")
      } and
      (JsPath \ "HasOwnImage").read[Boolean] and
      (JsPath \ "IsRead").read[Boolean] and
      (JsPath \ "Url").readNullable[String] and
      (JsPath \ "ArticlesInClusterCount").read[Int] and
      (JsPath \ "SumAtomIndex").read[Double] and
      (JsPath \ "HighlightText").read[String] and
      (JsPath \ "ReprintCount").read[Int]
    )(ArticleModel.apply _)
  implicit val writes: Writes[ArticleModel] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "title").write[String] and
      (JsPath \ "source").write[String] and
      (JsPath \ "publishDate").write[String] and
      (JsPath \ "imageUrl").write[String] and
      (JsPath \ "imageId").write[String] and
      (JsPath \ "hasOwnImage").write[Boolean] and
      (JsPath \ "isRead").write[Boolean] and
      (JsPath \ "url").writeNullable[String] and
      (JsPath \ "articlesInClusterCount").write[Int] and
      (JsPath \ "sumAtomIndex").write[Double] and
      (JsPath \ "highlightText").write[String] and
      (JsPath \ "reprintCount").write[Int]
    )(unlift(ArticleModel.unapply))
}
