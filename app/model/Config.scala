package model

import play.api.libs.json.{JsPath, Reads}
import play.api.libs.functional.syntax._

case class Config(id: String, priority: Int)
object Config {
  implicit val jsonReads: Reads[Config] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "operatorPriority").read[Int]
    )(Config.apply _)
}
