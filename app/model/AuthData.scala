package model

case class AuthData(name: String, password: String, isPermanent: Boolean)
