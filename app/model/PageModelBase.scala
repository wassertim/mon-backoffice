package model

//{"PageIndex":0,"PageSize":100,"TotalCount":0,"Items":[]}
abstract class PageModelBase[T](totalCount: Int, pageNumber: Int, pageSize: Int, items: Seq[T])
