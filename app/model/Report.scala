package model

import play.api.libs.json.{Writes, JsPath, Reads}
import play.api.libs.functional.syntax._

case class Report(
  reportId: Int,
  displayName: String,
  lastCacheDate: Option[String],
  lastViewDate: Option[String],
  numberOfConfigurations:Int,
  period:Int,
  `type`: Int
)
object Report {
  implicit val reads: Reads[Report] = (
    (JsPath \ "ReportId").read[Int] and
      (JsPath \ "DisplayName").read[String] and
      (JsPath \ "LastCacheDate").read[Option[String]] and
      (JsPath \ "LastViewDate").read[Option[String]] and
      (JsPath \ "NumberOfConfigurations").read[Int] and
      (JsPath \ "Period").read[Int] and
      (JsPath \ "Type").read[Int]
    )(Report.apply _)

  implicit val writes: Writes[Report] = (
    (JsPath \ "reportId").write[Int] and
      (JsPath \ "displayName").write[String] and
      (JsPath \ "lastCacheDate").write[Option[String]] and
      (JsPath \ "lastViewDate").write[Option[String]] and
      (JsPath \ "numberOfConfigurations").write[Int] and
      (JsPath \ "period").write[Int] and
      (JsPath \ "type").write[Int]
    )(unlift(Report.unapply))
}
