package model

import play.api.libs.json.{Writes, JsPath, Reads}
import play.api.libs.functional.syntax._
abstract class ObjectItemBase(
  id: Int,
  name: String,
  `type`: Int,
  objectRole: Int,
  quotationType: Int,
  emotionalTinge: Int,
  imageUrl: String,
  imageId: String,
  atomIndex: Double
)
case class ObjectItem(
  id: Int,
  name: String,
  `type`: Int,
  objectRole: Int,
  quotationType: Int,
  emotionalTinge: Int,
  imageUrl: String,
  imageId: String,
  atomIndex: Double
) extends ObjectItemBase(id, name, `type`, objectRole, quotationType, emotionalTinge, imageUrl, imageId, atomIndex)
object ObjectItem {
  implicit val reads: Reads[ObjectItem] = (
    (JsPath \ "Id").read[Int] and
      (JsPath \ "Name").read[String] and
      (JsPath \ "Type").read[Int] and
      (JsPath \ "ObjectRole").read[Int] and
      (JsPath \ "QuotationType").read[Int] and
      (JsPath \ "EmotionalTinge").read[Int] and
      (JsPath \ "ImageUrl").read[String] and
      (JsPath \ "ImageUrl").read[String].map {
        imageUrl =>
          val regex = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}".r
          regex.findFirstIn(imageUrl).getOrElse("")
      } and
      (JsPath \ "AtomIndex").read[Double]
    )(ObjectItem.apply _)

  implicit val writes: Writes[ObjectItem] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "type").write[Int] and
      (JsPath \ "objectRole").write[Int] and
      (JsPath \ "quotationType").write[Int] and
      (JsPath \ "emotionalTinge").write[Int] and
      (JsPath \ "imageUrl").write[String] and
      (JsPath \ "imageId").write[String] and
      (JsPath \ "atomIndex").write[Double]
    )(unlift(ObjectItem.unapply))
}
