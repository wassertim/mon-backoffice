package model

case class ArticleItemEx (
  id: Int,
  title: String,
  annotation: String,
  source: String,
  publishDate: String,
  imageUrl: String,
  imageId: String,
  hasOwnImage: Boolean,
  isRead: Boolean,
  url: Option[String],
  articlesInClusterCount: Int,
  sumAtomIndex: Double
) extends ArticleBase(
  id,title,source,publishDate,imageUrl,imageId,hasOwnImage,isRead,url, sumAtomIndex
)
