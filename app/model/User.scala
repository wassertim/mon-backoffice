package model

case class User(userName: String, isAuthenticated: Boolean, role: String = "user")
