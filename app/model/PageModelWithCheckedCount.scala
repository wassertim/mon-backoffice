package model

import play.api.libs.json._
import play.api.libs.functional.syntax._
import ArticleItem.{reads, writes}
import ClusterItem.{reads, writes}

case class PageModelWithCheckedCount[T](
  checkedItemsCount: Int,
  totalCount: Int,
  pageNumber: Int,
  pageSize: Int,
  items: Seq[T]
) extends PageModelBase(totalCount, pageNumber, pageSize, items)
object PageModelWithCheckedCount {
  implicit val articleItemReads: Reads[PageModelWithCheckedCount[ArticleItem]] = (
    (JsPath \ "CheckedItemsCount").read[Int] and
      (JsPath \ "TotalCount").read[Int] and
      (JsPath \ "PageIndex").read[Int].map(value => value + 1) and
      (JsPath \ "PageSize").read[Int] and
      (JsPath \ "Items").read[Seq[ArticleItem]]
    )(PageModelWithCheckedCount.apply[ArticleItem] _)

  implicit val articleItemWrites: Writes[PageModelWithCheckedCount[ArticleItem]] = (
    (JsPath \ "checkedItemsCount").write[Int] and
      (JsPath \ "totalCount").write[Int] and
      (JsPath \ "pageNumber").write[Int] and
      (JsPath \ "pageSize").write[Int] and
      (JsPath \ "items").write[Seq[ArticleItem]]
    )(unlift(PageModelWithCheckedCount.unapply[ArticleItem]))

  implicit val clusterItemReads: Reads[PageModelWithCheckedCount[ClusterItem]] = (
    (JsPath \ "CheckedItemsCount").read[Int] and
      (JsPath \ "TotalCount").read[Int] and
      (JsPath \ "PageIndex").read[Int].map(value => value + 1) and
      (JsPath \ "PageSize").read[Int] and
      (JsPath \ "Items").read[Seq[ClusterItem]]
    )(PageModelWithCheckedCount.apply[ClusterItem] _)

  implicit val clusterItemWrites: Writes[PageModelWithCheckedCount[ClusterItem]] = (
    (JsPath \ "checkedItemsCount").write[Int] and
      (JsPath \ "totalCount").write[Int] and
      (JsPath \ "pageNumber").write[Int] and
      (JsPath \ "pageSize").write[Int] and
      (JsPath \ "items").write[Seq[ClusterItem]]
    )(unlift(PageModelWithCheckedCount.unapply[ClusterItem]))
}