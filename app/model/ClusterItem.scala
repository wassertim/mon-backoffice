package model

import play.api.libs.json.{Writes, JsPath, Reads}
import play.api.libs.functional.syntax._

case class ClusterItem(
  id: Int,
  title: String,
  annotation: String,
  mainArticleId: Int,
  articlesCount: Int,
  articlesCountOriginal: Int,
  firstArticleDate: String,
  lastArticleDate: String,
  imageUrl: String,
  imageId: String,
  isRead: Boolean,
  negativeCount: Int,
  neutralCount: Int
)
object ClusterItem {
  implicit val reads: Reads[ClusterItem] = (
    (JsPath \ "Id").read[Int] and
      (JsPath \ "Title").read[String] and
      (JsPath \ "Annotation").read[String] and
      (JsPath \ "MainArticleId").read[Int] and
      (JsPath \ "ArticlesCount").read[Int] and
      (JsPath \ "ArticlesCountOriginal").read[Int] and
      (JsPath \ "FirstArticleDate").read[String] and
      (JsPath \ "LastArticleDate").read[String] and
      (JsPath \ "ImageUrl").read[String] and
      (JsPath \ "ImageUrl").read[String].map {
        imageUrl =>
          val regex = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}".r
          regex.findFirstIn(imageUrl).getOrElse("")
      } and
      (JsPath \ "IsRead").read[Boolean] and
      (JsPath \ "NegativeCount").read[Int] and
      (JsPath \ "NeutralCount").read[Int]
    )(ClusterItem.apply _)

  implicit val writes: Writes[ClusterItem] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "title").write[String] and
      (JsPath \ "annotation").write[String] and
      (JsPath \ "mainArticleId").write[Int] and
      (JsPath \ "articlesCount").write[Int] and
      (JsPath \ "articlesCountOriginal").write[Int] and
      (JsPath \ "firstArticleDate").write[String] and
      (JsPath \ "lastArticleDate").write[String] and
      (JsPath \ "imageUrl").write[String] and
      (JsPath \ "imageId").write[String] and
      (JsPath \ "isRead").write[Boolean] and
      (JsPath \ "negativeCount").write[Int] and
      (JsPath \ "neutralCount").write[Int]
    )(unlift(ClusterItem.unapply))
}
