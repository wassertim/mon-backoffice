package model

abstract class ArticleBase(
  id: Int,
  title: String,
  source: String,
  publishDate: String,
  imageUrl: String,
  imageId: String,
  hasOwnImage: Boolean,
  isRead: Boolean,
  url: Option[String],
  sumAtomIndex: Double
)
