package model

import play.api.libs.json._
import play.api.libs.functional.syntax._
import ArticleItem.{reads, writes}

case class PageModel[T](
  totalCount: Int,
  pageNumber: Int,
  pageSize: Int,
  items: Seq[T]
) extends PageModelBase(totalCount, pageNumber, pageSize, items)
object PageModel {
  implicit val articleItemReads: Reads[PageModel[ArticleItem]] = (
    (JsPath \ "TotalCount").read[Int] and
      (JsPath \ "PageIndex").read[Int].map(value => value + 1) and
      (JsPath \ "PageSize").read[Int] and
      (JsPath \ "Items").read[Seq[ArticleItem]]
    )(PageModel.apply[ArticleItem] _)

  implicit val articleItemWrites: Writes[PageModel[ArticleItem]] = (
    (JsPath \ "totalCount").write[Int] and
      (JsPath \ "pageNumber").write[Int] and
      (JsPath \ "pageSize").write[Int] and
      (JsPath \ "items").write[Seq[ArticleItem]]
    )(unlift(PageModel.unapply[ArticleItem]))
}
