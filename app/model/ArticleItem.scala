package model

import play.api.libs.json.{Writes, JsPath, Reads}
import play.api.libs.functional.syntax._

case class ArticleItem (
  id: Int,
  title: String,
  annotation: String,
  source: String,
  publishDate: String,
  imageUrl: String,
  imageId: String,
  hasOwnImage: Boolean,
  isRead: Boolean,
  url: Option[String],
  sumAtomIndex: Double
) extends ArticleBase(
  id,title,source,publishDate,imageUrl,imageId,hasOwnImage,isRead,url, sumAtomIndex
)

object ArticleItem {
  implicit val reads: Reads[ArticleItem] = (
    (JsPath \ "Id").read[Int] and
      (JsPath \ "Title").read[String] and
      (JsPath \ "Annotation").read[String] and
      (JsPath \ "Source").read[String] and
      (JsPath \ "PublishDate").read[String] and
      (JsPath \ "ImageUrl").read[String] and
      (JsPath \ "ImageUrl").read[String].map {
        imageUrl =>
          val regex = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}".r
          regex.findFirstIn(imageUrl).getOrElse("")
      } and
      (JsPath \ "HasOwnImage").read[Boolean] and
      (JsPath \ "IsRead").read[Boolean] and
      (JsPath \ "Url").readNullable[String] and
      (JsPath \ "SumAtomIndex").read[Double]
    )(ArticleItem.apply _)

  implicit val writes: Writes[ArticleItem] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "title").write[String] and
      (JsPath \ "annotation").write[String] and
      (JsPath \ "source").write[String] and
      (JsPath \ "publishDate").write[String] and
      (JsPath \ "imageUrl").write[String] and
      (JsPath \ "imageId").write[String] and
      (JsPath \ "hasOwnImage").write[Boolean] and
      (JsPath \ "isRead").write[Boolean] and
      (JsPath \ "url").writeNullable[String] and
      (JsPath \ "sumAtomIndex").write[Double]
    )(unlift(ArticleItem.unapply))
}

