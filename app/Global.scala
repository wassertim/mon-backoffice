import com.google.inject.Guice
import exception.ServiceError
import play.api.{Logger, GlobalSettings}
import play.api.libs.json.Json
import play.api.mvc.{Results, RequestHeader}
import scala.concurrent.Future

object Global extends GlobalSettings {

  private lazy val injector = Guice.createInjector(new InjectionModule)

  override def getControllerInstance[A](controllerClass: Class[A]) = {
    injector.getInstance(controllerClass)
  }

  override def onError(request: RequestHeader, ex: Throwable) = {

    val json = ex.getCause match {
      case serviceError: ServiceError => Json.obj(
        "message" -> serviceError.getMessage,
        "exceptionMessage" -> ("Service error: " + serviceError.exceptionMessage),
        "stackTrace" -> serviceError.stackTrace,
        "serviceMethod" -> serviceError.serviceMethod,
        "serviceUrl" -> serviceError.url,
        "serviceParams" -> serviceError.requestBody
      )
      case _ => Json.obj(
        "message" -> "Service Error",
        "exceptionMessage" -> ex.getMessage
      )
    }
    //Logger.error(ex.getMessage, ex)
    Future.successful(Results.InternalServerError(json))
  }
}
