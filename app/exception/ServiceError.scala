package exception

class ServiceError(message: String, val exceptionMessage: String, val stackTrace: String, val serviceMethod: String, val url: String, val requestBody: Option[String]) extends RuntimeException(message)
