package helper.json

import play.api.libs.json.{Writes, JsPath, Reads}
import play.api.libs.functional.syntax._
import model._

object JsonReadsWrites {

  implicit val authDataReads: Reads[AuthData] = (
    (JsPath \ "name").read[String] and
      (JsPath \ "password").read[String] and
      (JsPath \ "isPermanent").read[Boolean]
    )(AuthData.apply _)


  implicit val userWrites: Writes[User] = (
    (JsPath \ "userName").write[String] and
      (JsPath \ "isAuthenticated").write[Boolean] and
      (JsPath \ "role").write[String]
    )(unlift(User.unapply))

  implicit val userReads: Reads[User] = (
    (JsPath \ "userName").read[String] and
      (JsPath \ "isAuthenticated").read[Boolean] and
      (JsPath \ "role").read[String]
    )(User.apply _)
}
