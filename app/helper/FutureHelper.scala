package helper

import scala.concurrent.{Await, Awaitable}
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit

object FutureHelper {
  def await[T](awaitable: Awaitable[T]) = Await.result[T](awaitable, Duration(20000, TimeUnit.MILLISECONDS))
}
