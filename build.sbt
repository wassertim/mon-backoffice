name := "mon-backoffice"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "com.tzavellas" % "sse-guice" % "0.7.1",
  "commons-io" % "commons-io" % "2.4",
  "org.jsoup" % "jsoup" % "1.7.3"
)     

play.Project.playScalaSettings
