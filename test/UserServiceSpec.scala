import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.test.Helpers._
import play.api.test.{FakeRequest}

import service.http.UserService

@RunWith(classOf[JUnitRunner])
class UserServiceSpec extends Specification {

  "UserService" should {

    "verify user" in {

      val userService: service.common.UserService = new UserService("http://v-stg-app14/Monitor.Security.Host/UserService.svc/json/")
      userService.verifyUser("dev_glass2", "test")
      None must beNone
    }


  }
}
