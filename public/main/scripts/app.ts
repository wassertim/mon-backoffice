///<reference path="controllers/article/ArticleListController.ts"/>
///<reference path="directives/HighlightControl.ts"/>
///<reference path="directives/ErrorAlert.ts"/>
///<reference path="directives/InfoBar.ts"/>
///<reference path="directives/FileUpload.ts"/>
///<reference path="directives/ListToolbar.ts"/>
///<reference path="directives/Modal.ts"/>
///<reference path="directives/StickyBar.ts"/>
///<reference path="helper/BreadcrumbHelper.ts"/>
///<reference path="controllers/cluster/ClusterListController.ts"/>
///<reference path="controllers/BreadcrumbController.ts"/>
///<reference path="controllers/ConfigController.ts"/>
///<reference path="controllers/NavController.ts"/>
///<reference path="controllers/ReportController.ts"/>
///<reference path="controllers/ReportQueueController.ts"/>
///<reference path="controllers/SecurityController.ts"/>
///<reference path="controllers/ModalSecurityController.ts"/>
///<reference path="controllers/article/DeletedArticleListController.ts"/>
///<reference path="directives/HighlightText.ts"/>
///<reference path="directives/ItemInfoBar.ts"/>
///<reference path="directives/LazyImage.ts"/>
///<reference path="global.ts"/>
//var Router: any;
//controllers
declare var globalFunctions: any;
app.controller('BreadcrumbController', backOffice.controllers.BreadcrumbController);
app.controller('NavController', backOffice.controllers.NavController);
//directives
app.directive('highlightText', backOffice.directives.highlightText);
app.directive('objectid', backOffice.directives.objectid);
app.directive('highlightControl', backOffice.directives.highlightControl);
app.directive('errorAlert', backOffice.directives.errorAlert);
app.directive('fileUpload', ['$http', backOffice.directives.fileUpload]);
app.directive('infoBar', ['$templateCache', '$compile', '$http', '$state', backOffice.directives.infoBar]);
app.directive('itemInfoBar', ['$templateCache', '$compile', '$http', '$state', backOffice.directives.itemInfoBar]);
app.directive('lazyImageSrc', ['$window', 'ImageHelper', backOffice.directives.lazyImage]);
app.directive('listToolbar', ['$window', '$location', backOffice.directives.listToolbar]);
app.directive('stickyBar', ['$window', backOffice.directives.stickyBar]);
//This directive fixes the issue with autocomplete in some browsers. When using form don't forget to fire the autofill:update event
app.directive('autoFillSync', ['$timeout', ($timeout) => {
  return {
    require: "ngModel",
    link: (scope, element, attrs, ngModel) => {
      scope.$on("autofill:update", () => {
        ngModel.$setViewValue(element.val());
      });
    }
  }
}]);
app.service('Confirmation', ['$modal', ($modal) => {
  return (message, okText, action) => {
    var modalInstance = $modal.open({
      templateUrl: 'public/main/partials/modals/confirmation.html',
      controller: backOffice.controllers.shared.ModalConfirmationController,
      resolve: {
        params: () => {
          return {
            message: message,
            okText: okText,
            cancelText: 'Отменить'
          }
        }
      }
    });
    return modalInstance.result.then(() => {
      return action();
    });
  }
}]);
app.factory('myHttpResponseInterceptor', ['$q', '$location', '$injector', ($q, $location, $injector) => {
  return {
    'responseError': (rejection) => {
      if (rejection.status === 401) {
        if (!backOffice.Global.isLoginOpen) {
          var $modal = $injector.get('$modal');
          backOffice.Global.isLoginOpen = true;
          var modalInstance = $modal.open({
            templateUrl: 'public/main/partials/modals/login.html',
            controller: backOffice.controllers.ModalSecurityController,
            size: 'sm'
          });
          modalInstance.result.then(function () {
            var $state:any = $injector.get('$state');
            $state.transitionTo($state.current, $state.current.params, { reload: true, inherit: true, notify: true });
            backOffice.Global.isLoginOpen = false;
          }, function () {
            backOffice.Global.isLoginOpen = false;
          });
          //$location.path('/login');
        }
      }
      return $q.reject(rejection);
    }
  };
}]);

app.config([
  '$stateProvider', '$locationProvider', '$httpProvider', '$parseProvider', 'stateHelperProvider',
  ($stateProvider:any, $locationProvider:any, $httpProvider:any, $parseProvider:any, stateHelper:any) => {
    $parseProvider.unwrapPromises(true);
    $httpProvider.interceptors.push('myHttpResponseInterceptor');
    var vw = globalFunctions.vw;
    var stateConfig = () => {
      var breadcrumbsHelper = backOffice.helpers.BreadcrumbHelper;
      $stateProvider
        .state('home', {
          url: '',
          templateUrl: vw('partials/home.html'),
          controller: ['$state', ($state) => {
            $state.go('otk.reports');
          }],
          breadcrumbs: () => {
            return [];
          }
        })
        .state('home2', {
          url: '/',
          controller: ['$state', ($state) => {
            $state.go('otk.reports');
          }],
          templateUrl: vw('partials/home.html'),
          breadcrumbs: () => {
            return [];
          }
        })
        .state('clustersTrash', {
          url: '/reports/{reportId}/deleted-clusters',
          controller: backOffice.controllers.ClusterListController,
          action: 'getClustersTrash',
          templateUrl: vw('partials/shared/trash-list.html'),
          breadcrumbs: (stateParams:ng.ui.IStateParams) => {
            var bh = new breadcrumbsHelper(stateParams);
            return [bh.clustersTrash(true)];
          }
        })
        .state('pod', {
          url: '/pod',
          controller: backOffice.controllers.ReportController,
          templateUrl: vw('partials/pod/main.html'),
          breadcrumbs: (stateParams:ng.ui.IStateParams) => {
            var bh = new breadcrumbsHelper(stateParams);
            return [bh.pod(true)];
          }
        })
        .state('logOff', {
          url: '/logoff',
          controller: backOffice.controllers.SecurityController,
          action: 'logOff'
        })
        .state('login', {
          url: '/login',
          controller: backOffice.controllers.SecurityController,
          templateUrl: vw('partials/security/login.html')
        })
        .state('monitor', {
          url: '/monitor',
          templateUrl: vw('partials/monitor/main.html'),
          breadcrumbs: (stateParams:ng.ui.IStateParams) => {
            var bh = new breadcrumbsHelper(stateParams);
            return [bh.monitor(true)];
          }
        });

      stateHelper.setNestedState({
        name: 'otk',
        url: '/otk',
        templateUrl: vw('partials/otk/main.html'),
        breadcrumbs: (stateParams:ng.ui.IStateParams) => {
          var bh = new breadcrumbsHelper(stateParams);
          return [bh.otk(true)];
        },
        children: [
          {
            name: 'configMessages',
            url: '/configs/{configId}/R{reportId}',
            controller: backOffice.controllers.article.ArticleListController,
            templateUrl: vw('partials/otk/configs/messages.html'),
            breadcrumbs: (stateParams:ng.ui.IStateParams) => {
              var bh = new breadcrumbsHelper(stateParams);
              return [bh.otk(false), bh.otkConfigs(false), bh.otkConfigReports(false), bh.otkConfigArticles(true)];
            }
          },
          {
            name: 'configsByReport',
            url: '/configs/by-report/{reportId}',
            controller: backOffice.controllers.ConfigController,
            templateUrl: vw('partials/otk/configs/configs.html'),
            breadcrumbs: (stateParams:ng.ui.IStateParams) => {
              var bh = new breadcrumbsHelper(stateParams);
              return [bh.otk(false), bh.otkConfigs(false), bh.otConfigsByReport(true)];
            }
          },
          {
            name: 'configReports',
            url: '/configs/all/{configId}',
            controller: backOffice.controllers.ReportController,
            templateUrl: vw('partials/otk/configs/reports.html'),
            breadcrumbs: (stateParams:ng.ui.IStateParams) => {
              var bh = new breadcrumbsHelper(stateParams);
              return [bh.otk(false), bh.otkConfigs(false), bh.otkConfigReports(true)];
            }
          },
          {
            name: 'configs',
            url: '/configs/all',
            controller: backOffice.controllers.ConfigController,
            templateUrl: vw('partials/otk/configs/configs.html'),
            breadcrumbs: (stateParams:ng.ui.IStateParams) => {
              var bh = new breadcrumbsHelper(stateParams);
              return [bh.otk(false), bh.otkConfigs(true)];
            }
          },
          {
            name: 'reports',
            url: '/queue',
            controller: backOffice.controllers.ReportQueueController,
            action: 'getQueueStatistic',
            templateUrl: vw('partials/otk/queue/reportsQueue.html'),
            breadcrumbs: (stateParams:ng.ui.IStateParams) => {
              var bh = new breadcrumbsHelper(stateParams);
              return [bh.otk(false), bh.otkReports(true)];
            }
          },
          {
            name: 'report',
            url: '/queue/top',
            controller: backOffice.controllers.ReportController,
            action: 'getReportInfo',
            templateUrl: vw('partials/otk/queue/report.html'),
            breadcrumbs: (stateParams:ng.ui.IStateParams) => {
              var bh = new breadcrumbsHelper(stateParams);
              return [bh.otk(false), bh.otkReports(false), bh.otkReport(true)];
            },
            children: [
              {
                name: 'articles',
                url: '/articles-{vs}',
                controller: backOffice.controllers.article.ArticleListController,
                action: 'getArticleList',
                templateUrl: vw('partials/article/list.html'),
                breadcrumbs: (stateParams:ng.ui.IStateParams) => {
                  var bh = new breadcrumbsHelper(stateParams);
                  return [bh.otk(false), bh.otkReports(false), bh.otkReport(false), bh.otkReportArticles(true)];
                }
              },
              {
                name: 'clusters',
                url: '/clusters-{vs}',
                controller: backOffice.controllers.ClusterListController,
                action: 'getClusters',
                templateUrl: vw('partials/cluster/list.html'),
                breadcrumbs: (stateParams:ng.ui.IStateParams) => {
                  var bh = new breadcrumbsHelper(stateParams);
                  return [bh.otk(false), bh.otkReports(false), bh.otkReport(false), bh.otkReportClusters(true)];
                }
              }
            ]
          },
          {
            "name": 'reportConfigs',
            url: '/queue/R{reportId}-{vs}/configs',
            controller: backOffice.controllers.ConfigController,
            templateUrl: vw('partials/otk/queue/configs.html'),
            breadcrumbs: (stateParams:ng.ui.IStateParams) => {
              var bh = new breadcrumbsHelper(stateParams);
              return [bh.otk(false), bh.otkReports(false), bh.otkReport(false), bh.otkReportConfigs(true)];
            }
          }
        ]
      });
      stateHelper.setNestedState({
        name: 'settings',
        url: '/settings',
        templateUrl: vw('partials/settings/main.html'),
        breadcrumbs: (stateParams:ng.ui.IStateParams) => {
          var bh = new breadcrumbsHelper(stateParams);
          return [bh.settings(true)];
        },
        children: [
          {
            name: 'general',
            url: '/general',
            templateUrl: vw('partials/settings/general/main.html'),
            breadcrumbs: (stateParams:ng.ui.IStateParams) => {
              var bh = new breadcrumbsHelper(stateParams);
              return [bh.settings(false), bh['settings.queue'](true)];
            }
          },
          {
            name: 'queue',
            url: '/queue',
            controller: backOffice.controllers.ReportQueueController,
            action: 'getSettingsData',
            templateUrl: vw('partials/settings/queue/main.html'),
            breadcrumbs: (stateParams:ng.ui.IStateParams) => {
              var bh = new breadcrumbsHelper(stateParams);
              return [bh.settings(false), bh['settings.queue'](true)];
            }
          }
        ]
      });
    };
    stateConfig();
    $httpProvider.interceptors.push(["$q", "$injector", ($q:ng.IQService, $injector:any) => {
      return {
        'responseError': (rejection:any) => {
          //TODO: Сделать глобальную обработку ошибок
          var rootScope = $injector.get('$rootScope');
          rootScope.error = rejection.data;
          return $q.reject(rejection);
        }
      };
    }]);
    $locationProvider.html5Mode(false);
  }]);




