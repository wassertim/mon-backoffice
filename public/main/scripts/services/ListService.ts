module backOffice.services {
  export interface IListServiceParams {
    deleteItems(count:number[]);
  }
  export class ListService implements common.IListService {


    deletedCount = 0;
    selectedCount = 0;

    constructor(private itemList, private params?:IListServiceParams) {

    }


    getSelectedIds() {
      return _.map(this.getSelectedItems(), (item:any) => {
        return item.id;
      });
    }

    deleteSelectedItems() {
      this.deleteItemsFromList(this.getSelectedIds());
    }

    deleteItemsFromList(ids:number[]):any {
      var deleteItems = () => {
        var deleted = _.remove(this.itemList().items, item => {
          return _.contains(ids, item.id);
        });
        this.deletedCount += deleted.length;
        this.itemList().totalCount -= deleted.length;
        this.selectedCount = 0;
      }
      deleteItems();
    }


    updateSelectedCount():any {
      this.selectedCount = this.getSelectedItems().length;
      return this.selectedCount;
    }

    getSelectedItems() {
      return _.where(this.itemList().items, { 'selected': true });
    }

    invertSelected(e) {
      e.preventDefault();
      _.forEach(this.itemList().items, (item:any) => {
        item.selected = !item.selected;
      });
      this.updateSelectedCount();
    }

    selectItem(item, e?) {
      if (e) {
        e.preventDefault();
      }
      item.selected = !item.selected;
      this.updateSelectedCount();
    }

    toggleSelected(e):any {
      e.preventDefault();
      this.updateSelectedCount();
      var selected;
      if (this.selectedCount === 0) {
        selected = true;
      }
      _.forEach(this.itemList().items, (item:any)=> {
        item.selected = selected;
      });
      this.updateSelectedCount();
    }

    toggleSelectButtonText():any {
      if (this.selectedCount > 0) {
        return 'Снять выделение';
      } else {
        return 'Выделить все';
      }
    }
  }
  app.service('ListService', ListService);
}