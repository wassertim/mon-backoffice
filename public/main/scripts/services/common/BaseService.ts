module backOffice.services.common {
    export class BaseService {
        constructor(public $http) { }
      res(url:string) {
        return this.$http.get(url).then(r=>r.data);
      }
      req(conf:any) {
        return this.$http(conf).then(r=>r.data);
      }
    }
}