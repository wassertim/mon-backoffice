module backOffice.services.common {
  export interface INavService {
    isLogin():any
    lastIndex(array):any
    isActive(path):any
    getActive(path):any
  }
}