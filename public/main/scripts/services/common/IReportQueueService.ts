module backOffice.services.common {
  export interface IReportQueueService {
    getQueueStatistic():ng.IPromise<any>
    getReport():ng.IPromise<any>
    saveReport():ng.IPromise<any>
    addConfigurationToQueue(config):ng.IPromise<any>
    getConfigurationsOnControl():ng.IPromise<any>
    deleteConfigurationFromQueue(configId):ng.IPromise<any>
    deleteConfigurationFromQueueBatch(configIds):ng.IPromise<any>
    forceReportRefresh(reportId:number):ng.IPromise<any>
  }
}