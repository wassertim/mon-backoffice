module backOffice.services.common {
  export interface IConfigService {
    getConfigurationsByReportId(reportId:number):ng.IPromise<any>
    getAllPublishTerminalConfiguration():ng.IPromise<any>
  }
}
