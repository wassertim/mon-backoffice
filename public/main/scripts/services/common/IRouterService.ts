module backOffice.services.common {
  export interface IRouterService {
    action(controller: string, action: string, params?: any):ng.IRequestConfig;
  }
}