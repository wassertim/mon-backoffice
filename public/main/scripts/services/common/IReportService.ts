module backOffice.services.common {
  export interface IReportService {
    getReportInfo(reportId:number): ng.IPromise<any>
  }
}