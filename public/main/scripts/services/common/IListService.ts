module backOffice.services.common {
  export interface IListService {
    deletedCount:number;
    selectedCount:number;
    toggleSelectButtonText();
    updateSelectedCount();
    getSelectedItems();
    getSelectedIds();
    deleteItemsFromList(ids:number[]);
    deleteSelectedItems();
    selectItem(item, e?);
    invertSelected(e);
    toggleSelected(e)
  }
}