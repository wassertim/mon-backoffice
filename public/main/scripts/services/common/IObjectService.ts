module backOffice.services.common {
  export interface IObjectService {
    getByArticle(articleId:number):any
    getObjectById(objectId:number):any
    getObjectAtomInfo(articleId: number, objectId: number): any
  }
}
