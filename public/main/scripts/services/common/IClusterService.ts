module backOffice.services.common {
  export interface IClusterService {
    getCounters(reportId:number, period:any): ng.IPromise<any>
    getByReport(reportId:number, period:any, isVerifiedExcluded:boolean, pageSize?:number, pageNumber?:number):ng.IPromise<any>
    getById(clusterId):ng.IPromise<any>
    getDeletedFromReport(reportId:number, period:any, pageSize?:number, pageNumber?:number):ng.IPromise<any>
    getDeletedFromReportCount(reportId:number, period:any):ng.IPromise<any>
    getClusterCount(reportId:number, period:any, isVerifiedExcluded:number):ng.IPromise<any>
    restoreToReport(reportId:number, articleIds:number[]):ng.IPromise<any>
    restoreEverywhere(articleIds):ng.IPromise<any>
    deleteEverywhere(articleIds:number[]):ng.IPromise<any>
    deleteFromReport(articleIds:number[], reportId:number):ng.IPromise<any>
    setImage(clusterId:number, imageId:number):ng.IPromise<any>
    getImages(clusterId:number):ng.IPromise<any>
    setTitle(clusterId:number, title:string):ng.IPromise<any>
    setMainArticle(clusterId, articleId):ng.IPromise<any>
  }
}
