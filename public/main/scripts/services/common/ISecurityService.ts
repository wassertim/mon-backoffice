module backOffice.services.common {
  export interface ISecurityService {
    signOff();
    signIn(auth);
  }
}