module backOffice.services.common {
  export interface IArticleService {
    getCounters(reportId:number, period:any): ng.IPromise<any>
    getByReport(reportId:number, period:any, isVerifiedExcluded:boolean, pageSize?:number, pageNumber?:number): ng.IPromise<any>
    getByReportCount(reportId:number, isVerifiedExcluded:boolean, period:any): ng.IPromise<any>
    getDeletedFromReportCount(reportId:number, period:any): ng.IPromise<any>
    getDeletedFromReport(reportId:number, period:any, pageSize?:number, pageNumber?:number): ng.IPromise<any>
    getById(articleId:number, reportId?:number, imgW?:number, imgH?:number): ng.IPromise<any>
    getItemById(articleId:number, reportId?:number, imgW?:number, imgH?:number): ng.IPromise<any>
    setImage(articleId:number, imageId:number): ng.IPromise<any>
    getMore(articleId:number): ng.IPromise<any>
    restoreToReport(articleIds, reportId:number): ng.IPromise<any>
    restoreEverywhere(articleIds): ng.IPromise<any>
    deleteEverywhere(articleIds): ng.IPromise<any>
    deleteFromReport(articleIds, reportId:number): ng.IPromise<any>
    getPdfAttachments(articleId:number): ng.IPromise<any>
    getUrls(pdfs:any): any
    getByObject(objectId: number, sourceFilter: number, emotionFilter: number, query: string, pageNumber: number, pageSize: number);
  }
}
