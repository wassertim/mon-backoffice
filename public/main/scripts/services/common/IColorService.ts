module backOffice.services.common {
  export interface IColorService {
    toRGB(num:number):any
    fontColor(color:any):any
    toColor(color:any):any
  }
}