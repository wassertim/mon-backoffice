module backOffice.services.common {
  export interface IClusterArticleService {
    getList(clusterId:number, period, isVerifiedExcluded:boolean, pageSize?:number, pageNumber?:number):any
    getRelevantList(clusterId:number):any
    getCounters(clusterId:number, mainArticleId: number)
    getCountersForMoreArticles(articleId:number)
    getDeletedCount(clusterId:number):any
    deleteArticles(ids:number[], clusterId:number)
    getDeletedFromCluster(clusterId:number)
    restoreToCluster(ids:number[], clusterId:number)
    getFilteredList(clusterId:number, period, emotion:string, source:string, sort:string, pageSize:number, pageNumber:number)
    getFilteredListForArticles(articleId:number, period, emotion:string, source:string, sort:string, pageSize:number, pageNumber:number)
    getFilteredListForArticlesWithFilter(articleId: number, period: any, filter: any, pageSize: number, pageNumber:any)
  }
}
