///<reference path="common/IColorService.ts"/>
///<referenct path="../global.ts"/>
module backOffice.services {
  export class ColorService implements common.IColorService {

    toRGB(num:number):any {
      num >>>= 0;
      var b = num & 0xFF,
          g = (num & 0xFF00) >>> 8,
          r = (num & 0xFF0000) >>> 16,
          a = ((num & 0xFF000000) >>> 24) / 255;
      return {
        R: r,
        G: g,
        B: b
      };
    }

    fontColor(color:any):any {
      var d = 0;
      var a = 1 - (0.299 * color.R + 0.587 * color.G + 0.114 * color.B) / 255;

      if (a < 0.5)
        d = 0; // bright colors - black font
      else
        d = 255; // dark colors - white font
      return { R: d, G: d, B: d };
    }

    toColor(color:any):any {
      return "rgb(" + [color.R, color.G, color.B].join(",") + ")";
    }

  }
  app.service('ColorService', ColorService);
}