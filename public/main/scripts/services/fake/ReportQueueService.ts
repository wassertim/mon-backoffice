///<reference path="../../global.ts" />
///<reference path="../common/IReportQueueService.ts"/>
module backOffice.services.fake {
  export class ReportQueueService extends common.BaseService implements services.common.IReportQueueService {

    public static $inject = ['$http'];

    constructor(public $http:ng.IHttpService) {
      super($http);
    }


    getQueueStatistic():ng.IPromise<any> {
      return this.res('public/main/data/report-queue/statistics.json');
    }

    getReport():ng.IPromise<any> {
      return this.$http.get('public/main/data/report-queue/report-info.json').then(r=>{
        r.data.isLoaded = true;
        return r.data
      });
    }

    saveReport():ng.IPromise<any> {
      return undefined;
    }

    addConfigurationToQueue(config):ng.IPromise<any> {
      return undefined;
    }

    getConfigurationsOnControl():ng.IPromise<any> {
      return this.res('public/main/data/settings/configs-on-control.json');
    }

    deleteConfigurationFromQueue(configId):ng.IPromise<any> {
      return undefined;
    }

    deleteConfigurationFromQueueBatch(configIds):ng.IPromise<any> {
      return undefined;
    }

    forceReportRefresh(reportId:number):ng.IPromise<any> {
      return undefined;
    }


  }
}
app.service('ReportQueueService', backOffice.services.fake.ReportQueueService);
