///<reference path="../common/ISecurityService.ts" />
module backOffice.services.fake {
  export class SecurityService extends services.common.BaseService implements services.common.ISecurityService {

    public static $inject = ['$http'];

    constructor(public $http) {
      super($http);
    }

    signOff() {
      return this.res('public/main/data/article/deleted-count.json');
    }

    signIn(auth) {
      return this.res('public/main/data/article/deleted-count.json').then(()=> {
        return auth.name === 'dev_glass2' && auth.password === 'test'
      });
    }
  }
  app.service('SecurityService', SecurityService);
}