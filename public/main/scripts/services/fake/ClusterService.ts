///<reference path="../common/IClusterService.ts"/>
module backOffice.services.fake {
  export class ClusterService extends common.BaseService implements common.IClusterService{
    public static $inject = ['$http'];

    constructor(public $http: ng.IHttpService){
      super($http);
    }

    getCounters(reportId:number, period:any):ng.IPromise<any> {
      return this.$http.get('public/main/data/cluster/counters.json').then(r=>r.data);
    }

    getByReport(reportId:number, period:any, isVerifiedExcluded:boolean, pageSize?:number, pageNumber?:number):ng.IPromise<any> {
      return this.$http.get('public/main/data/cluster/list.json').then(r=>{
        r.data.isLoaded = true;
        return r.data;
      });
    }

    getById(clusterId):ng.IPromise<any> {
      return this.res('public/main/data/cluster/display.json');
    }

    getDeletedFromReport(reportId:number, period:any, pageSize?:number, pageNumber?:number):ng.IPromise<any> {
      return this.res('public/main/data/cluster/deleted-list.json');
    }

    getDeletedFromReportCount(reportId:number, period:any):ng.IPromise<any> {
      return this.res('public/main/data/cluster/deleted-count.json');
    }

    getClusterCount(reportId:number, period:any, isVerifiedExcluded:number):ng.IPromise<any> {
      return undefined;
    }

    restoreToReport(reportId:number, articleIds:number[]):ng.IPromise<any> {
      return this.res('public/main/data/cluster/deleted-count.json');
    }

    restoreEverywhere(articleIds):ng.IPromise<any> {
      return this.res('public/main/data/cluster/deleted-count.json');
    }

    deleteEverywhere(articleIds:number[]):ng.IPromise<any> {
      return this.res('public/main/data/cluster/deleted-count.json');
    }

    deleteFromReport(articleIds:number[], reportId:number):ng.IPromise<any> {
      return this.res('public/main/data/cluster/deleted-count.json');
    }

    setImage(clusterId:number, imageId:number):ng.IPromise<any> {
      return undefined;
    }

    getImages(clusterId:number):ng.IPromise<any> {
      return this.res('public/main/data/cluster/images.json');
    }

    setTitle(clusterId:number, title:string):ng.IPromise<any> {
      return undefined;
    }

    setMainArticle(clusterId, articleId):ng.IPromise<any> {
      return this.res('public/main/data/cluster/deleted-count.json');
    }

  }
  app.service('ClusterService', ClusterService);
}
