///<reference path="../common/IClusterArticleService.ts"/>
module backOffice.services.fake {
  export class ClusterArticleService extends common.BaseService implements common.IClusterArticleService {
    public static $inject = ['$http'];
    constructor(public $http: ng.IHttpService){
      super($http);
    }

    getList(clusterId:number, period, isVerifiedExcluded:boolean, pageSize?:number, pageNumber?:number):any {

    }

    getRelevantList(clusterId:number):any {
      return this.res('public/main/data/cluster/article/relevant-list.json');
    }

    getCounters(clusterId:number) {
      return this.res('public/main/data/cluster/article/counters.json');
    }

    getCountersForMoreArticles(articleId:number) {
      return this.res('public/main/data/article/display/counters.json');
    }

    getDeletedCount(clusterId:number):any {
      return this.res('public/main/data/cluster/article/');
    }

    deleteArticles(ids:number[], clusterId:number) {
    }

    getDeletedFromCluster(clusterId:number) {
    }

    restoreToCluster(ids:number[], clusterId:number) {
    }

    getFilteredList(clusterId:number, period, emotion:string, source:string, sort:string, pageSize:number, pageNumber:number) {
      return this.res('public/main/data/cluster/article/list.json');
    }

    getFilteredListForArticles(articleId:number, period, emotion:string, source:string, sort:string, pageSize:number, pageNumber:number) {
      return this.res('public/main/data/article/display/get-more.json');
    }

    getFilteredListForArticlesWithFilter(articleId:number, period:any, filter:any, pageSize:number, pageNumber:any) {
      return this.getFilteredListForArticles(articleId, period, filter.emotion, filter.source, filter.sort, pageSize, pageNumber);
    }
  }
  app.service('ClusterArticleService', ClusterArticleService);
}
