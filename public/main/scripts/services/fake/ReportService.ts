///<reference path="../common/IReportService.ts"/>
module backOffice.services.fake {
  export class ReportService implements services.common.IReportService {

    public static $inject = ['$http'];
    constructor(private $http: ng.IHttpService){}

    getReportInfo(reportId:number):ng.IPromise<any> {
      return this.$http.get('public/main/data/report-queue/report-info.json').then(r => {
        r.data.isLoaded = true;
        return r.data;
      });
    }
  }
  app.service('ReportService', ReportService);
}
