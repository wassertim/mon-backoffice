///<reference path="../../global.ts" />
///<reference path="../common/IConfigService.ts"/>
module backOffice.services.fake {
  export class ConfigService extends common.BaseService implements services.common.IConfigService {

    public static $inject = ['$http'];

    constructor(public $http:ng.IHttpService) {
      super($http);
    }

    getConfigurationsByReportId(reportId:number):ng.IPromise<any> {
      return this.res('public/main/data/config/by-report.json');
    }

    getAllPublishTerminalConfiguration():ng.IPromise<any> {
      return this.res('public/main/data/settings/all-terminal.json');
    }

  }
}
app.service('ConfigService', backOffice.services.fake.ConfigService);
