///<reference path="../common/IRouterService.ts"/>
module backOffice.services {
  export class RouterService implements services.common.IRouterService {
    action(controller: string, action: string, params?: any):ng.IRequestConfig {
      return {
        url: '',
        method: 'GET'
      }
    }
  }
}
app.service("RouterService", backOffice.services.RouterService);
