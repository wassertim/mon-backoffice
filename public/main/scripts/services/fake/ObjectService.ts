///<reference path="../../global.ts"/>
///<reference path="../common/IObjectService.ts"/>
module backOffice.services.fake {
  export class ObjectService extends common.BaseService implements services.common.IObjectService {

    public static $inject = ['$http', '$timeout'];
    constructor(public $http:ng.IHttpService, private $timeout: any){
      super($http);
    }
    getByArticle(articleId:number):any {
      return this.res('public/main/data/article/objects.json');
    }
    getObjectById(objectId:number):any {
      return this.res('public/main/data/object/'+objectId+'/info.json');
    }

    getObjectAtomInfo(articleId:number, objectId:number):any {
      return this.$timeout(() => {
        return this.res('public/main/data/object/object-atom-info.json');
      }, 500);
    }
  }
  app.service('ObjectService', ObjectService);
}
