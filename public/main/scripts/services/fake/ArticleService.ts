///<reference path="../../global.ts"/>
///<reference path="../common/BaseService.ts"/>
///<reference path="../common/IArticleService.ts"/>
module backOffice.services.fake {
  export class ArticleService extends common.BaseService implements common.IArticleService {


    public static $inject = ['$http', '$timeout'];

    constructor(public $http:ng.IHttpService, private $timeout) {
      super($http);
    }

    getCounters(reportId:number, period:any):ng.IPromise<any> {
      return this.res('public/main/data/article/counters.json');
    }

    getByReport(reportId:number, period:any, isVerifiedExcluded:boolean, pageSize?:number, pageNumber?:number):ng.IPromise<any> {
      return this.$timeout(()=> {
        return this.res('public/main/data/article/list.json');
      }, 500);
    }

    getByReportCount(reportId:number, isVerifiedExcluded:boolean, period:any):ng.IPromise<any> {
      return undefined;
    }

    getDeletedFromReportCount(reportId:number, period:any):ng.IPromise<any> {
      return this.res('public/main/data/article/deleted-count.json');
    }

    getDeletedFromReport(reportId:number, period:any, pageSize?:number, pageNumber?:number):ng.IPromise<any> {
      return this.res('public/main/data/article/deleted-list.json');
    }

    getById(articleId:number, reportId?:number, imgW?:number, imgH?:number):ng.IPromise<any> {
      return this.res('public/main/data/article/display.json');
    }

    getItemById(articleId:number, reportId:number, imgW?:number, imgH?:number):ng.IPromise<any> {
      return this.res('public/main/data/article/display.json');
    }

    setImage(articleId:number, imageId:number):ng.IPromise<any> {
      return undefined;
    }

    getMore(articleId:number):ng.IPromise<any> {
      return this.res('public/main/data/article/display/get-more.json');
    }

    restoreToReport(articleIds, reportId:number):ng.IPromise<any> {
      return this.res('public/main/data/article/deleted-count.json');
    }

    restoreEverywhere(articleIds):ng.IPromise<any> {
      return this.res('public/main/data/article/deleted-count.json');
    }

    deleteEverywhere(articleIds):ng.IPromise<any> {
      return this.res('public/main/data/article/deleted-count.json');
    }

    deleteFromReport(articleIds, reportId:number):ng.IPromise<any> {
      return this.res('public/main/data/article/deleted-count.json');
    }

    getPdfAttachments(articleId:number):ng.IPromise<any> {
      return undefined;
    }

    getUrls(pdfs:any):any {
      return undefined;
    }

    getByObject(objectId:number, sourceFilter:number, emotionFilter:number, query:string, pageNumber:number, pageSize:number) {
      return this.res('public/main/data/article/display/get-more.json');
    }
  }
  app.service('ArticleService', ArticleService);
}
