///<reference path="../../global.ts" />
module backOffice.services {
  export class ConfigService implements common.IConfigService {
    public static $inject = ['$http', 'RouterService'];

    constructor(private $http:any, private routerService: services.common.IRouterService) {
    }

    getConfigurationsByReportId(reportId:number):any {
      return this.$http(this.routerService.action("Configuration", "GetConfigurationsByReportId", { reportId: reportId })).then(response => {
        return response.data;
      });
    }

    getAllPublishTerminalConfiguration():any {
      return this.$http(this.routerService.action("Configuration", "GetAllPublishTerminalConfiguration")).then((response:any) => {
        return response.data;
      });
    }
  }
  app.service('ConfigService', ConfigService);
}
