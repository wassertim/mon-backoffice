///<reference path="../common/IRouterService.ts"/>
module backOffice.services {
  export class RouterService implements services.common.IRouterService {
    STRIP_COMMENTS:any = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
    ARGUMENT_NAMES = /([^\s,]+)/g;

    getParamNames(func) {
      var fnStr = func.toString().replace(this.STRIP_COMMENTS, '');
      var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(this.ARGUMENT_NAMES);
      if (result === null)
        result = [];
      return result
    }
    toCamel(str) {
      var monb = str;
      if (monb.length <= 1) {
        monb = monb.toLowerCase();
      } else {
        monb = monb.substring(0, 1).toLowerCase() + monb.substring(1);
      }
      return monb;
    }
    action(controller: string, action: string, params?: any):any {
      var obj = jsRoutes.controllers[controller];
      var fn = obj[this.toCamel(action)];
      var paramNames = this.getParamNames(fn);
      var values = [];
      for (var i = 0; i < paramNames.length; i++) {
        values.push(params[paramNames[i]]);
      }
      return fn.apply(obj, values);
    }
  }
}
app.service("RouterService", backOffice.services.RouterService);
