///<reference path="../common/ISecurityService.ts" />
module backOffice.services.http {
  export class SecurityService implements services.common.ISecurityService {

    public static $inject = ['$http', 'RouterService'];

    constructor(private $http, private routerService: services.common.IRouterService) {
    }

    signOff() {
      return this.$http(this.routerService.action('Security', 'SignOut')).then((r) => r.data);
    }

    signIn(auth) {
      if (!auth.isPermanent) auth.isPermanent = false;
      return this.$http(angular.extend(this.routerService.action('Security', 'SignIn'), {data:auth})).then((r) => {
        return r.data;
      });
    }
  }
  app.service('SecurityService', SecurityService);
}