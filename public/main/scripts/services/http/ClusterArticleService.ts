///<reference path="../common/BaseService.ts"/>
///<reference path="../common/IClusterArticleService.ts"/>
module backOffice.services {
  export class ClusterArticleService extends common.BaseService implements common.IClusterArticleService {
    public static $inject: string[] = ['$http', 'RouterService'];
    constructor(public $http:any, private routerService) {
      super($http);
    }

    getList(clusterId:number, period, isVerifiedExcluded:boolean, pageSize?:number, pageNumber?:number):any {
      return this.$http(Router.action("ClusterArticle", "GetList", {
            clusterId: clusterId,
            pageSize: pageSize,
            pageNumber: pageNumber
          })
      ).then((r:any) => r.data);
    }

    getRelevantList(mainArticleId:number):any {
      return this.$http(this.routerService.action("ClusterArticle", "GetRelevantList", { mainArticleId: mainArticleId })).then((r:any) => r.data);
    }

    getCounters(clusterId:number, mainArticleId: number) {
      return this.$http(this.routerService.action("ClusterArticle", "GetCounters", { clusterId: clusterId, mainArticleId: mainArticleId })).then((r:any) => r.data);
    }

    getCountersForMoreArticles(articleId:number) {
      return this.$http(this.routerService.action('Article', "GetCountersForMoreArticles", { articleId: articleId })).then((r:any) => r.data);
    }

    getDeletedCount(clusterId:number):any {
      return this.$http(this.routerService.action("ClusterArticle", "GetDeletedCount", { clusterId: clusterId })).then(r => r.data);
    }

    deleteArticles(ids:number[], clusterId:number) {
      return this.$http(angular.extend(this.routerService.action("ClusterArticle", "DeleteArticles", { clusterId: clusterId }), {
        data: ids,
        headers: {
          'Content-Type': 'application/json'
        }
      })).then(r => r.data);
    }

    getDeletedFromCluster(clusterId:number) {
      return this.$http(this.routerService.action("ClusterArticle", "GetDeletedFromCluster", { clusterId: clusterId })).then((r:any) => r.data);
    }

    restoreToCluster(ids:number[], clusterId:number) {
      return this.$http(angular.extend(this.routerService.action("ClusterArticle", "RestoreToCluster", { clusterId: clusterId }), {data: ids})).then(r => r.data);
    }

    getFilteredList(clusterId:number, period, emotion:string, source:string, sort:string, pageSize = 20, pageNumber = 1) {
      return this.$http(Router.action('Article', 'GetFilteredList', {
        period: period,
        clusterId: clusterId,
        emotion: emotion,
        source: source,
        sort: sort,
        pageSize: pageSize,
        pageNumber: pageNumber
      })).then(r=> r.data);
    }

    getFilteredListForArticles(articleId:number, period, emotion:string, source:string, sort:string, pageSize = 20, pageNumber = 1) {
      return this.$http(this.routerService.action('Article', 'GetFilteredListForArticles', {
        period: period,
        articleId: articleId,
        emotion: emotion,
        source: source,
        sort: sort,
        pageSize: pageSize,
        pageNumber: pageNumber
      })).then(r=> r.data);
    }

    getFilteredListForArticlesWithFilter(articleId:number, period:any, filter:any, pageSize:number, pageNumber:any) {
      return this.getFilteredListForArticles(articleId, period, filter.emotion, filter.source, filter.sort, pageSize, pageNumber);
    }

  }
  app.service('ClusterArticleService', ClusterArticleService);
}
