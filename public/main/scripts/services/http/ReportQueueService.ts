///<reference path="../../typings/router.d.ts"/>
///<reference path="../../typings/angularjs/angular.d.ts" />
///<reference path="../../global.ts" />
module backOffice.services {
  export class ReportQueueService implements common.IReportQueueService {
    private controller = "ReportQueue";
    public static $inject = ['$http', 'RouterService'];

    constructor(private $http:ng.IHttpService, private routerService) {
    }

    getQueueStatistic():any {
      return this.$http(this.routerService.action("ReportQueue", "GetQueueStatistic")).then((response) => {
        return response.data;
      });
    }

    getReport():any {
      return this.$http(this.routerService.action(this.controller, "GetReport")).then(
          (success:any) => {
            success.data.isLoaded = true;
            return success.data;
          }
      );
    }

    saveReport():any {
      return this.$http(this.routerService.action(this.controller, "SaveReport")).then(
          status => {
            return status.data;
          },
          error => {
            return error.data;
          }
      );
    }

    addConfigurationToQueue(config):any {
      return this.$http(angular.extend(this.routerService.action(this.controller, "AddConfigurationToQueue"), {data:config})).then(response => {
        return response.data;
      });
    }

    getConfigurationsOnControl():any {
      return this.$http(this.routerService.action(this.controller, "GetConfigurationsOnControl")).then(response => {
        return response.data;
      });
    }

    deleteConfigurationFromQueue(configId):any {
      return this.$http(this.routerService.action(this.controller, "DeleteConfigurationFromQueue", { id: configId })).then(response => {
        return response.data;
      });
    }

    deleteConfigurationFromQueueBatch(configIds):any {
      return this.$http(angular.extend(Router.action(this.controller, "DeleteConfigurationFromQueueBatch"), {data:configIds})).then(response => {
        return response.data;
      });
    }

    forceReportRefresh(reportId:number) {
      return this.$http(this.routerService.action(this.controller, "ForceReportRefresh", { reportId: reportId })).then(r => r.data);
    }
  }
  app.service('ReportQueueService', ReportQueueService);
}

