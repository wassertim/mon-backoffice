///<reference path="../common/BaseService.ts"/>
module backOffice.services {
  export class ArticleService extends common.BaseService implements common.IArticleService {


    private articleController = "Article";
    public static $inject = ['$http', 'RouterService'];

    constructor(public $http:any, private routerService: services.common.IRouterService) {
      super($http);
    }

    getCounters(reportId:number, period:any):ng.IPromise<any> {
      return this.$http(this.routerService.action('Article', "GetCounters", { reportId: reportId, period: period })).then((r:any) => {
        return r.data;
      });
    }

    getByReport(reportId:number, period:any, isVerifiedExcluded:boolean, pageSize?:number, pageNumber?:number):ng.IPromise<any> {
      return this.$http(
          this.routerService.action('Article', 'GetByReport',
              {
                reportId: reportId,
                period: period,
                isVerifiedExcluded: isVerifiedExcluded,
                pageSize: pageSize,
                pageNumber: pageNumber
              }
          )
      ).then((response) => response.data);
    }

    getByReportCount(reportId:number, isVerifiedExcluded:boolean, period:any):ng.IPromise<any> {
      return this.$http(Router.action(this.articleController, "GetByReportCount", { reportId: reportId, period: period, isVerifiedExcluded: isVerifiedExcluded })).then(
          response => {
            return response.data;
          }
      );
    }

    getDeletedFromReportCount(reportId:number, period:any):ng.IPromise<any> {
      return this.$http(this.routerService.action('Article', 'GetDeletedFromReportCount', { reportId: reportId, period: period })).then(response => {
        return response.data;
      });
    }

    getDeletedFromReport(reportId:number, period:any, pageSize?:number, pageNumber?:number):ng.IPromise<any> {
      return this.$http(this.routerService.action('Article', 'GetDeletedFromReport', { reportId: reportId, pageSize: pageSize, pageNumber: pageNumber, period: period })).then(
          response=> {
            return response.data;
          }
      );
    }

    getById(articleId:number, reportId?:number, imgW?:number, imgH?:number):ng.IPromise<any> {
      return this.$http(this.routerService.action('Article', "GetById", { articleId: articleId, reportId: reportId }))
          .then(response => response.data);
    }

    getItemById(articleId:number, reportId:number, imgW?:number, imgH?:number):ng.IPromise<any> {
      return this.$http(this.routerService.action('Article', "GetItemById", { articleId: articleId, reportId: reportId }))
        .then(response => response.data);
    }

    setImage(articleId:number, imageId:number):ng.IPromise<any> {
      return this.$http(angular.extend(this.routerService.action("Article", "SetImage", { articleId: articleId }), {data: { imageId: imageId }})).then(response => {
        return response.data;
      });
    }

    getMore(articleId:number):ng.IPromise<any> {
      return this.$http(Router.action("Article", "GetMore", { articleId: articleId })).then(response => response.data);
    }

    restoreToReport(articleIds, reportId:number):ng.IPromise<any> {
      return this.$http(angular.extend(this.routerService.action("Article", "RestoreToReport", {reportId: reportId}), {data:articleIds})).then(r=>r.data);
    }

    restoreEverywhere(articleIds):ng.IPromise<any> {
      return this.$http(angular.extend(this.routerService.action("Article", "RestoreEverywhere"), {data:articleIds})).then(r=>r.data);
    }

    deleteEverywhere(articleIds):ng.IPromise<any> {
      return this.$http(angular.extend(this.routerService.action("Article", "DeleteEverywhere"), {
        data:articleIds,
        headers: {
          'Content-Type': 'application/json'
        }
      })).then(r=>r.data);
    }

    deleteFromReport(articleIds, reportId:number):ng.IPromise<any> {
      return this.$http(angular.extend(this.routerService.action("Article", "DeleteFromReport", {reportId: reportId}), {
        data:articleIds,
        headers: {
          'Content-Type': 'application/json'
        }
      })).then(r=>r.data);
    }
    getPdfAttachments(articleId:number):ng.IPromise<any> {
      return this.$http(this.routerService.action("Article", "GetPdfAttachments", { articleId: articleId })).then(r=> {
        if (r.data && r.data.length > 0)
          return this.getUrls(r.data);
        return "";
      });
    }

    getUrls(pdfs:any):any {
      return backOffice.Global.pdfUrl + '?name=' + pdfs[0].name.replace('.pdf', '');
    }

    getByObject(objectId:number, sourceFilter:number, emotionFilter:number, query:string, pageNumber:number, pageSize:number) {
      return this.req(this.routerService.action('Article', 'GetByObject', {
        objectId: objectId,
        sourceFilter: sourceFilter,
        emotionFilter: emotionFilter,
        query: query,
        pageNumber: pageNumber,
        pageSize: pageSize
      }));
    }

  }
  app.service('ArticleService', ArticleService);
}
