module backOffice.services {
  export class ObjectService extends common.BaseService implements common.IObjectService {
    public static $inject = ['$http', 'RouterService'];

    constructor(public $http:any, private routerService) {
      super($http);
    }

    getByArticle(articleId:number):any {
      return this.req(this.routerService.action("Object", "GetByArticle", { articleId: articleId }));
    }

    getObjectById(objectId:number):any {
      return this.req(angular.extend(this.routerService.action("Object", "GetObjectById", { objectId: objectId }), {cache: true}));
    }

    getObjectAtomInfo(articleId:number, objectId:number):any {
      return this.req(angular.extend(this.routerService.action("Object", "GetObjectAtomInfo", {objectId: objectId, articleId: articleId}), {cache:true}));
    }
  }
  app.service('ObjectService', ObjectService);
}
