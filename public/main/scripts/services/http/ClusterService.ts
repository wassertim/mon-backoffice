///<reference path="../common/BaseService.ts"/>
///<reference path="../common/IClusterService.ts"/>
///<reference path="../../typings/lodash/lodash.d.ts"/>
module backOffice.services {
  export class ClusterService extends common.BaseService implements common.IClusterService {
    public static $inject = ['$http', 'RouterService'];

    constructor(public $http:any, private routerService: services.common.IRouterService) {
      super($http);
    }

    getCounters(reportId:number, period:any) {
      return this.$http(this.routerService.action("Cluster", "GetCounters", { reportId: reportId, period: period })).then((r:any) => {
        return r.data;
      });
    }

    getByReport(reportId:number, period:any, isVerifiedExcluded:boolean, pageSize?:number, pageNumber?:number):any {
      return this.$http(this.routerService.action("Cluster", "GetByReport", { reportId: reportId, period: period, isVerifiedExcluded: isVerifiedExcluded, pageSize: pageSize, pageNumber: pageNumber }))
          .then((r:any) => {
            _.forEach(r.data.items, (item:any) => {
              item.selected = false;
            });
            r.data.isLoaded = true;
            return r.data;
          });
    }

    getById(clusterId):any {
      return this.$http(this.routerService.action("Cluster", "GetById", { clusterId: clusterId })).then((r:any) => r.data);
    }

    getDeletedFromReport(reportId:number, period:any, pageSize?:number, pageNumber?:number) {
      return this.$http(this.routerService.action("Cluster", "GetDeletedFromReport", { reportId: reportId, pageSize: pageSize, pageNumber: pageNumber, period: period }))
          .then((r:any) => r.data);
    }

    getDeletedFromReportCount(reportId:number, period:any):any {
      return this.$http(this.routerService.action('Cluster', "GetDeletedFromReportCount", { reportId: reportId, period: period })).then((r:any) => r.data);
    }

    getClusterCount(reportId:number, period:any, isVerifiedExcluded:number):any {
      return this.$http(Router.action("Cluster", "GetClusterCount", { reportId: reportId, period: period, isVerifiedExcluded: isVerifiedExcluded }))
          .then((r:any) => r.data);
    }

    restoreToReport(reportId:number, articleIds:number[]):any {
      var ids: any;
      if (articleIds.length === undefined)
        ids = [articleIds];
      else ids = articleIds;
      return this.$http(angular.extend(this.routerService.action("Cluster", "RestoreToReport", {reportId: reportId}), {data: ids}))
          .then((r:any) => r.data);
    }

    restoreEverywhere(articleIds):any {
      return this.$http(angular.extend(this.routerService.action("Cluster", "RestoreEverywhere"), {data: articleIds})).then((r:any) =>  r.data);
    }

    deleteEverywhere(articleIds:number[]) {
      return this.$http(angular.extend(this.routerService.action("Cluster", "DeleteEverywhere"), {
        data:articleIds,
        headers: {
          'Content-Type': 'application/json'
        }
      })).then((r:any) => r.data);
    }

    deleteFromReport(articleIds:number[], reportId:number) {
      return this.$http(angular.extend(this.routerService.action("Cluster", "DeleteFromReport", {reportId:reportId}), {
        data:articleIds,
        headers: {
          'Content-Type': 'application/json'
        }
      })).then((r:any) => r.data);
    }

    setImage(clusterId:number, imageId:number):any {
      return this.$http(angular.extend(this.routerService.action("Cluster", "SetImage", {clusterId: clusterId}), {data: {imageId: imageId}})).then((r:any) => r.data);
    }

    getImages(clusterId:number):any {
      return this.$http(this.routerService.action("Cluster", "GetImages", { clusterId: clusterId })).then((r:any) => r.data);
    }

    setTitle(clusterId:number, title:string) {
      return this.$http(angular.extend(this.routerService.action("Cluster", "SetTitle", { clusterId: clusterId }), {data:{title:title}})).then(
          (r:any) => {
            return r.data;
          },
          (error) => {
            return error;
          }
      );
    }

    setMainArticle(clusterId, articleId):any {
      return this.$http(angular.extend(this.routerService.action("ClusterArticle", "SetMainArticleId", { clusterId: clusterId }), {data: { 'articleId': articleId }}))
          .then((r:any) => r.data);
    }
  }
  app.service('ClusterService', ClusterService);
}