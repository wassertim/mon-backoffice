module backOffice.services {
  export class ReportService implements common.IReportService {
    public static $inject = ["$http"];

    constructor(private $http:any) {
    }

    getReportInfo(reportId:number) {
      return this.$http(Router.action("ReportQueue", "GetReport"))
          .then((response:any) => {
            response.data.isLoaded = true;
            return response.data;
          });
    }
  }
  app.service('ReportService', ReportService);
}