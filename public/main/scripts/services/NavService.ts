///<reference path="../global.ts"/>
///<reference path="../typings/lodash/lodash.d.ts" />
module backOffice.services {
  export class NavService implements common.INavService {
    public static $inject = ['$location', '$state'];

    constructor(public $location:ng.ILocationService, public $state:ng.ui.IState) {

    }

    isLogin():any {
      return this.$state.current.name === "login";
    }

    lastIndex(array):any {
      return array.length - 1;
    }



    isActiveState(stateName) {
      return stateName === this.$state.current.name;
    }

    isActive(path):any {
      console.log(path);
      var active = _.contains(this.$location.path(), path.replace('#', ''));
      console.log(path + ': isActive? ' + active);
      return active;
    }

    getActive(path):any {
      if (this.isActive(path)) {
        return "active";
      } else {
        return "";
      }
    }
  }
  app.service('NavService', NavService);
}