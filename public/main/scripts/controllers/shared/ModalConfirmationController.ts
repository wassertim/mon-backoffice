module backOffice.controllers.shared {
  export class ModalConfirmationController extends controllers.common.ModalController {
    public static $inject = ['$scope', '$modalInstance', 'params'];
    private params: any;
    constructor($scope, public $modalInstance, params) {
      super($scope, $modalInstance);
      this.params = params;
    }

    ok() {
      this.$modalInstance.close('ok');
    }
  }
}