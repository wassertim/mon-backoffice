///<reference path="../common/ModalController.ts"/>
///<reference path="../article/DeletedInClusterArticleListController.ts" />
///<reference path="../../services/common/IArticleService.ts" />
module backOffice.controllers.cluster {
  export class ModalClusterDisplayController extends DeleteRestoreController {
    mainArticle:any;
    cluster:any;
    isItemsLoaded = true;
    pageSize = 100;
    verifiedHidden = false;
    public itemList = {
      totalCount: 0,
      items: [],
      isLoaded: false
    };
    counters:any;
    deletedCount:number;
    ls:services.common.IListService;
    fatalError:any;
    listMoreFilter = {
      emotion: 0, //All
      source: 0, //All
      sort: 0 //Time
    };
    isDeleting = false;
    public static $inject:string[] = ['$q', '$scope', '$modalInstance', '$modal', 'ClusterService', 'params', 'ArticleService', 'ClusterArticleService', 'Confirmation'];

    constructor(
      private $q,
      $scope,
      public $modalInstance,
      private $modal,
      private clusterService,
      private params,
      private articleService: services.common.IArticleService,
      private clusterArticleService,
      private confirmation
      ) {
      super($scope, $modalInstance, params);
      $scope.vm = this;
      this.cluster = params.cluster;
      this.ls = new services.ListService(() => this.itemList);
      //this.display().then(mainArticleId => {
      this.getMainArticle(this.cluster.mainArticleId).then(()=>{
        this.list(this.cluster.mainArticleId);
      });

      //});

    }

    refresh(e) {
      e.preventDefault();
      this.list(this.cluster.mainArticleId);
    }

    showError(message:string):any {
      this.fatalError = {
        type: 'danger',
        message: message
      };
    }

    openTrash(e) {
      e.preventDefault();
      this.$modal.open({
        templateUrl: 'public/main/partials/article/deleted-list-modal.html',
        controller: controllers.article.DeletedInClusterArticleListController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              clusterId: this.cluster.id,
              report: this.params.report
            }
          }
        }
      });
    }
    isSortedBy(sort:number) {
      return this.listMoreFilter.sort === sort;
    }

    changeSort(sort:number, e) {
      e.preventDefault();
      this.listMoreFilter.sort = sort;
      this.list(this.cluster.mainArticleId);
    }

    deleteConfirmation(e) {
      e.preventDefault();
      this.$modal.open({
        templateUrl: 'public/main/partials/modals/delete-restore.html',
        controller: backOffice.controllers.DeleteRestoreController,
        resolve: {
          params: () => {
            return {
              title: 'Удалить',
              items: this.ls.getSelectedItems(),
              _action: () => this.deleteFromCluster(this.ls.getSelectedIds()),
              actionButtonText: 'Удалить',
              alert: 'Вы действительно хотите удалить эти статьи?'
            };
          }
        }
      });
    }

    deleteSingleArticleFromCluster(articleId: number) {
      return this.confirmation('Вы действительно хотите удалить эту статью из кластера?', 'Удалить', () => {
        return this.deleteFromCluster([articleId]);
      });
    }

    deleteFromCluster(ids):any {
      return this.clusterArticleService.deleteArticles(ids, this.cluster.id).then(() => {
        this.ls.deleteSelectedItems();
      });
    }

    display():any {
      return this.clusterService.getById(this.params.clusterId).then((cluster:any) => {
        if (!cluster || cluster === "null") {
          var errorText = 'Невозможно загрузить данные кластера'
          this.showError(errorText);
          return this.$q.reject(errorText);
        }
        this.cluster = cluster;
        this.getMainArticle(cluster.mainArticleId);
        return cluster.mainArticleId;
      });
    }

    getMainArticle(id:number):any {
      return this.articleService.getItemById(id).then(article => {
        this.mainArticle = article;
      });
    }

    deleteSingleInReport():any {
      if (confirm('Вы действительно хотите удалить эту статью в отчете?')) {
        this.isDeleting = true;
        return this.clusterService.deleteFromReport([this.params.clusterId], this.params.report.reportId).then(()=> {
          this.isDeleting = false;
          this.$modalInstance.close('deleteSingleInReport');
        });
      }
    }

    deleteSingleEverywhere():any {
      if (confirm('Вы действительно хотите удалить эту статью везде?')) {
        this.isDeleting = true;
        this.clusterService.deleteEverywhere([this.params.clusterId]).then(() => {
          this.isDeleting = false;
          this.$modalInstance.close('deleteSingleEverywhere');
        });
      }
    }

    isNegative() {
      return this.listMoreFilter.emotion === 2;
    }

    changeEmotion(e) {
      e.preventDefault();
      this.listMoreFilter.emotion = this.listMoreFilter.emotion === 2 ? 0 : 2;
      this.list(this.cluster.mainArticleId);
    }

    changeSource(source, e) {
      e.preventDefault();
      this.listMoreFilter.source = source;
      this.list(this.cluster.mainArticleId);
    }


    isSource(source) {
      return source === this.listMoreFilter.source;
    }

    openEdit(clusterId, cluster, e) {
      e.preventDefault();
      this.$modal.open({
        templateUrl: 'public/main/partials/cluster/edit-modal.html',
        controller: backOffice.controllers.cluster.ModalClusterEditController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              clusterId: clusterId,
              report: this.params.report,
              cluster: cluster
            }
          }
        }
      });
    }

    openArticle(articleId:number, article, e) {
      e.preventDefault();
      var modal = this.$modal.open({
        templateUrl: 'public/main/partials/article/display-modal.html',
        controller: controllers.article.ModalArticleDisplayController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              articleId: articleId,
              article: article,
              _action: () => this.deleteSingleArticleFromCluster(articleId),
              actionButtonText: 'Удалить в кластере',
              report: this.params.report
            }
          }
        }
      });
      modal.result.then((message) => {
        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
          this.ls.deleteItemsFromList([articleId]);
        }
      }, () => {

      });
    }

    list(mainArticleId):any {
      this.isItemsLoaded = false;
      var period = this.params.report.period;
      var emotion = this.listMoreFilter.emotion;
      var source = this.listMoreFilter.source;
      var sort = this.listMoreFilter.sort;
      return this.clusterArticleService.getFilteredListForArticles(mainArticleId, period, emotion, source, sort, this.pageSize, 1)
        .then((itemList:any) => {
          this.isItemsLoaded = true;
          this.itemList = itemList;
          this.clusterArticleService.getCounters(this.params.clusterId, mainArticleId, period, true).then((counters:any) => {
            this.counters = counters;
          });
          this.clusterArticleService.getDeletedCount(this.params.clusterId, period).then((deletedCount:any) => {
            this.ls.deletedCount = parseInt(deletedCount);
          });
        });
    }
  }
}