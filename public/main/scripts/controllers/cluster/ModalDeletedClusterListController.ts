///<reference path="../common/ModalController.ts"/>
module backOffice.controllers.cluster {
  export class ModalDeletedClusterListController extends common.ModalController {
    ls:services.common.IListService;
    public itemList = {
      totalCount: 0,
      items: [],
      isLoaded: false
    };
    public static $inject:string[] = ['$scope', 'params', 'ClusterService', '$modal', '$modalInstance', 'Confirmation'];

    constructor(
      $scope,
      private params,
      private clusterService:services.common.IClusterService,
      private $modal,
      public $modalInstance,
      private confirmation) {

      super($scope, $modalInstance);
      $scope.vm = this;
      this.ls = new services.ListService(()=>this.itemList);
      this.getClustersTrash();
    }

    getClustersTrash():any {
      return this.clusterService.getDeletedFromReport(this.params.report.reportId, this.params.report.period, 100, 1).then((itemList:any) => {
        itemList.isLoaded = true;
        this.itemList = itemList;
      });
    }

    openArticle(clusterId, cluster, e) {
      e.preventDefault();
      this.$modal.open({
        templateUrl: 'public/main/partials/cluster/display-modal.html',
        controller: backOffice.controllers.cluster.ModalClusterDisplayController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              clusterId: clusterId,
              report: this.params.report,
              cluster: cluster,
              _action: () => this.restoreSingleToReport(clusterId),
              _secondaryAction: () => this.restoreSingleEverywhere(clusterId),
              actionButtonText: 'Восстановить',
              secondaryActionButtonText: 'Восстановить везде'
            }
          }
        }
      });
    }

    openRestoreConfirmation(e) {
      e.preventDefault();
      this.ls.updateSelectedCount();
      if (this.ls.selectedCount) {
        this.$modal.open({
          templateUrl: 'public/main/partials/modals/delete-restore.html',
          controller: backOffice.controllers.DeleteRestoreController,
          resolve: {
            params: () => {
              return {
                title: 'Восстановить',
                items: this.ls.getSelectedItems(),
                _action: () => this.restoreToReport(this.ls.getSelectedIds()),
                _secondaryAction: () => this.restoreEverywhere(this.ls.getSelectedIds()),
                actionButtonText: 'Восстановить',
                secondaryActionButtonText: 'Восстановить везде',
                alert: 'Вы действительно хотите восстановить эти статьи?'
              };
            }
          }
        });
      }
    }

    restoreSingleToReport(clusterId: number) {
      return this.confirmation('Вы действительно хотите восстановить этот кластер в отчете?', 'Восстановить', () => {
        return this.restoreToReport([clusterId]);
      });
    }

    restoreSingleEverywhere(clusterId: number) {
      return this.confirmation('Вы действительно хотите восстановить этот кластер в отчете?', 'Восстановить', () => {
        return this.restoreEverywhere([clusterId]);
      });
    }

    restoreEverywhere(ids: number[]):any {
      return this.clusterService.restoreEverywhere(ids).then(()=>{
        this.ls.deleteSelectedItems();
      });
    }

    restoreToReport(ids: number[]):any {
      return this.clusterService.restoreToReport(this.params.report.reportId, ids).then(()=>{
        this.ls.deleteSelectedItems();
      });
    }
  }
}