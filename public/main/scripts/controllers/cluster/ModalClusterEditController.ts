///<reference path="../common/ModalController.ts" />
module backOffice.controllers.cluster {
  export class ModalClusterEditController extends common.ModalController {
    isItemsLoaded = true;
    public itemList = {
      totalCount: 0,
      items: [],
      isLoaded: false
    };
    imageList:any;
    progress = 0;
    originalCluster:any;
    isSettingMain = false;
    isSettingImage = false;
    imageData:any;
    cluster:any;
    imagelistPage = 1;
    isDeleting = false;
    isUploading = false;
    jsAction:string;
    hasNewImage = false;
    fatalError:any;
    public static $inject:string[] = ['$q', '$scope', '$modalInstance', 'ClusterService', 'params', 'ImageHelper', 'ClusterArticleService', 'RouterService'];

    constructor(private $q, $scope, public $modalInstance, private clusterService, private params, private imageHelper, private clusterArticleService, private routerService) {
      super($scope, $modalInstance);
      $scope.vm = this;
      $scope.$watch("vm.imageData", newVal => {
        if (newVal)
          this.cluster.imageId = newVal.id;
      });
      this.jsAction = routerService.action("Image", "UploadImage").url;
      this.originalCluster = angular.copy(params.cluster);
      this.cluster = params.cluster;
      //this.edit().then((cluster:any)=> {
      this.initCluster(this.cluster)
      this.getRelevantList(this.cluster);
      //});
    }

    initCluster(cluster:any) {
      if (!cluster || (cluster === "null")) {
        var errorText = 'Невозможно загрузить данные кластера'
        this.showError(errorText);
        return this.$q.reject(errorText);
      }
      this.cluster = cluster;
      //this.cluster.imageId = this.imageHelper.getIdFromUrl(this.cluster.imageUrl);
      this.originalCluster = angular.copy(this.cluster);
      this.imageData = { id: cluster.imageId, url: cluster.imageUrl };
      this.clusterService.getImages(this.params.clusterId).then((imageList:any) => {
        this.imageList = imageList;
        this.imageList.pages = this.getPages(imageList.items, 7);
        var currentImage:any = _.find(this.imageList.items, (item:any) => {
          return this.imageHelper.getIdFromUrl(this.cluster.imageUrl) === this.imageHelper.getIdFromUrl(item.url);
        });
        if (currentImage)
          currentImage.selected = true;
      });
      return cluster;
    }

    edit():any {
      return this.clusterService.getById(this.params.clusterId).then(this.initCluster);
    }

    onFileChange():any {
      this.isUploading = true;
    }

    uploadSuccess(response):any {
      this.hasNewImage = true;
      this.imageData = response;
      this.isUploading = false;
    }

    isPrevImageListPageDisabled():any {
      if (this.imageList)
        return this.imagelistPage === 0;
      return false;
    }

    isNextImageListPageDisabled():any {
      if (this.imageList && this.imageList.pages)
        return this.imagelistPage === (this.imageList.pages.length - 1);
      return false;
    }

    prevImgListPage():any {
      if (!this.isPrevImageListPageDisabled())
        this.imagelistPage = this.imagelistPage - 1;
    }

    nextImgListPage():any {
      if (!this.isNextImageListPageDisabled())
        this.imagelistPage = this.imagelistPage + 1;
    }

    setImageButtonText():any {
      return this.isSettingImage ? "Изображение обновляется" : "Сохранить изменения";
    }

    changePictureText():any {
      return this.isUploading ? 'Идет загрузка ...' : 'Сменить картинку';
    }

    getPages(items, pageSize:number):any {
      var pages = [];
      var itms = items;
      var pagesCount = Math.floor(items.length / pageSize) + ((items.length % pageSize) > 0 ? 1 : 0);
      for (var i = 0; i < pagesCount; i++) {
        var page = {
          index: i,
          images: _.take(itms, pageSize)
        };
        itms = _.drop(itms, pageSize);
        pages.push(page);
      }
      return pages;
    }

    selectImage(img):any {
      _.forEach(this.imageList.items, (item:any) => {
        item.selected = false;
      });
      img.selected = !img.selected;
      this.imageData = { id: img.imageId, url: img.url };
    }

    showError(message:string):any {
      this.fatalError = {
        type: 'danger',
        message: message
      };
      //this.$window.alert(message);
    }

    isSaveButtonDisabled():any {
      return !this.imageData ||
        ((this.cluster.imageId === this.originalCluster.imageId) && (this.originalCluster.title === this.cluster.title)) ||
        this.isSettingImage;
    }

    getRelevantList(cluster):any {
      this.isItemsLoaded = false;
      this.clusterArticleService.getRelevantList(this.cluster.mainArticleId).then((itemsList:any) => {
        this.isItemsLoaded = true;
        this.itemList = itemsList;
        var mainArticle:any = _.find(this.itemList.items, (item:any) => {
          return item.id === cluster.mainArticleId;
        });
        if (mainArticle)
          mainArticle.selected = true;
      });
    }

    setAsMain(article):any {
      article.isSettingMain = true;
      _.forEach(this.itemList.items, (item:any) => {
        item.selected = false;
      });
      article.selected = true;
      this.clusterService.setMainArticle(this.params.clusterId, article.id).then(() => {
        this.isSettingMain = false;
        article.isSettingMain = false;
      });
    }

    deleteSingleInReport():any {
      if (confirm('Вы действительно хотите удалить эту статью в отчете?')) {
        this.isDeleting = true;
        return this.clusterService.deleteFromReport([this.params.clusterId], this.params.report.reportId).then(()=> {
          this.isDeleting = false;
          this.$modalInstance.close('deleteSingleInReport');
        });
      }
    }

    deleteSingleEverywhere():any {
      if (confirm('Вы действительно хотите удалить эту статью везде?')) {
        this.isDeleting = true;
        this.clusterService.deleteEverywhere([this.params.clusterId]).then(() => {
          this.isDeleting = false;
          this.$modalInstance.close('deleteSingleEverywhere');
        });
      }
    }

    setImage():any {
      this.cluster.imageId = this.imageData.id;
      //TODO: Добавить обработку ошибок
      if (this.imageData.id !== this.originalCluster.imageId)
        this.setImageBase({ action: this.clusterService.setImage, ref: this.clusterService }, this.cluster.id, this.imageData.id);
      this.clusterService.setTitle(this.cluster.id, this.cluster.title).then(() => {
        this.originalCluster.title = this.cluster.title;
        //TODO: Добавить обработку ошибок
      });
    }
    setImageBase(saveImageFn:backOffice.model.ICallable, id:number, imageId:number):any {
      this.isSettingImage = true;
      saveImageFn.action.call(saveImageFn.ref, id, imageId).then(() => {
        this.progress = 0;
        this.originalCluster.imageId = this.cluster.imageId;
        this.isSettingImage = false;
        this.hasNewImage = false;
      });
    }
  }
}