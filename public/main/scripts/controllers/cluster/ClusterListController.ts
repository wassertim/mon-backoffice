///<reference path="../../services/common/IArticleService.ts"/>
///<reference path="../../services/common/IClusterService.ts"/>
///<reference path="../../services/common/IReportQueueService.ts"/>
///<reference path="../../services/common/IConfigService.ts"/>
///<reference path="ModalDeletedClusterListController.ts"/>
///<reference path="ModalClusterDisplayController.ts"/>
///<reference path="../../helper/ImageHelper.ts"/>
///<reference path="ModalClusterEditController.ts" />
///<reference path="../common/ListController.ts" />
module backOffice.controllers {
  export class ClusterListController extends controllers.common.ListController {
    collapsed = false;
    public counters = { totalCount: 0, verifiedCount: 0 };
    public static $inject = ['$scope', 'ClusterService',
      'ReportQueueService', '$state', 'ImageHelper', 'ConfigService', '$modal', '$window', 'NavService', 'Confirmation'];

    constructor(
        public $scope:any,
        private clusterService:services.common.IClusterService,
        public reportQueueService:services.common.IReportQueueService,
        public $state:ng.ui.IState,        
        private imageHelper:helpers.ImageHelper,
        public configService:services.common.IConfigService,
        private $modal,
        private $window,
        private nav,
        private confirmation
      ) {
      super($scope, configService, reportQueueService, $state);
      this.getClusters();
    }

    isClustersDisabled():any {
      if (!this.report || !this.report.type) return false;
      return this.report.type === 1;
    }

    isArticlesDisabled():any {
      if (!this.report || !this.report.type) return false;
      return this.report.type === 2;
    }

    showError(message:string):any {
      this.$window.alert(message);
    }

    openTrash(e) {
      e.preventDefault();
      var modal = this.$modal.open({
        templateUrl: 'public/main/partials/cluster/deleted-list-modal.html',
        controller: controllers.cluster.ModalDeletedClusterListController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              report: this.report
            };
          }
        }
      });
    }

    getClusters():any {
      this.reportQueueService.getReport().then((report:any) => {
        this.reportId = report.reportId;
        this.report = report;
        this.isItemsLoaded = false;
        var period = this.report.period;
        this.clusterService.getByReport(this.report.reportId, period, this.verifiedHidden, this.pageSize, 1).then((itemList:any) => {
          this.isItemsLoaded = true;
          this.itemList = itemList;
          this.ls.updateSelectedCount();
          this.clusterService.getCounters(this.report.reportId, period).then((counters:any) => {
            this.counters = counters;
          });
          this.clusterService.getDeletedFromReportCount(this.report.reportId, period).then((deletedCount:any) => {
            this.ls.deletedCount = parseInt(deletedCount);
          });
        });
        return report;
      }).then((report) => this.loadConfigurations(report.reportId));
    }

    toggleCollapsed():any {
      this.collapsed = !this.collapsed;
      console.log("collapsed: " + this.collapsed);
    }

    img(src):any {
      return this.imageHelper.getImageUrl(src, 100, 100, 6);
    }

    isClusterListEmpty():any {
      return this.itemList.totalCount === 0;
    }

    openArticle(clusterId, cluster, e) {
      e.preventDefault();
      this.$modal.open({
        templateUrl: 'public/main/partials/cluster/display-modal.html',
        controller: backOffice.controllers.cluster.ModalClusterDisplayController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              clusterId: clusterId,
              report: this.report,
              cluster: cluster,
              _action: () => this.deleteSingleFromReport(clusterId),
              _secondaryAction: () => this.deleteSingleEverywhere(clusterId),
              actionButtonText: 'Удалить в отчете',
              secondaryActionButtonText: 'Удалить везде'
            }
          }
        }
      });
    }

    deleteSingleFromReport(clusterId: number) {
      return this.confirmation('Вы действительно хотите удалить этот кластер из отчета?', 'Удалить', () => {
        return this.deleteFromReport([clusterId]);
      });
    }

    deleteSingleEverywhere(clusterId: number) {
      return this.confirmation('Вы действительно хотите удалить этот кластер везде?', 'Удалить', () => {
        return this.deleteEverywhere([clusterId]);
      });
    }

    openEdit(clusterId, cluster, e) {
      e.preventDefault();
      this.$modal.open({
        templateUrl:'public/main/partials/cluster/edit-modal.html',
        controller: backOffice.controllers.cluster.ModalClusterEditController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              clusterId: clusterId,
              report: this.report,
              cluster: cluster
            }
          }
        }
      });
    }

    deleteConfirmation(e) {
      e.preventDefault();
      this.$modal.open({
        templateUrl: 'public/main/partials/modals/delete-restore.html',
        controller: backOffice.controllers.DeleteRestoreController,
        resolve: {
          params: () => {
            return {
              title: 'Удалить события',
              items: this.ls.getSelectedItems(),
              _action: () => this.deleteFromReport(this.ls.getSelectedIds()),
              _secondaryAction: () => this.deleteEverywhere(this.ls.getSelectedIds()),
              actionButtonText: 'Удалить',
              secondaryActionButtonText: 'Удалить везде',
              alert: 'Вы действительно хотите удалить эти события?'
            };
          }
        }
      });
    }
    loadMore():any {
      this.loadMoreBase(this.reportId, { action: this.clusterService.getByReport, ref: this.clusterService });
    }

    loadMoreBase(id, serviceFunction:backOffice.model.ICallable):any {
      this.isItemsLoaded = false;
      var action = (period) => {
        serviceFunction.action.call(serviceFunction.ref, id, period, this.verifiedHidden, this.pageSize, ++this.currentPage).then((itemList:any)=> {
          this.itemList.items.push.apply(this.itemList.items, itemList.items);
          this.isItemsLoaded = true;
        });
      };
      if (this.report) {
        action(this.report.period);
      } else {
        this.reportQueueService.getReport().then(report=> {
          this.report = report;
          action(this.report.period);
        });
      }
    }

    deleteEverywhere(selectedIds):any {
      return this.clusterService.deleteEverywhere(selectedIds).then(() => {
        this.ls.deleteSelectedItems();
      });
    }

    deleteFromReport(selectedIds):any {
      return this.clusterService.deleteFromReport(selectedIds, this.reportId).then(() => {
        this.ls.deleteSelectedItems();
      });
    }

    refresh(e):any {
      e.preventDefault();
      this.getClusters();
    }

  }
}