///<reference path="../common/ModalController.ts" />
module backOffice.controllers.object {
  export class ModalObjectInfoController extends common.ModalController {
    object:any;
    isObjectLoading = false;
    itemList:any;
    isItemsLoaded = true;
    ls:services.common.IListService;
    listMoreFilter = {
      emotion: 0,
      source: 0,
      query: ''
    };
    public static $inject:string[] = ['$scope', '$modalInstance', 'params', 'ObjectService', 'ArticleService', '$modal'];

    constructor(
        $scope,
        public $modalInstance,
        private params,
        private objectService:services.common.IObjectService,
        private articleService:services.common.IArticleService,
        private $modal) {
      super($scope, $modalInstance);
      this.ls = new services.ListService(() => this.itemList);
      this.object = params.object;
      this.getObjectInfo();
    }

    isNegative() {
      return this.listMoreFilter.emotion === 2;
    }

    changeEmotion(e) {
      e.preventDefault();
      this.listMoreFilter.emotion = this.listMoreFilter.emotion === 2 ? 0 : 2;
      this.listMore();
    }

    openArticle(articleId:number, e) {
      e.preventDefault();
      var modal = this.$modal.open({
        templateUrl: 'public/main/partials/article/display-modal.html',
        controller: controllers.article.ModalArticleDisplayController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              articleId: articleId
            }
          }
        }
      });
      modal.result.then((message) => {
        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
          this.ls.deleteItemsFromList([articleId]);
        }
      }, () => {

      });
    }

    changeSource(source, e) {
      e.preventDefault();
      this.listMoreFilter.source = source;
      this.listMore();
    }


    isSource(source) {
      return source === this.listMoreFilter.source;
    }

    listMore() {
      this.isItemsLoaded = false;
      this.articleService.getByObject(
          this.object.id,
          this.listMoreFilter.source,
          this.listMoreFilter.emotion,
          this.listMoreFilter.query,
          1,
          10
      ).then((itemList) => {
            this.isItemsLoaded = true;
            this.itemList = itemList;
          });
    }

    getObjectInfo() {
      this.isObjectLoading = true;
      this.objectService.getObjectAtomInfo(this.params.articleId, this.object.id).then((object) => {
        this.isObjectLoading = false;
        this.object = object;
        return object;
      });
      //this.listMore();
    }
  }
}
