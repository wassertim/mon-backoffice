///<reference path="../common/ModalController.ts"/>
module backOffice.controllers.article {
  export class DeletedArticleListController extends common.ModalController {
    public itemList = {
      totalCount: 0,
      items: [],
      isLoaded: false
    };
    deletedCount = 0;
    selectedCount = 0;
    ls:services.common.IListService;
    public static $inject = ['$scope', 'ReportQueueService', 'ArticleService', '$modal', 'params', '$modalInstance', 'Confirmation'];

    constructor(private $scope, private reportQueueService, private articleService, private $modal, private params, public $modalInstance, private confirmation) {
      super($scope, $modalInstance);
      this.ls = new services.ListService(()=>this.itemList);
      this.getList();
    }

    getList():any {
      this.reportQueueService.getReport().then(report => {
        this.articleService.getDeletedFromReport(report.reportId, report.period, 100, 1).then((itemList:any) => {
          itemList.isLoaded = true;
          this.itemList = itemList;
        });
      });
    }

    openArticle(articleId:number, article:any, e) {
      e.preventDefault();
      var modal = this.$modal.open({
        templateUrl: 'public/main/partials/article/display-modal.html',
        controller: controllers.article.ModalArticleDisplayController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              articleId: articleId,
              article: article,
              report: this.params.report,
              _action: () => this.restoreSingleToReport(articleId),
              _secondaryAction: () => this.restoreSingleEverywhere(articleId),
              actionButtonText: 'Восстановить',
              secondaryActionButtonText: 'Восстановить везде'
            }
          }
        }
      });
      modal.result.then((message) => {
        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
          this.ls.deleteItemsFromList([articleId]);
        }
      }, () => {

      });
    }
    
    openRestoreConfirmation(e) {
      e.preventDefault();
      this.ls.updateSelectedCount();
      if (this.ls.selectedCount) {
        this.$modal.open({
          templateUrl: 'public/main/partials/modals/delete-restore.html',
          controller: backOffice.controllers.DeleteRestoreController,
          resolve: {
            params: () => {
              return {
                title: 'Восстановить',
                items: this.ls.getSelectedItems(),
                _action: () => this.restoreToReport(this.ls.getSelectedIds()),
                _secondaryAction: () => this.restoreEverywhere(this.ls.getSelectedIds()),
                actionButtonText: 'Восстановить',
                secondaryActionButtonText: 'Восстановить везде',
                alert: 'Вы действительно хотите восстановить эти статьи?'
              };
            }
          }
        });
      }
    }

    restoreSingleToReport(articleId) {
      return this.confirmation('Вы действительно хотите восстановить эту статью в отчете?', 'Восстановить', () => {
        return this.restoreToReport([articleId]);
      });
    }
    restoreSingleEverywhere(articleId) {
      return this.confirmation('Вы действительно хотите восстановить эту статью везде?', 'Восстановить', () => {
        return this.restoreEverywhere([articleId]);
      });
    }
    restoreEverywhere(selectedIds:number[]):any {
      return this.articleService.restoreEverywhere(selectedIds).then(() => {
        this.ls.deleteSelectedItems();
      });
    }

    restoreToReport(selectedIds:number[]):any {
      return this.articleService.restoreToReport(selectedIds, this.params.reportId).then(() => {
        this.ls.deleteSelectedItems();
      });
    }

  }
}
