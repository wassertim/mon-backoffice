///<reference path="../../helper/RandomHelper.ts"/>
///<reference path="../common/ModalController.ts" />
///<reference path="../object/ModalObjectInfoController.ts" />
///<reference path="../DeleteRestoreController.ts"/>
module backOffice.controllers.article {
  export class ModalArticleDisplayController extends DeleteRestoreController {
    isItemsLoaded = true;
    isDeleting = false;
    ls:services.common.IListService;
    counters = {};
    public itemList = {
      totalCount: 0,
      items: [],
      isLoaded: false
    };
    deletedCount = 0;
    verifiedHidden = false;
    pageSize = 100;
    imageSize = { width: 200, height: 200 };
    isArticleLoaded:boolean;
    modalId:string;
    report:any;
    article:any;
    objects:any;
    selectedCount = 0;
    progress = 0;
    hasNewImage = false;
    jsAction:string;
    iconClass = 'icon-square-o';
    isUploading = false;
    isSettingImage = false;
    imageData:any;
    listMoreFilter = {
      emotion: 0, //All
      source: 0, //All
      sort: 0 //Time
    };
    public static $inject =
      ['$scope', '$modalInstance', 'params', 'ReportQueueService', 'ArticleService',
        'ObjectService', 'ClusterArticleService', '$modal', 'RouterService'];

    constructor($scope, public $modalInstance, private params, private reportQueueService:services.common.IReportQueueService, private articleService:services.common.IArticleService, private objectService:services.common.IObjectService, private clusterArticleService:services.common.IClusterArticleService, private $modal, private routerService:services.common.IRouterService) {
      super($scope, $modalInstance, params);
      this.modalId = new helpers.RandomHelper().getCharacters(8);
      this.article = params.article;
      this.getById(params.articleId);
      this.listMore();

      this.jsAction = routerService.action("Image", "UploadImage").url;
      this.ls = new services.ListService(()=>this.itemList);
      $scope.$watch("vm.imageData", newVal => {
        if (newVal)
          this.article.imageId = newVal.id;
      });
    }

    refresh(e) {
      e.preventDefault();
      this.listMore();
    }

    setImage():any {
      this.isSettingImage = true;
      this.articleService.setImage(this.params['articleId'], this.imageData.id).then(() => {
        this.progress = 0;
        this.isSettingImage = false;
        this.hasNewImage = false;
      });
    }


    setImageButtonText():any {
      return this.isSettingImage ? "Сохраняется" : "Сохранить изменения";
    }

    onFileChange():any {
      this.isUploading = true;
    }

    uploadSuccess(response):any {
      this.hasNewImage = true;
      this.imageData = response;
      this.isUploading = false;
    }

    changePictureText():any {
      return this.isUploading ? 'Идет загрузка ...' : 'Сменить картинку';
    }

    isNegative() {
      return this.listMoreFilter.emotion === 2;
    }

    changeEmotion(e) {
      e.preventDefault();
      this.listMoreFilter.emotion = this.listMoreFilter.emotion === 2 ? 0 : 2;
      this.listMore();
    }

    changeSource(source, e) {
      e.preventDefault();
      this.listMoreFilter.source = source;
      this.listMore();
    }


    isSource(source) {
      return source === this.listMoreFilter.source;
    }

    getById(articleId:number):any {
      var reportId = this.params.report.reportId;
      this.isArticleLoaded = false;
      this.articleService.getById(articleId, reportId, this.imageSize.width, this.imageSize.height).then((article:any) => {
        this.isArticleLoaded = true;
        this.article = article;
        return article;
      }).then((article:any) => {
        if (!article.url) {
          this.articleService.getPdfAttachments(article.id).then(url=> {
            this.article.url = url;
          });
        }
      });
      this.objectService.getByArticle(articleId).then(objects => {
        this.objects = objects;
      });
    }

    getObjectInfluence(objectId:number) {
      return this.objectService.getObjectAtomInfo(this.params.articleId, objectId).then((atomInfo) => {
        return atomInfo;
      });
    }

    getObjectInfo(objectId:number):any {
      return this.objectService.getObjectById(objectId).then((objectInfo:any) => {
        return objectInfo.dossier;
      });
    }

    isSortedBy(sort:number) {
      return this.listMoreFilter.sort === sort;
    }

    changeSort(sort:number, e) {
      e.preventDefault();
      this.listMoreFilter.sort = sort;
      this.listMore();
    }

    openObjectInfo(object:any, e) {
      e.preventDefault();
      this.$modal.open({
        templateUrl: 'public/main/partials/object/display-modal.html',
        controller: backOffice.controllers.object.ModalObjectInfoController,
        size: 'sm',
        resolve: {
          params: () => {
            return {
              object: object,
              articleId: this.article.id
            }
          }
        }
      });
    }

    openArticle(articleId:number, article, e) {
      e.preventDefault();
      var modal = this.$modal.open({
        templateUrl: 'public/main/partials/article/display-modal.html',
        controller: controllers.article.ModalArticleDisplayController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              articleId: articleId,
              article: article,
              report: this.params.report
            }
          }
        }
      });
      modal.result.then((message) => {
        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
          this.ls.deleteItemsFromList([articleId]);
        }
      }, () => {

      });
    }

    listMore():any {
      this.isItemsLoaded = false;
      this.report = this.params.report;
      var period = this.report.period;
      this.clusterArticleService.getFilteredListForArticlesWithFilter(this.params['articleId'], period, this.listMoreFilter, this.pageSize, 1)
        .then((itemList:any) => {
          this.isItemsLoaded = true;
          this.itemList = itemList;
          this.clusterArticleService.getCountersForMoreArticles(this.params['articleId']).then((counters:any) => {
            this.counters = counters;
          });
        });

    }
  }
}
