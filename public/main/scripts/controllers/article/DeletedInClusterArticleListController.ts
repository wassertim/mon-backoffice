module backOffice.controllers.article {
  export class DeletedInClusterArticleListController extends controllers.common.ModalController {

    public itemList = {
      totalCount: 0,
      items: [],
      isLoaded: false
    };
    ls: any;
    public static $inject = ['$scope', '$modalInstance', 'ClusterArticleService', 'params', '$modal', 'Confirmation'];

    constructor($scope, $modalInstance, private clusterArticleService: services.common.IClusterArticleService, private params: any, private $modal, private confirmation) {
      super($scope, $modalInstance);
      this.ls = new services.ListService(() => this.itemList);
      this.getList(params.clusterId);
    }

    getList(clusterId):any {
      this.clusterArticleService.getDeletedFromCluster(clusterId).then((itemList:any) => {
        itemList.isLoaded = true;
        this.itemList = itemList;
      });
    }

    openArticle(articleId:number, article:any, e) {
      e.preventDefault();
      var modal = this.$modal.open({
        templateUrl: 'public/main/partials/article/display-modal.html',
        controller: controllers.article.ModalArticleDisplayController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              articleId: articleId,
              article: article,
              report: this.params.report,
              _action: () => this.restoreSingleToCluster(articleId),
              actionButtonText: 'Восстановить в кластере'
            }
          }
        }
      });
      modal.result.then((message) => {
        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
          this.ls.deleteItemsFromList([articleId]);
        }
      }, () => {

      });
    }
    
    openRestoreConfirmation(e) {
      e.preventDefault();
      this.ls.updateSelectedCount();
      if (this.ls.selectedCount) {
        this.$modal.open({
          templateUrl: 'public/main/partials/modals/delete-restore.html',
          controller: backOffice.controllers.DeleteRestoreController,
          resolve: {
            params: () => {
              return {
                title: 'Восстановить',
                items: this.ls.getSelectedItems(),
                _action: () => this.restoreToCluster(this.ls.getSelectedIds()),
                actionButtonText: 'Восстановить',
                alert: 'Вы действительно хотите восстановить эти статьи?'
              };
            }
          }
        });
      }
    }

    restoreSingleToCluster(articleId) {
      return this.confirmation('Вы действительно хотите восстановить эту статью в кластере?', 'Восстановить', () => {
        return this.restoreToCluster([articleId]);
      });
    }
    restoreToCluster(ids):any {
      return this.clusterArticleService.restoreToCluster(ids, this.params.clusterId).then(() => {
        this.ls.deleteSelectedItems();
      });
    }
  }
}