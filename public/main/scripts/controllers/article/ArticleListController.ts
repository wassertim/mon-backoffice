///<reference path="../../services/common/IConfigService.ts"/>
///<reference path="../../services/common/IArticleService.ts"/>
///<reference path="../../services/common/IReportQueueService.ts"/>
///<reference path="../../services/common/IObjectService.ts"/>
///<reference path="../article/ModalArticleDisplayController.ts"/>
///<reference path="../DeleteRestoreController.ts" />
///<reference path="../common/ListController.ts" />
module backOffice.controllers.article {
  export class ArticleListController extends controllers.common.ListController {
    isDeleting = false;
    objects:any;
    period:any;
    searchText:string;
    counters = { totalCount: 0, verifiedCount: 0 };
    public static $inject = ['$modal', '$scope', 'ArticleService',
      'ReportQueueService', '$state', 'ConfigService', 'NavService', 'Confirmation'];

    constructor(
      private $modal:any,
      public $scope:any,
      private articleService:services.common.IArticleService,
      public reportQueueService:services.common.IReportQueueService,
      public $state:ng.ui.IState,
      public configService:any,
      private nav:any,
      private confirmation: any) {
      super($scope, configService, reportQueueService, $state);
      this.getArticleList();
    }

    isClustersDisabled():any {
      if (!this.report || !this.report.type) return false;
      return this.report.type === 1;
    }

    isArticlesDisabled():any {
      if (!this.report || !this.report.type) return false;
      return this.report.type === 2;
    }

    deleteConfirmation(e) {
      e.preventDefault();
      if (this.ls.selectedCount > 0) {
        this.$modal.open({
          templateUrl: 'public/main/partials/modals/delete-restore.html',
          controller: controllers.DeleteRestoreController,
          resolve: {
            params: () => {
              return {
                title: 'Удалить',
                items: this.ls.getSelectedItems(),
                _action: () => this.deleteFromReport(this.ls.getSelectedIds()),
                _secondaryAction: () => this.deleteEverywhere(this.ls.getSelectedIds()),
                actionButtonText: 'Удалить',
                secondaryActionButtonText: 'Удалить везде',
                alert: 'Вы действительно хотите удалить эти статьи?'
              };
            }
          }
        });
      }
    }

    openTrash(e) {
      e.preventDefault();
      var modal = this.$modal.open({
        templateUrl: 'public/main/partials/article/deleted-list-modal.html',
        controller: controllers.article.DeletedArticleListController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              reportId: this.reportId,
              report: this.report
            };
          }
        }
      });
    }

    deleteSingleFromReport(articleId:number) {
      return this.confirmation('Вы действительно хотите удалить эту статью из отчета?', 'Удалить', () => {
        return this.deleteFromReport([articleId]);
      });
    }

    deleteSingleEverywhere(articleId:number) {
      return this.confirmation('Вы действительно хотите удалить эту статью везде?', 'Удалить', () => {
        return this.deleteEverywhere([articleId]);
      });
    }

    openArticle(articleId:number, article:any, e) {
      e.preventDefault();
      var modal = this.$modal.open({
        templateUrl: 'public/main/partials/article/display-modal.html',
        controller: controllers.article.ModalArticleDisplayController,
        size: 'lg',
        resolve: {
          params: () => {
            return {
              articleId: articleId,
              article: article,
              report: this.report,
              _action: () => this.deleteSingleFromReport(articleId),
              _secondaryAction: () => this.deleteSingleEverywhere(articleId),
              actionButtonText: 'Удалить в отчете',
              secondaryActionButtonText: 'Удалить везде'
            }
          }
        }
      });
      modal.result.then((message) => {
        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
          this.ls.deleteItemsFromList([articleId]);
        }
      }, () => {

      });
    }

    getArticleList():any {
      this.reportQueueService.getReport().then((report:any) => {
        this.isItemsLoaded = false;
        this.report = report;
        this.reportId = report.reportId;
        this.period = this.report.period;
        return report
      }).then((report) => {
        this.loadConfigurations(report.reportId);
        return this.articleService.getByReport(report.reportId, report.period, this.verifiedHidden, this.pageSize, 1).then((itemList:any) => {
          this.isItemsLoaded = true;
          this.itemList = itemList;
          this.ls.updateSelectedCount();
          return itemList;
        });
      }).then((itemList:any)=> {
        return this.articleService.getCounters(this.reportId, this.period).then((counters:any) => {
          this.counters = counters;
          return counters;
        });
      }).then((counters:any)=> {
        this.articleService.getDeletedFromReportCount(this.reportId, this.period).then((deletedCount:any) => {
          this.ls.deletedCount = +deletedCount;
          return this.ls.deletedCount;
        });
      });
    }


    deleteEverywhere(selectedIds):any {
      return this.articleService.deleteEverywhere(selectedIds).then(() => {
        this.ls.deleteSelectedItems();
      });
    }

    deleteFromReport(selectedIds):any {
      return this.articleService.deleteFromReport(selectedIds, this.reportId).then(() => {
        this.ls.deleteSelectedItems();
      });
    }
    refresh(e):any {
      e.preventDefault();
      if (this.isItemsLoaded) {
        this.getArticleList();
      }
    }

    loadMore():any {
      this.isItemsLoaded = false;
      this.articleService.getByReport(this.reportId, this.report.period, this.verifiedHidden, this.pageSize, ++this.currentPage).then((itemList:any)=> {
        this.itemList.items.push.apply(this.itemList.items, itemList.items);
        this.isItemsLoaded = true;
      });
    }

    hasMore():any {
      return this.itemList.totalCount > this.itemList.items.length;
    }

    showGotoClustersButton():boolean {
      return this.report && (this.report.type === 3);
    }


  }
}
