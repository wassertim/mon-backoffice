///<reference path="common/BaseController.ts"/>
///<reference path="../services/common/IReportQueueService.ts"/>
///<reference path="../services/common/IConfigService.ts"/>
module backOffice.controllers {
  export class ReportQueueController extends common.BaseController {
    public static $inject = ['$scope', '$stateParams', '$state', '$$state', '$location', 'ReportQueueService', 'ConfigService'];
    private showInfo:boolean = true;
    private verifiedState:string;
    private nextVerifiedState:string;
    private verifiedHidden:boolean;
    private isReportInfoHidden:boolean = true;
    private isReportLoading:boolean = false;
    private isConfigsLoaded:boolean = false;
    private isInfoLoaded:boolean = false;
    private priority:number = 1;
    private configToAdd = { displayName: "", operatorPriority: 1 };
    private configs = [];
    private allConfigs = [];
    private reportQueue:any;

    appTypes = {
      '0': 'Other',
      '1': 'Web',
      '2': 'Ipad',
      '3': 'Plasma',
      '4': 'Glass2'
    };
    private alertBlock = {
      type: 'danger',
      message: 'Произошла ошибка. Повторите пожалуйста запрос еще раз. Если ошибка будет повторяться - обратитесь в службу поддержки?',
      show: false
    };

    constructor(
        public $scope:any,
        $stateParams:ng.ui.IStateParams,
        public $state:ng.ui.IState,
        $localState:ng.ui.ILocalState,
        $location:ng.ILocationService,
        public reportQueueService:backOffice.services.common.IReportQueueService,
        public configService:backOffice.services.common.IConfigService) {
      super($scope, $state, $localState);

      this.verifiedState = $stateParams["vs"];
      this.nextVerifiedState = $stateParams["vs"] === "SV" ? "HV" : "SV";
      this.verifiedHidden = $stateParams["vs"] === "HV";

      this.init();
    }

    showAlert(message, type):any {
      if (message) {
        this.alertBlock.message = message;
      }
      this.alertBlock.type = type;
      this.alertBlock.show = true;
    }

    configFilter(item:any):any {
      return !_.any(this.configs, (config:any) => {
        return config.id == item.id;
      });
    }

    goodToAdd(config):any {
      return config &&
          (typeof (config) === "object") &&
          config.displayName && !_.any(this.configs, (conf:any) => {
        return conf.id === config.id;
      });
    }

    checkboxClick(e):any {
      //don't propogate, because parent element has a click event handler too
      e.stopPropagation();
    }

    loadReportButtonText():any {
      if (!this.isReportLoading) {
        return "Загрузить отчет из очереди";
      } else {
        return "Идет загрузка отчета из очереди ...";
      }
    }

    getTopReport():any {
      this.isReportLoading = true;
      this.reportQueueService.getReport().then(
          (report:any) => {
            this.isReportLoading = false;
            if (report == "null") {
              this.showAlert("В очереди нет непроверенных отчетов", "danger");
              return;
            }
            var types = ['', 'articles', 'clusters', 'mixed'];
            switch (types[report.type]) {
              case 'articles':
                this.$state.go('otk.report.articles', { vs: 'HV' });
                break;
              case 'clusters':
                this.$state.go('otk.report.clusters', { vs: 'HV' });
                break;
              default:
                this.$state.go('otk.report.articles', { vs: 'HV' });
                break;
            }
          }, (error) => {
            this.isReportLoading = false;
          }
      );
    }

    deleteConfigurationFromQueue():any {
      _.forEach(_.where(this.configs, { 'selected': true }), (config:any) => {
        config.deleted = true;
        config.selected = false;

        this.reportQueueService.deleteConfigurationFromQueue(config.id).then(
            () => {
              _.remove(this.configs, { id: config.id });
            },
            () => {
              config.deleted = false;
            }
        );
      });
    }

    deleteConfigurationFromQueueBatch():any {
      var selectedConfigs = _.where(this.configs, { 'selected': true });
      if (selectedConfigs.length === 0)
        return;
      var selectedConfigIds = _.map(selectedConfigs, (config:any) => {
        return config.id;
      });
      _.forEach(selectedConfigs, (config:any) => {
        config.deleted = true;
        config.selected = false;
      });
      this.reportQueueService.deleteConfigurationFromQueue(selectedConfigIds).then(
          function success() {
            _.remove(this.configs, { deleted: true });
          },
          function error() {
            _.forEach(_.where(this.configs, { 'deleted': true }), (item:any) => {
              item.deleted = false;
            });
          });
    }

    addConfigurationToQueue(config):any {
      if (this.goodToAdd(config)) {
        var configCopy = angular.copy(config);
        this.configToAdd = { displayName: "", operatorPriority: 1 };
        this.$scope.form.$setPristine();
        configCopy.notAdded = true;
        this.configs.push(configCopy);
        this.reportQueueService.addConfigurationToQueue(configCopy).then(() => {
          configCopy.notAdded = false;
          _.remove(this.allConfigs, { id: configCopy.id });
        });
      }
    }

    getAllPublishTerminalConfiguration() {
      this.configService.getAllPublishTerminalConfiguration().then((configs:any) => {
        _.forEach(configs, (config:any) => {
          config.getApplicationType = () => {
            return this.appTypes[config.applicationType];
          };
        });
        this.allConfigs = configs;
      });
    }

    getConfigurationsOnControl() {
      this.reportQueueService.getConfigurationsOnControl().then((configs:any) => {
        this.isConfigsLoaded = true;
        this.configs = configs;
        return configs;
      });
    }

    //action
    getQueueStatistic() {
      this.reportQueueService.getQueueStatistic().then(queueInfo => {
        this.isInfoLoaded = true;
        this.reportQueue = queueInfo;
      });
    }

    //action
    getSettingsData() {
      this.getConfigurationsOnControl();
      this.getAllPublishTerminalConfiguration();
    }

  }
}