///<reference path="common/BaseController.ts"/>
///<reference path="../services/common/IReportQueueService.ts"/>
///<reference path="../services/common/INavService.ts"/>
///<reference path="../services/common/IReportService.ts"/>
module backOffice.controllers {
  export class ReportController extends common.BaseController {
    private showInfo = true;
    private messages = [];
    private verifiedState:string;
    private nextVerifiedState:string;
    private verifiedHidden:boolean;
    private isReportInfoHidden = false;
    private configId:any;
    private report:any;
    private isSaveInProgress = false;
    public isConfigListLoaded = false;
    public configs:any;
    private isRefreshingCache = false;
    private isArticlesActive = false;
    private isClustersActive = false;
    public periodTranslation = {
      "1": "День",
      "3": "Неделя",
      "4": "Месяц",
      "2": "Плазма"
    };
    private configPluralizer:any = {
      '1': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
      '21': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
      '31': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
      'other': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурациях',
      '0': '',
      '-1': ''
    };

    public static $inject = ['$rootScope', '$scope', '$stateParams', '$location', '$http', 'ReportService', 'ReportQueueService', '$state', '$$state', 'NavService', 'ConfigService'];

    constructor(
        $rootScope,
        public $scope:any,
        public $stateParams:any,
        public $location:ng.ILocationService,
        public $http:ng.IHttpService,
        public reportService:services.common.IReportService,
        public reportQueueService:services.common.IReportQueueService,
        public $state:ng.ui.IState,
        public $localState:ng.ui.ILocalState,
        private nav:backOffice.services.common.INavService,
        private configService:services.common.IConfigService) {
      super($scope, $state, $localState, $scope.getReportInfo);

      this.verifiedState = $state.params.vs;
      this.nextVerifiedState = $state.params.vs === "SV" ? "HV" : "SV";
      this.verifiedHidden = $stateParams.vs === "HV";

      this.configId = $stateParams.configId;
      this.report = this.defaultReport();
      //console.log('reportController');
      this.getReportInfo();
    }



    defaultReport():any {
      return {
        reportId: 0,
        numberOfConfigurations: 0,
        isLoaded: false,
        'type': 1
      };
    }

    forceReportRefresh() {
      if (this.report.reportId) {
        this.isRefreshingCache = true;
        this.reportQueueService.forceReportRefresh(this.report.reportId).then(r=> {
          this.isRefreshingCache = false;
        });
      }
    }


    getReportInfo():any {
      this.reportQueueService.getReport().then((report:any) => {
        if (report === "null") {
          this.$state.go("otk.reports");
        } else {
          this.report = report;

          this.loadConfigurations(report.reportId);
          if (this.$state.current.name === 'otk.report') {
            this.redirect(report.type);
          }
        }
      });
    }

    loadConfigurations(reportId:number) {
      this.isConfigListLoaded = false;
      this.configService.getConfigurationsByReportId(reportId).then(items => {
        this.configs = items;
        this.isConfigListLoaded = true;
      });
    }

    redirect(type):any {
      switch (type) {
        case 1:
          this.$state.go('otk.report.articles', { vs: 'HV' });
          break;
        case 2:
          this.$state.go('otk.report.clusters', { vs: 'HV' });
          break;
        default:
          this.$state.go('otk.report.articles', { vs: 'HV' });
          break;
      }
    }


  }
}