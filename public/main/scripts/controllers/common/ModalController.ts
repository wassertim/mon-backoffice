module backOffice.controllers.common {
  export class ModalController extends Controller {
    constructor($scope, public $modalInstance) {
      super($scope);
    }
    close() {
      this.$modalInstance.dismiss('cancel');
    }
  }
}