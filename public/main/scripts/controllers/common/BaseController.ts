module backOffice.controllers.common {
  //TODO: Deprecate
  export class BaseController {
    public execStateConfig:ng.ui.IStateConfig;
    public url:any;

    constructor(public $scope:any, public $state:ng.ui.IState, $localState?:ng.ui.ILocalState, private load?:any) {
      $scope.vm = this;
      this.url = $state.href;
      if ($localState)
        this.execStateConfig = $localState.self;
    }

    public init():any {
      this.load && this.load();
      this.execStateConfig && this.$scope.vm[this.execStateConfig.action] && this.$scope.vm[this.execStateConfig.action].call(this);
    }
  }
} 