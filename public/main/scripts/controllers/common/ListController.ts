module backOffice.controllers.common {
  export class ListController extends controllers.common.Controller {
    public configs:any;
    public reportId:number;
    public report:any;
    public isRefreshingCache = false;
    public isSaveInProgress = false;
    public verifiedHidden: boolean;
    public verifiedState:string;
    public nextVerifiedState: string;
    public ls: services.common.IListService;
    public isItemsLoaded = false;
    public pageSize = 100;
    public currentPage = 1;
    public isConfigListLoaded = false;
    public itemList = {
      totalCount: 0,
      items: [],
      isLoaded: false
    };
    public appTypes = {
      '0': 'Other',
      '1': 'Web',
      '2': 'Ipad',
      '3': 'Plasma',
      '4': 'Glass2'
    };
    public periodTranslation = {
      "1": "День",
      "3": "Неделя",
      "4": "Месяц",
      "2": "Плазма"
    };
    public configPluralizer:any = {
      '1': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
      '21': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
      '31': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
      'other': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурациях',
      '0': '',
      '-1': ''
    };

    constructor(
      $scope,
      public configService:services.common.IConfigService,
      public reportQueueService:services.common.IReportQueueService,
      public $state:ng.ui.IState) {
      super($scope);
      this.verifiedState = $state.params.vs;
      this.nextVerifiedState = this.verifiedState  === "SV" ? "HV" : "SV";
      this.verifiedHidden = this.verifiedState === "HV";
      this.ls = new services.ListService(() => this.itemList);
    }

    public getConfigurations() {
      return this.configService.getConfigurationsByReportId(this.reportId).then(items => {
        _.forEach(items, (item:any) => item.applicationType = this.appTypes[item.applicationType]);
        return items;
      });
    }

    public loadConfigurations(reportId:number) {
      this.isConfigListLoaded = false;
      return this.configService.getConfigurationsByReportId(reportId).then(items => {
        this.configs = items;
        this.isConfigListLoaded = true;
      });
    }

    public forceReportRefresh() {
      if (this.report.reportId) {
        this.isRefreshingCache = true;
        this.reportQueueService.forceReportRefresh(this.report.reportId).then(r => {
          this.isRefreshingCache = false;
        });
      }
    }

    public saveButtonText():any {
      if (!this.isSaveInProgress) {
        return "Сохранить отчет";
      } else {
        return "Идет сохранение отчета ...";
      }
    }

    public saveAndLoadNewButtonText():any {
      if (!this.isSaveInProgress) {
        return "Сохранить отчет и загрузить новый";
      } else {
        return "Идет сохранение отчета ...";
      }
    }

    public save(onSuccess):any {

      if (this.isSaveInProgress || !this.report.isLoaded)
        return;
      this.isSaveInProgress = true;
      this.reportQueueService.saveReport().then(
        () => {
          this.isSaveInProgress = false;
          onSuccess();
        },
        (err) => {
          console.log(err);
        }
      );
    }

    public verifiedHiddenText():any {
      if (this.verifiedHidden) {
        return "Показать проверенные";
      } else {
        return "Скрыть проверенные";
      }
    }

    public saveReport():any {
      this.save(() => {
        this.$state.go('otk.reports');

      });
    }

    public saveAndLoadNew():any {
      this.save(() => {
        this.$state.go('otk.report', {}, { reload: true, inherit: true, notify: true });
      });
    }
  }
}