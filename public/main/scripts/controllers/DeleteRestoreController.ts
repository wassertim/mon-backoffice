module backOffice.controllers {
  export class DeleteRestoreController extends controllers.common.ModalController {
    public static $inject = ['$scope', '$modalInstance', 'params'];

    constructor($scope, public $modalInstance, params) {
      super($scope, $modalInstance);
      angular.extend($scope, params);
      $scope.action = () => {
        var promise = params._action();
        if (promise) {
          $scope.isActionActive = true;
          promise.then(() => {
            $scope.isActionActive = false;
            this.$modalInstance.close('ok');
          }, () => {
            $scope.isActionActive = false;
          });
        }
      };
      $scope.secondaryAction = () => {
        var promise = $scope._secondaryAction();
        if (promise) {
          $scope.isSecondaryActionActive = true;
          promise.then(() => {
            $scope.isSecondaryActionActive = false;
            this.$modalInstance.close('ok');
          }, () => {
            $scope.isSecondaryActionActive = false;
          });
        }
      };
    }
  }
}