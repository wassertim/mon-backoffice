///<reference path="../services/common/IConfigService.ts"/>
module backOffice.controllers {
  //TODO: Сделать похожим на другие контроллеры
  export class ConfigController {
    public static $inject = ['$scope', '$state', '$stateParams', 'ConfigService'];

    constructor(
        public $scope:any,
        public $state:ng.ui.IState,
        public $stateParams:any,
        private configService: services.common.IConfigService) {
      $scope.configs = [];
      $scope.isConfigListLoaded = false;
      this.init();
    }

    init():any {
      if (this.$state.current.name === "otk.configsByReport")
        this.getConfigurationsByReportId();
    }

    getConfigurationsByReportId = function () {
      this.$scope.isConfigListLoaded = false;
      this.configService.getConfigurationsByReportId(this.$stateParams.reportId).then(items => {
        this.$scope.configs = items;
        this.$scope.isConfigListLoaded = true;
      });
    };
  }
}