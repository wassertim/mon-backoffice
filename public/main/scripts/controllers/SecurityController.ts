///<reference path="common/BaseController.ts"/>
///<reference path="../services/common/ISecurityService.ts"/>
///<reference path="shared/ModalConfirmationController.ts" />
module backOffice.controllers {
  export class SecurityController {
    auth;
    alerts:any[];
    isLoading = false;
    public static $inject = ['$scope', '$state', 'SecurityService', '$modal'];

    constructor(public $scope:any, public $state:ng.ui.IState, private securityService:services.common.ISecurityService, private $modal) {
      $scope.vm = this;

    }

    signIn() {
      this.isLoading = true;
      this.$scope.$broadcast("autofill:update");
      this.securityService.signIn(this.auth).then((identity:any) => {
        this.isLoading = false;
        if (identity.isAuthenticated) {
          this.$state.go('otk.reports');
        } else {
          this.showError('Логин или пароль не верны');
        }
      });
    }

    closeAlert(index) {
      this.alerts.splice(index, 1);
    }

    showError(error) {
      this.alerts = [
        {
          type: 'danger',
          msg: error
        }
      ];
    }


  }
}