module backOffice.controllers {
  export class BreadcrumbController {
    public static $inject = ['$scope', '$rootScope', '$stateParams', '$state'];
    constructor($scope:any, $rootScope:any, $stateParams:any, private $state:any) {
      $scope.vm = this;
      $scope.reportId = $stateParams.reportId;
      $scope.verifiedState = $stateParams.vs;
      $scope.breadcrumbs = [];

      $scope.$on("$stateChangeSuccess", () => {
        if ($state.current.breadcrumbs)
          $scope.breadcrumbs = $state.current.breadcrumbs($stateParams);
      });
    }
    currentUrl(stateName) {
      return this.$state.href(stateName, this.$state.params);
    }
  }
}