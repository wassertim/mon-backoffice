///<reference path="common/BaseController.ts"/>
///TODO: Obsolete - Это контроллер уступает место NavService
module backOffice.controllers {
  export class NavController extends common.BaseController {
    public static $inject = ['$scope', '$location', '$state', '$modal', 'SecurityService'];

    constructor(
      public $scope:any,
      public $location:ng.ILocationService,
      public $state:ng.ui.IState,
      private $modal,
      private securityService: services.common.ISecurityService) {
      super($scope, $state);

    }

    isLogin():any {
      return this.$state.current.name === "login";
    }

    lastIndex(array):any {
      return array.length - 1;
    }

    isActive(path):any {
      return _.contains(this.$location.path(), path);
    }

    getActive(path):any {
      if (this.isActive(path)) {
        return "active";
      } else {
        return "";
      }
    }

    logOff(e):any {
      e.preventDefault();
      var modalInstance = this.$modal.open({
        templateUrl: 'public/main/partials/modals/confirmation.html',
        controller: controllers.shared.ModalConfirmationController,
        resolve: {
          params: () => {
            return {
              message: 'Вы действительно хотите выйти из системы?',
              okText: 'Выйти',
              cancelText: 'Отменить'
            }
          }
        }
      });
      modalInstance.result.then(() => {
        this.securityService.signOff();
        this.$state.go('login');
      });
    }
  }
}