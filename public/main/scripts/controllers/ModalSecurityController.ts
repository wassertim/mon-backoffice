module backOffice.controllers {
  export class ModalSecurityController  extends common.ModalController {
    auth;
    alerts:any[];
    isLoading = false;
    public static $inject = ['$scope', '$state', 'SecurityService', '$modalInstance'];

    constructor(
      public $scope:any,
      public $state:ng.ui.IState,
      private securityService: services.common.ISecurityService,
      public $modalInstance) {
      super($scope, $modalInstance);

    }

    signIn(){
      this.isLoading = true;
      this.$scope.$broadcast("autofill:update");
      this.securityService.signIn(this.auth).then((identity:any) => {
        this.isLoading = false;
        if (identity.isAuthenticated) {
          if (this.$modalInstance) {
            this.$modalInstance.close('ok');
          } else {
            this.$state.go('otk.reports');
          }
        } else {
          this.showError('Логин или пароль не верны');
        }
      });
    }

    closeAlert(index) {
      this.alerts.splice(index, 1);
    }

    showError(error) {
      this.alerts = [{
        type: 'danger',
        msg: error
      }];
    }


  }
}