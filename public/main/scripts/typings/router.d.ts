declare var Router:jsRouter.IRouter;
declare module jsRouter {
  interface IRouter {
    action(controller:string, action:string, urlParams?:any): ng.IRequestConfig;
  }
  interface RequestData {
    url: string
  }
}