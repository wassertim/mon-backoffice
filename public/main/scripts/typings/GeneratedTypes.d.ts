/* Generated at 13.12.2013 14:33:22 */
declare module backOffice.typeDefinitions {
interface CoreArticleListModel {

                 items: any; //of type Monitor.Core.Contracts.DataContracts.Results.Article.ArticleBaseItem[] does not have a corresponding typescript type mapping yet
checkedItemsCount: number; // System.Int32
 totalCount: number; // System.Int32
 pageIndex: number; // System.Int32
 pageSize: number; // System.Int32
 }// CoreArticleListModel

      
 interface CoreObjectListItem {

         id: number; // System.Int32
 name: string; // System.String
         type: any; //of type Monitor.Domain.Definitions.ObjectType does not have a corresponding typescript type mapping yet
        objectRole: any; //of type Monitor.Domain.Definitions.ObjectRole does not have a corresponding typescript type mapping yet
        quotationType: any; //of type Monitor.Domain.Definitions.ObjectQuotationType does not have a corresponding typescript type mapping yet
        emotionalTinge: any; //of type Monitor.Domain.Definitions.Filters.EmotionalTinge does not have a corresponding typescript type mapping yet
imageUrl: string; // System.String
         atomIndex: any; //of type System.Double does not have a corresponding typescript type mapping yet
}// CoreObjectListItem

      
 interface ArticleBaseItem {

         id: number; // System.Int32
 title: string; // System.String
 annotation: string; // System.String
 source: string; // System.String
         publishDate: any; //of type System.DateTime does not have a corresponding typescript type mapping yet
imageUrl: string; // System.String
 hasOwnImage: boolean; // System.Boolean
 isRead: boolean; // System.Boolean
 url: string; // System.String
 articlesInClusterCount: number; // System.Int32
         sumAtomIndex: any; //of type System.Double does not have a corresponding typescript type mapping yet
}// ArticleBaseItem

      
 interface ArticleItemWithText {

         text: string; // System.String
 highlightText: string; // System.String
 highlightXML: string; // System.String
 reprintCount: number; // System.Int32
 id: number; // System.Int32
 title: string; // System.String
 annotation: string; // System.String
 source: string; // System.String
         publishDate: any; //of type System.DateTime does not have a corresponding typescript type mapping yet
imageUrl: string; // System.String
 hasOwnImage: boolean; // System.Boolean
 isRead: boolean; // System.Boolean
 url: string; // System.String
 articlesInClusterCount: number; // System.Int32
         sumAtomIndex: any; //of type System.Double does not have a corresponding typescript type mapping yet
}// ArticleItemWithText

      
 interface ImageListModel {

                 items: any; //of type Monitor.Core.Contracts.DataContracts.Results.ImageListItem[] does not have a corresponding typescript type mapping yet
totalCount: number; // System.Int32
 pageIndex: number; // System.Int32
 pageSize: number; // System.Int32
 }// ImageListModel

      
 interface ReportQueueModel {

         reportId: number; // System.Int32
 displayName: string; // System.String
         lastViewDate: any; //of type System.Nullable`1[System.DateTime] does not have a corresponding typescript type mapping yet
        lastCacheDate: any; //of type System.Nullable`1[System.DateTime] does not have a corresponding typescript type mapping yet
        type: any; //of type Monitor.Domain.Definitions.ReportQueueType does not have a corresponding typescript type mapping yet
numberOfConfigurations: number; // System.Int32
         period: any; //of type Monitor.Domain.Definitions.ReportQueuePeriod does not have a corresponding typescript type mapping yet
}// ReportQueueModel

      
 interface PdfInfo {

         name: string; // System.String
 }// PdfInfo

      
 }