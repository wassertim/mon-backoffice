///<reference path="../typings/angularjs/angular.d.ts"/>
///<reference path="../typings/angularjs/angular-ui.d.ts"/>
///<reference path="../model/Breadcrumb.ts"/>
module backOffice.helpers {
    export class BreadcrumbHelper {
        
        constructor(public $stateParams: ng.ui.IStateParams) {}
        pod(active: boolean) {
            return new backOffice.model.Breadcrumb('Картина дня', 'pod', active);
        }
        settings(active: boolean) {
            return new backOffice.model.Breadcrumb('Настройки', 'settings', active);
        }
        'settings.queue'(active: boolean) {
            return new backOffice.model.Breadcrumb('Очередь', 'settings.queue', active);
        }
        monitor(active: boolean) {
            return new backOffice.model.Breadcrumb('Монитор', 'monitor', active);
        }
        otk(active: boolean) {
            return new backOffice.model.Breadcrumb('ОТК', 'otk', active);
        }
        otkConfigs(active: boolean) {
            return new backOffice.model.Breadcrumb('Все конфигурации', 'otk.configs', active);
        }
        otConfigsByReport(active: boolean) {
            return new backOffice.model.Breadcrumb('По отчету ' + this.$stateParams['reportId'], 'otk.configsByReport', active);
        }
        otkConfigReports(active: boolean) {
            return new backOffice.model.Breadcrumb('Отчеты', 'otk.configReports', active);
        }
        otkConfigArticles(active: boolean) {
            return new backOffice.model.Breadcrumb('Сообщения', 'otk.configArticles', active);
        }
        otkReports(active: boolean) {
            return new backOffice.model.Breadcrumb('Очередь', 'otk.reports', active);
        }
        otkReport(active: boolean) {
            return new backOffice.model.Breadcrumb('Мой отчет', 'otk.report', active);
        }
        otkArticleEdit(active: boolean) {
            return new backOffice.model.Breadcrumb('Редактирование ' + this.$stateParams['articleId'], 'otk.articleEdit', active);
        }
        otkArticleDisplay(active: boolean) {
            return new backOffice.model.Breadcrumb('Просмотр ' + this.$stateParams['articleId'], 'otk.articleDisplay', active);
        }
        otkClusterEdit(active: boolean) {
            return new backOffice.model.Breadcrumb('Редактирование ' + this.$stateParams['clusterId'], 'otk.clusterEdit', active);
        }
        'otk.clusterEdit.relevantList'(active: boolean) {
            return new backOffice.model.Breadcrumb('Выбор главной статьи', 'otk.clusterEdit.relevantList', active);
        }
        otkClusterDisplay(active: boolean) {
            return new backOffice.model.Breadcrumb('Просмотр ' + this.$stateParams['clusterId'], 'otk.clusterDisplay', active);
        }
        otkClusterDisplayArticles(active: boolean) {
            return new backOffice.model.Breadcrumb('Статьи', 'otk.clusterDisplay.articles', active);
        }
        otkReportConfigs(active: boolean) {
            return new backOffice.model.Breadcrumb('Конфигурации', 'otk.reportConfigs', active);
        }
        otkReportArticles(active: boolean) {
            return new backOffice.model.Breadcrumb('Сообщения', 'otk.report.articles', active);
        }
        otkReportClusters(active: boolean) {
            return new backOffice.model.Breadcrumb('События', 'otk.report.clusters', active);
        }
        otkClusterPhotoEdit(active: boolean) {
            return new backOffice.model.Breadcrumb('Картинки', 'otk.clusterPhotoEdit', active);
        }
        articlesTrash(active: boolean) {
            return new backOffice.model.Breadcrumb('Корзина статей', 'articlesTrash', active);
        }
        clustersTrash(active: boolean) {
            return new backOffice.model.Breadcrumb('Корзина кластеров', 'clustersTrash', active);
        }
        clusterArticlesTrash(active: boolean) {
            return new backOffice.model.Breadcrumb('Корзина сообщений кластера', 'clusterArticlesTrash', active);
        }
        otkArticleDisplayMoreArticles(active: boolean) {
            return new backOffice.model.Breadcrumb('Еще на тему', 'otk.articleDisplay.moreArticles', active);
        }
    }
}
