///<reference path="../global.ts"/>
module backOffice.helpers {
  export class ImageHelper {
    getImageUrl(imageId:string, width:any, height:any, type:any):string {
      return (() => {
        if (backOffice.Global.imageHost === 'http://lorempixel.com')
          return backOffice.Global.imageHost + '/' + width + '/' + height + '?id=' + imageId;
        else
          return backOffice.Global.imageHost + '/Monitor.Image.Web/Image/GetById/' +
            imageId + '/A4/1' + '/' + width + '/' + height + '/' + type;
      })();
    }
    isGuid(source:string):boolean {
      return /[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/.test(source);
    }

    getIdFromUrl(url) {
      var re = /[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/;
      var resultArray = url.match(re);
      return resultArray[0];
    }
  }
  app.service('ImageHelper', ImageHelper);
}
