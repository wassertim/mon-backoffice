///<reference path="typings/angularjs/angular.d.ts"/>
interface Globals {
  pdfUrl: string;
  imageHost: string;
}
var backofficeGlobals:Globals;
var app = angular.module('anbo', [
  'ngRoute',
  'ui.router.compat',
  'ngAnimate',
  'angularMoment',
  /*''ui.bootstrap.bindHtml',
  'ui.bootstrap.typeahead',
  'ui.bootstrap.buttons',
  ui.bootstrap.collapse',*/
  'ui.bootstrap',
  'angular-carousel',
  //'ui.bootstrap.popover',
  'ui.router.stateHelper'
]);
module backOffice.global {
  var appTypes = {
    '0': 'Other',
    '1': 'Web',
    '2': 'Ipad',
    '3': 'Plasma',
    '4': 'Glass2'
  };

}
module backOffice {
  export class Global {
    public static isLoginOpen = false;
    public static pdfUrl = backofficeGlobals.pdfUrl;
    public static imageHost = backofficeGlobals.imageHost;
  }
}
declare var jsRoutes: any;