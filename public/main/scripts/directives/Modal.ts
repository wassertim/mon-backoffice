///<reference path="../typings/bootstrap.d.ts"/>
'use strict';
module backOffice.directives {
  bsModal.$inject = ['$window', '$http', '$compile', '$location'];
  export function bsModal($window, $http, $compile, $location) {
    return {
      restrict: 'AE',
      scope: {
        templateUrl: '@',
        title: '@',
        alert: '@',
        actionButtonText: '@',
        secondaryActionButtonText: '@',
        serviceObject: '=',
        itemsToShow: '=',
        action: '=',
        secondaryAction: '=',
        noItems: '='
      },
      link: (scope, elem, attr)=> {
        var modal = null;
        scope.currentUrl = $location.path();
        scope.settings = {
          show: false
        };
        elem.bind("click", ()=> {
          scope.safeApply(()=> {
            openModal();
          });
        });
        scope.safeApply = fn => {
          (scope.$$phase || scope.$root.$$phase) ? fn() : scope.$apply(fn);
        };
        scope.$watch('settings.show', isOpen=> {
          if (isOpen) {
            $http.get(attr.templateUrl).then(response=> {
              var template = angular.element(response.data);
              var linkFn = $compile(template);
              linkFn(scope);
              modal = $(template).modal('show');
              modal.on('hidden.bs.modal', ()=> {
                scope.cleanModal();
              });
            });
          } else {
            if (modal != null) {
              scope.closeModal();
            }
          }
        });
        scope.action = (initialAction=> ()=> {
          initialAction.call(scope.serviceObject, scope.settings);
        })(scope.action);
        if (scope.secondaryAction) {
          scope.secondaryAction = (initialAction=> ()=> {
            initialAction.call(scope.serviceObject, scope.settings);
          })(scope.secondaryAction);
        }

        function openModal() {
          var items = scope.itemsToShow.call(scope.serviceObject);
          if (items.length > 0 || scope.noItems) {
            scope.items = items;
            scope.settings.show = true;
          }
        };

        scope.cleanModal = ()=> {
          scope.safeApply(()=> {
            $(".modal-backdrop").remove();
            scope.settings.show = false;
          });
        };
        scope.closeModal = ()=> {
          modal.hide();
          scope.cleanModal();
        };
      }
    };
  }
}
