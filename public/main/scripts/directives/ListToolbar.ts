﻿'use strict';
module backOffice.directives {
  export function listToolbar($window, $location) {
    return {
      restrict: 'A',
      scope: {
        items: '='
      },
      link: (scope, elem, attr)=> {
        scope.currentUrl = $location.path();
      },
      templateUrl: 'public/main/partials/otk/shared/listToolbar.html'
    };
  }
}
