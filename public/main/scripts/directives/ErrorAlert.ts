module backOffice.directives {

  export function errorAlert($rootScope:ng.IRootScopeService, $templateCache, $compile, $http):ng.IDirective {
    return {
      restrict: "EA",
      scope: true,
      link: ($scope:any, element:JQuery) => {
        var templateUrl = "public/main/scripts/directives/template/errorAlert.html";
        $rootScope.$watch("error", error => {
          if (error) {
            $scope.error = error.exceptionMessage || error.message;
            $scope.other = error;
            $rootScope["error"] = undefined;
            $http.get(templateUrl, { cache: $templateCache }).success(tplContent=> {
              element.replaceWith($compile(tplContent.trim())($scope));
            });
          }
        });

      }
    };
  }
  errorAlert.$inject = ["$rootScope", "$templateCache", "$compile", "$http"];
}