'use strict';
module backOffice.directives {
  export function fileUpload($http) {
    return {
      restrict: 'A',
      scope: {
        progress: '=',
        onSuccess: '=',
        onError: '=',
        onComplete: '=',
        canceled: '=',
        fileUpload: '=',
        onChange: '=',
        service: '='
      },
      link: (scope, elem, attr)=> {

        elem.bind('change', function () {
          var file = this.files[0];
          if (file === undefined)
            return;
          scope.$apply(()=> {
            if (scope.onChange)
              scope.onChange.call(scope.service);
            if (scope.progress)
              scope.progress.call(scope.service, 0);
          });
          uploadFile(file);
        });

        function transformResponse(response) {
          angular.forEach($http.defaults.transformResponse, transformFn=> {
            response = transformFn(response);
          });
          return response;
        }

        function uploadFile(file) {
          var fd = new FormData();
          fd.append('file', file);
          var xhr = new XMLHttpRequest();
          xhr.upload.addEventListener("progress", event=> {
            scope.$apply(()=> {
              if (scope.progress)
                scope.progress.call(scope.service, event.lengthComputable ? event.loaded * 100 / event.total : 0);
            });
          }, false);
          xhr.addEventListener("load", ()=> {
            scope.$apply(()=> {
              var response = transformResponse(xhr.response);
              if (scope.onSuccess)
                xhr.status === 200 && scope.onSuccess.call(scope.service, response);
              if (scope.onError)
                xhr.status !== 200 && scope.onError.call(scope.service, response);
              if (scope.onComplete)
                scope.onComplete.call(scope.service, response);
            });

          }, false);
          xhr.addEventListener("error", scope.failed, false);
          xhr.addEventListener("abort", scope.canceled, false);
          xhr.open("POST", scope.fileUpload);
          scope.progressVisible = true;
          xhr.send(fd);
        }
      }
    };
  }

  fileUpload.$inject = ['$http'];
}
