module backOffice.directives {
  highlightText.$inject= ['$compile'];
  export function highlightText($compile):ng.IDirective {
    return {
      scope: {
        highlightText: "=",
        hightlightObjects: "="
      },
      link: ($scope:ng.IScope, element:JQuery, attributes:any) => {
        element.addClass('ng-binding').data('$binding', attributes.highlightText);
        $scope.$watch("highlightText", (value) => {
          //Текст оборачивается во внешний тэг чтобы избежать обрезания слов в начале текста по неустановленной причине
          var fixedText = "<p>" + value + "</p>";
          var compiledContent = $compile(fixedText)($scope);
          element.html(compiledContent || '');
        });
      }
    };
  }

  objectid.$inject = ['ColorService', '$compile'];
  export function objectid(colorService:backOffice.services.ColorService, $compile):ng.IDirective {
    return {
      link: ($scope:ng.IScope, element:JQuery, attributes:any) => {
        var activeClass = "active";

        var textColor = element.css("color");
        var textDecoration = "underline";
        var hc = element.find(".highlight-control");
        var click = ()=> {
          var modalId = element.parents('.modal-body').attr('data-modal-id');
          var querySelector = "[objectId='" + attributes["objectid"] + "']";
          querySelector = "[data-modal-id=" + modalId + "] " + querySelector;
          if (!element.hasClass(activeClass)) {
            var rgb = colorService.toRGB(attributes["objectid"] * 100);
            var bgColor = colorService.toColor(rgb);

            $(querySelector)
                .addClass(activeClass)
                .css({ backgroundColor: bgColor, color: colorService.toColor(colorService.fontColor(rgb))});

          } else {

            $(querySelector)
                .removeClass(activeClass)
                .css({ backgroundColor: "transparent", color: textColor});

          }
          //element.css({/*backgroundColor: bgColor, borderColor: bgColor, color: toColor(fontColor(rgb))*/ borderBottom: "solid 2px " + bgColor });
        };
        //object из текста
        if (hc.length == 0) {
          element.bind("click", click);
          var text = element.text();
          var id = attributes['objectid'];
          var el = $('<span popover="" popover-title="Загрузка данных объекта  ..." popover-template="public/main/partials/object/info2.html" popover-trigger="mouseenter" popover-placement="right" popover-get-content="vm.getObjectInfluence(' + id + ')"></span>').text(text);
          var content = $compile(el)($scope.$parent);

          element.text('').append(content);
        } else { //objectid из sidebar
          hc.bind("click", click);
        }

      }
    };
  }
}