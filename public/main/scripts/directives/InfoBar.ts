﻿'use strict';

module backOffice.directives {
  export function infoBar($templateCache, $compile, $http, $state) {
    return {
      restrict: 'A',
      scope: {
        infoBar: '@',
        verifiedCount: '=',
        itemsOnPage: '=',
        totalCount: '=',
        deletedCount: '=',
        reportId: '=',
        trashUrl: '=',
        nextVerifiedState: '='
      },
      link: (scope, elem, attr)=> {
        scope.url = $state.href;
        scope.currentState = $state.current.name;
        $http.get(scope.infoBar, { cache: $templateCache }).success(tplContent=> {
          elem.replaceWith($compile(tplContent.trim())(scope));
        });
      }
    };
  };

  infoBar.$inject = ['$templateCache', '$compile', '$http', '$state'];
}