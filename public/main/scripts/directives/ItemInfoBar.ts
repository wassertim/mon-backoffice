'use strict';

module backOffice.directives {
  itemInfoBar.$inject = ['$templateCache', '$compile', '$http', '$state'];
  export function itemInfoBar($templateCache, $compile, $http, $state) {
    return {
      restrict: 'A',
      scope: {
        itemInfoBar: '=',
        selectAction: '=',
        service: '='
      },
      link: (scope, elem, attr)=> {
        scope.url = $state.href;
        scope.params = $state.params;
        scope.message = scope.itemInfoBar;
        scope.$watch("itemInfoBar", newValue=> {
          if (newValue) {
            scope.message = newValue;
            var relativeUrl = 'public/main/scripts/directives/template/itemInfoBar/';
            var templateUrl = relativeUrl + (scope.message.hasOwnProperty('articlesCount') ? 'cluster-info-bar.html' : 'article-info-bar.html');
            $http.get(templateUrl, { cache: $templateCache }).success(tplContent=> {
              elem.replaceWith($compile(tplContent.trim())(scope));
            });
          }
        });
        scope.onChange = ()=> {
          scope.selectAction.call(scope.service, undefined);
        };


      }
    };
  }
}

