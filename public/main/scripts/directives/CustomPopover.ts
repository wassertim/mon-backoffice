angular.module('ui.bootstrap.popover', ['ui.bootstrap.tooltip2'])
  .directive('popoverPopup', function () {
    return {
      restrict: 'EA',
      replace: true,
      scope: { ptitle: '@', content: '@', placement: '@', animation: '&', isOpen: '&', isContentLoaded: '=' },
      templateUrl: 'public/main/scripts/directives/template/popover/popover.html'
    };
  })
  .directive('popover', ['$compile', '$timeout', '$parse', '$window', '$tooltip2', function ($compile, $timeout, $parse, $window, $tooltip) {
    return $tooltip('popover', 'popover', 'click');
  }]);
angular.module('ui.bootstrap.tooltip2', ['ui.bootstrap.position', 'ui.bootstrap.bindHtml'])

/**
 * The $tooltip service creates tooltip- and popover-like directives as well as
 * houses global options for them.
 */
  .provider('$tooltip2', function () {
    // The default options tooltip and popover.
    var defaultOptions = {
      placement: 'top',
      animation: true,
      popupDelay: 0
    };

    // Default hide triggers for each show trigger
    var triggerMap = {
      'mouseenter': 'mouseleave',
      'click': 'click',
      'focus': 'blur'
    };

    // The options specified to the provider globally.
    var globalOptions = {};

    /**
     * `options({})` allows global configuration of all tooltips in the
     * application.
     *
     *   var app = angular.module( 'App', ['ui.bootstrap.tooltip'], function( $tooltipProvider ) {
     *     // place tooltips left instead of top by default
     *     $tooltipProvider.options( { placement: 'left' } );
     *   });
     */
    this.options = function (value) {
      angular.extend(globalOptions, value);
    };

    /**
     * This allows you to extend the set of trigger mappings available. E.g.:
     *
     *   $tooltipProvider.setTriggers( 'openTrigger': 'closeTrigger' );
     */
    this.setTriggers = function setTriggers(triggers) {
      angular.extend(triggerMap, triggers);
    };

    /**
     * This is a helper function for translating camel-case to snake-case.
     */
    function snake_case(name) {
      var regexp = /[A-Z]/g;
      var separator = '-';
      return name.replace(regexp, function (letter, pos) {
        return (pos ? separator : '') + letter.toLowerCase();
      });
    }

    /**
     * Returns the actual instance of the $tooltip service.
     * TODO support multiple triggers
     */
    this.$get = ['$window', '$compile', '$timeout', '$parse', '$document', '$position', '$interpolate', '$http', '$templateCache',
      function ($window, $compile, $timeout, $parse, $document, $position, $interpolate, $http, $templateCache) {
        return function $tooltip(type, prefix, defaultTriggerShow) {
          var options = angular.extend({}, defaultOptions, globalOptions);

          /**
           * Returns an object of show and hide triggers.
           *
           * If a trigger is supplied,
           * it is used to show the tooltip; otherwise, it will use the `trigger`
           * option passed to the `$tooltipProvider.options` method; else it will
           * default to the trigger supplied to this directive factory.
           *
           * The hide trigger is based on the show trigger. If the `trigger` option
           * was passed to the `$tooltipProvider.options` method, it will use the
           * mapped trigger from `triggerMap` or the passed trigger if the map is
           * undefined; otherwise, it uses the `triggerMap` value of the show
           * trigger; else it will just use the show trigger.
           */
          function getTriggers(trigger) {
            var show = trigger || options.trigger || defaultTriggerShow;
            var hide = triggerMap[show] || show;
            return {
              show: show,
              hide: hide
            };
          }

          var directiveName = snake_case(type);

          var startSym = $interpolate.startSymbol();
          var endSym = $interpolate.endSymbol();
          var template =
            '<' + directiveName + '-popup ' +
            'ptitle="' + startSym + 'tt_title' + endSym + '" ' +
            'content="' + startSym + 'tt_content' + endSym + '" ' +
            'placement="' + startSym + 'tt_placement' + endSym + '" ' +
            'is-content-loaded="isContentLoaded" style="width:300px" ' +
            'animation="tt_animation" ' +
            'is-open="tt_isOpen"' +
            '>' +
            '</' + directiveName + '-popup>';
          function withTemplate(templateUrl, handler) {
            $http({
              url: templateUrl,
              method: 'get',
              cache: $templateCache
            }).success((html) => {
              $timeout(() => {
                handler(html)
              });
            });
          }
          return {
            restrict: 'EA',
            scope: {
              service: '=',
              popoverGetContent: "&",
              popoverTemplate: '@'
            },
            link: function link(scope, element, attrs) {
              scope.isContentLoaded = false;
              var tooltip = $compile(template)(scope);
              var hideTimeOut = 200;
              var transitionTimeout;
              var popupTimeout;
              var $body = $document.find('body');
              var appendToBody = angular.isDefined(options.appendToBody) ? options.appendToBody : false;
              var triggers = getTriggers(undefined);
              var hasRegisteredTriggers = false;
              var hasEnableExp = angular.isDefined(attrs[prefix + 'Enable']);

              // By default, the tooltip is not open.
              // TODO add ability to start tooltip opened
              scope.tt_isOpen = false;

              function toggleTooltipBind() {
                if (!scope.tt_isOpen) {
                  showTooltipBind();
                } else {
                  hideTooltipBind();
                }
              }

              // Show the tooltip with delay if specified, otherwise show it immediately
              function showTooltipBind() {
                if (hasEnableExp && !scope.$eval(attrs[prefix + 'Enable'])) {
                  return;
                }
                if (scope[prefix + "GetContent"]) {
                  scope[prefix + "GetContent"]().then((content:any) => {
                    var templateUrl = scope[prefix + 'Template'];
                    if (templateUrl) {
                      withTemplate(templateUrl, (html)=>{
                        var scp:any = scope.$new();
                        scp.model = content;
                        var cnt = $compile(html)(scp);
                        scp.$digest();
                        scope.isContentLoaded = true;
                        scope.tt_content = cnt.get(0).outerHTML;
                        if (content.name) {
                          scope.tt_title = content.name;
                        }
                        changePosition(tooltip);
                      });
                    } else {
                      scope.isContentLoaded = true;
                      scope.tt_content = content;
                    }
                  });
                }
                if (scope.tt_popupDelay) {
                  popupTimeout = $timeout(show, scope.tt_popupDelay);
                } else {
                  scope.$apply(show);
                }
              }

              function hideTooltipBind() {
                scope.$apply(function () {
                  //hide();

                  tooltip.isMouseOut = true;
                  $timeout(hideIfMouseOut, hideTimeOut);

                });
              }

              function hideIfMouseOut() {
                if (tooltip.hasOwnProperty('isMouseOut')) {
                  if (tooltip.isMouseOut) {
                    hide();
                  }
                }
              }

              function calculatePosition(tooltip:any) {
                var ttPosition;
                var position:any = appendToBody ? $position.offset(element) : $position.position(element);

                var ttWidth = tooltip.prop('offsetWidth');
                var ttHeight = tooltip.prop('offsetHeight');
                switch (scope.tt_placement) {
                  case 'right':
                    ttPosition = {
                      top: position.top + position.height / 2 - ttHeight / 2,
                      left: position.left + position.width
                    };
                    break;
                  case 'bottom':
                    ttPosition = {
                      top: position.top + position.height,
                      left: position.left + position.width / 2 - ttWidth / 2
                    };
                    break;
                  case 'left':
                    ttPosition = {
                      top: position.top + position.height / 2 - ttHeight / 2,
                      left: position.left - ttWidth
                    };
                    break;
                  default:
                    ttPosition = {
                      top: position.top - ttHeight,
                      left: position.left + position.width / 2 - ttWidth / 2
                    };
                    break;
                }
                return ttPosition;
              }

              function changePosition(tooltip) {
                if (tooltip) {
                  var ttPosition = calculatePosition(tooltip);
                  ttPosition.top += 'px';
                  ttPosition.left += 'px';
                  tooltip.css(ttPosition);
                }
              }

              // Show the tooltip popup element.
              function show() {
                // If there is a pending remove transition, we must cancel it, lest the
                // tooltip be mysteriously removed.
                if (transitionTimeout) {
                  $timeout.cancel(transitionTimeout);
                }
                // Set the initial positioning.
                tooltip.css({ top: 0, left: 0, display: 'block', opacity: 0.9 });

                // Now we add it to the DOM because need some info about it. But it's not
                // visible yet anyway.
                if (appendToBody) {
                  $body.append(tooltip);
                } else {
                  element.after(tooltip);
                }
                tooltip.bind('mouseenter', function () {
                  tooltip.isMouseOut = false;
                });

                tooltip.bind('mouseleave', function () {
                  tooltip.isMouseOut = true;
                  $timeout(hideIfMouseOut, hideTimeOut);
                  //console.log('tooltip.mouseleave');
                });
                changePosition(tooltip);
                // And show the tooltip.
                scope.tt_isOpen = true;
              }

              // Hide the tooltip popup element.
              function hide() {
                tooltip.unbind('mouseleave');
                tooltip.unbind('mouseenter');
                // First things first: we don't show it anymore.
                scope.tt_isOpen = false;

                //if tooltip is going to be shown after delay, we must cancel this
                $timeout.cancel(popupTimeout);

                // And now we remove it from the DOM. However, if we have animation, we
                // need to wait for it to expire beforehand.
                // FIXME: this is a placeholder for a port of the transitions library.
                if (scope.tt_animation) {
                  transitionTimeout = $timeout(function () {
                    if (tooltip)
                      tooltip.remove();
                  }, 500);
                } else {
                  if (tooltip)
                    tooltip.remove();
                }
              }

              /**
               * Observe the relevant attributes.
               */
              attrs.$observe(type, function (val) {
                if (val) {
                  scope.tt_content = val;
                } else {
                  if (scope.tt_isOpen) {
                    hide();
                  }
                }
              });

              attrs.$observe(prefix + 'Title', function (val) {
                scope.tt_title = val;
              });

              attrs.$observe(prefix + 'Placement', function (val) {
                scope.tt_placement = angular.isDefined(val) ? val : options.placement;
              });

              attrs.$observe(prefix + 'Animation', function (val) {
                scope.tt_animation = angular.isDefined(val) ? !!val : options.animation;
              });

              attrs.$observe(prefix + 'PopupDelay', function (val) {
                var delay = parseInt(val, 10);
                scope.tt_popupDelay = !isNaN(delay) ? delay : options.popupDelay;
              });

              attrs.$observe(prefix + 'Trigger', function (val) {

                if (hasRegisteredTriggers) {
                  element.unbind(triggers.show, showTooltipBind);
                  element.unbind(triggers.hide, hideTooltipBind);
                }

                triggers = getTriggers(val);

                if (triggers.show === triggers.hide) {
                  element.bind(triggers.show, toggleTooltipBind);
                } else {
                  element.bind(triggers.show, showTooltipBind);
                  element.bind(triggers.hide, hideTooltipBind);
                }

                hasRegisteredTriggers = true;
              });

              attrs.$observe(prefix + 'AppendToBody', function (val) {
                appendToBody = angular.isDefined(val) ? $parse(val)(scope) : appendToBody;
              });

              // if a tooltip is attached to <body> we need to remove it on
              // location change as its parent scope will probably not be destroyed
              // by the change.
              if (appendToBody) {
                scope.$on('$locationChangeSuccess', function closeTooltipOnLocationChangeSuccess() {
                  if (scope.tt_isOpen) {
                    hide();
                  }
                });
              }

              // Make sure tooltip is destroyed and removed.
              scope.$on('$destroy', function onDestroyTooltip() {
                $timeout.cancel(popupTimeout);
                tooltip.remove();
                tooltip.unbind();
                tooltip = null;
                $body = null;
              });
            }
          };
        };
      }];
  });