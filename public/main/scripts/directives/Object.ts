app.directive('boObjectRole', () => {
  return {
    compile: (e, a) => {
      var roles = {
        '0': 'Все',
        '1': 'Главная',
        '2': 'Второстепенная',
        '3': 'Эпизодическая'
      };
      return (scope, element, attrs) => {
        element.html(roles[attrs.boObjectRole]);
      }
    }
  }
});
app.directive('boObjectQuotationType', () => {
  return {
    compile: (e, a) => {
      var qTypes = {
        '2': 'Нет',
        '1': 'Прямая речь',
        '0': 'Все'
      };
      return (s, element, attrs) => {
        element.html(qTypes[attrs.boObjectQuotationType]);
      }
    }
  }
});
app.directive('boObjectEmotion', () => {
  return {
    compile: (e, a) => {
      var data = {
        '0': 'Все',
        '1': 'Позитив',
        '2': 'Негатив',
        '3': 'Риск',
        '4': 'Нейтрал'
      };
      return (s, element, attrs) => {
        element.html(data[attrs.boObjectEmotion]);
      }
    }
  }
});
