'use strict';
module backOffice.directives {
  export function stickyBar($window) {
    return {
      restrict: 'A',
      link: (scope, elem, attr)=> {
        var wnd:any = angular.element($window);
        wnd.bind("scroll", (e:any) => {
          var elementOffset = elem.next().offset().top - elem.get(0).offsetHeight;
          var stickAtPoint = parseInt(attr.stickyBar);
          var stickyScrollOffset = elementOffset - stickAtPoint;
          //css animation can make stickyScrollOffset to be negative
          if (stickyScrollOffset > 0) {
            if (window.pageYOffset > stickyScrollOffset && !elem.hasClass('navbar-fixed-top')) {
              elem.next().css({ marginTop: elem.height() });
              elem.addClass('navbar-fixed-top').css({ top: stickAtPoint });
              elem.find('.navbar-inner').addClass('container');
            } else if (window.pageYOffset < stickyScrollOffset && elem.hasClass('navbar-fixed-top')) {
              elem.next().css({ marginTop: 0 });
              elem.find('.navbar-inner').removeClass('container');
              elem.removeClass('navbar-fixed-top').css({ top: 0 });
            }
          }
        });
      }
    };
  }

  stickyBar.$inject = ['$window'];
}
