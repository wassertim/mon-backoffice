module backOffice.directives {
  lazyImage.$inject = ['$window', 'ImageHelper'];
  export function lazyImage($window, imageService) {
    return {
      restrict: 'A',
      scope: {
        lazyImageSrc: '@',
        id: '@',
        containerId: '@'
      },
      link: (scope, elem, attr)=> {
        var width = attr.lazyWidth ? attr.lazyWidth : 100;
        var height = attr.lazyHeight ? attr.lazyHeight : 100;
        var resizeType = attr.lazyType ? attr.lazyType : 6;
        attr.$observe('lazyImageSrc', (val) => {
          if(imageService.isGuid(val)) {
            var imgSrc = imageService.getImageUrl(val, width, height, resizeType);
            if (elem.attr("src") !== imgSrc) {
              elem.attr("src", imgSrc);
            }
            var performLoadAnimation = ()=> {
              elem.removeClass("img-default");
              elem.addClass("img-loaded");
            };
            elem.on('load', ()=> {
              if (elem.attr("src") === imgSrc) {
                performLoadAnimation();
              }
            });
          }
        });
      }
    };
  }
}

