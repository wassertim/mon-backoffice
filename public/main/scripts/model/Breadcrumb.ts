module backOffice.model {
    export class Breadcrumb {
        constructor(
            public title: string,
            public url: string,
            public active: boolean) { }
    }
}