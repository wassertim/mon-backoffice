﻿var backOffice;
(function (backOffice) {
    (function (model) {
        var Breadcrumb = (function () {
            function Breadcrumb(title, url, active) {
                this.title = title;
                this.url = url;
                this.active = active;
            }
            return Breadcrumb;
        })();
        model.Breadcrumb = Breadcrumb;
    })(backOffice.model || (backOffice.model = {}));
    var model = backOffice.model;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (helpers) {
        var BreadcrumbHelper = (function () {
            function BreadcrumbHelper($stateParams) {
                this.$stateParams = $stateParams;
            }
            BreadcrumbHelper.prototype.pod = function (active) {
                return new backOffice.model.Breadcrumb('Картина дня', 'pod', active);
            };
            BreadcrumbHelper.prototype.settings = function (active) {
                return new backOffice.model.Breadcrumb('Настройки', 'settings', active);
            };
            BreadcrumbHelper.prototype['settings.queue'] = function (active) {
                return new backOffice.model.Breadcrumb('Очередь', 'settings.queue', active);
            };
            BreadcrumbHelper.prototype.monitor = function (active) {
                return new backOffice.model.Breadcrumb('Монитор', 'monitor', active);
            };
            BreadcrumbHelper.prototype.otk = function (active) {
                return new backOffice.model.Breadcrumb('ОТК', 'otk', active);
            };
            BreadcrumbHelper.prototype.otkConfigs = function (active) {
                return new backOffice.model.Breadcrumb('Все конфигурации', 'otk.configs', active);
            };
            BreadcrumbHelper.prototype.otConfigsByReport = function (active) {
                return new backOffice.model.Breadcrumb('По отчету ' + this.$stateParams['reportId'], 'otk.configsByReport', active);
            };
            BreadcrumbHelper.prototype.otkConfigReports = function (active) {
                return new backOffice.model.Breadcrumb('Отчеты', 'otk.configReports', active);
            };
            BreadcrumbHelper.prototype.otkConfigArticles = function (active) {
                return new backOffice.model.Breadcrumb('Сообщения', 'otk.configArticles', active);
            };
            BreadcrumbHelper.prototype.otkReports = function (active) {
                return new backOffice.model.Breadcrumb('Очередь', 'otk.reports', active);
            };
            BreadcrumbHelper.prototype.otkReport = function (active) {
                return new backOffice.model.Breadcrumb('Мой отчет', 'otk.report', active);
            };
            BreadcrumbHelper.prototype.otkArticleEdit = function (active) {
                return new backOffice.model.Breadcrumb('Редактирование ' + this.$stateParams['articleId'], 'otk.articleEdit', active);
            };
            BreadcrumbHelper.prototype.otkArticleDisplay = function (active) {
                return new backOffice.model.Breadcrumb('Просмотр ' + this.$stateParams['articleId'], 'otk.articleDisplay', active);
            };
            BreadcrumbHelper.prototype.otkClusterEdit = function (active) {
                return new backOffice.model.Breadcrumb('Редактирование ' + this.$stateParams['clusterId'], 'otk.clusterEdit', active);
            };
            BreadcrumbHelper.prototype['otk.clusterEdit.relevantList'] = function (active) {
                return new backOffice.model.Breadcrumb('Выбор главной статьи', 'otk.clusterEdit.relevantList', active);
            };
            BreadcrumbHelper.prototype.otkClusterDisplay = function (active) {
                return new backOffice.model.Breadcrumb('Просмотр ' + this.$stateParams['clusterId'], 'otk.clusterDisplay', active);
            };
            BreadcrumbHelper.prototype.otkClusterDisplayArticles = function (active) {
                return new backOffice.model.Breadcrumb('Статьи', 'otk.clusterDisplay.articles', active);
            };
            BreadcrumbHelper.prototype.otkReportConfigs = function (active) {
                return new backOffice.model.Breadcrumb('Конфигурации', 'otk.reportConfigs', active);
            };
            BreadcrumbHelper.prototype.otkReportArticles = function (active) {
                return new backOffice.model.Breadcrumb('Сообщения', 'otk.report.articles', active);
            };
            BreadcrumbHelper.prototype.otkReportClusters = function (active) {
                return new backOffice.model.Breadcrumb('События', 'otk.report.clusters', active);
            };
            BreadcrumbHelper.prototype.otkClusterPhotoEdit = function (active) {
                return new backOffice.model.Breadcrumb('Картинки', 'otk.clusterPhotoEdit', active);
            };
            BreadcrumbHelper.prototype.articlesTrash = function (active) {
                return new backOffice.model.Breadcrumb('Корзина статей', 'articlesTrash', active);
            };
            BreadcrumbHelper.prototype.clustersTrash = function (active) {
                return new backOffice.model.Breadcrumb('Корзина кластеров', 'clustersTrash', active);
            };
            BreadcrumbHelper.prototype.clusterArticlesTrash = function (active) {
                return new backOffice.model.Breadcrumb('Корзина сообщений кластера', 'clusterArticlesTrash', active);
            };
            BreadcrumbHelper.prototype.otkArticleDisplayMoreArticles = function (active) {
                return new backOffice.model.Breadcrumb('Еще на тему', 'otk.articleDisplay.moreArticles', active);
            };
            return BreadcrumbHelper;
        })();
        helpers.BreadcrumbHelper = BreadcrumbHelper;
    })(backOffice.helpers || (backOffice.helpers = {}));
    var helpers = backOffice.helpers;
})(backOffice || (backOffice = {}));
var backofficeGlobals;
var app = angular.module('anbo', [
    'ngRoute',
    'ui.router.compat',
    'ngAnimate',
    'angularMoment',
    'ui.bootstrap',
    'angular-carousel',
    'ui.router.stateHelper'
]);
var backOffice;
(function (backOffice) {
    (function (global) {
        var appTypes = {
            '0': 'Other',
            '1': 'Web',
            '2': 'Ipad',
            '3': 'Plasma',
            '4': 'Glass2'
        };
    })(backOffice.global || (backOffice.global = {}));
    var global = backOffice.global;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    var Global = (function () {
        function Global() {
        }
        Global.isLoginOpen = false;
        Global.pdfUrl = backofficeGlobals.pdfUrl;
        Global.imageHost = backofficeGlobals.imageHost;
        return Global;
    })();
    backOffice.Global = Global;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (helpers) {
        var ImageHelper = (function () {
            function ImageHelper() {
            }
            ImageHelper.prototype.getImageUrl = function (imageId, width, height, type) {
                return (function () {
                    if (backOffice.Global.imageHost === 'http://lorempixel.com')
                        return backOffice.Global.imageHost + '/' + width + '/' + height + '?id=' + imageId;
                    else
                        return backOffice.Global.imageHost + '/Monitor.Image.Web/Image/GetById/' + imageId + '/A4/1' + '/' + width + '/' + height + '/' + type;
                })();
            };
            ImageHelper.prototype.isGuid = function (source) {
                return /[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/.test(source);
            };

            ImageHelper.prototype.getIdFromUrl = function (url) {
                var re = /[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/;
                var resultArray = url.match(re);
                return resultArray[0];
            };
            return ImageHelper;
        })();
        helpers.ImageHelper = ImageHelper;
        app.service('ImageHelper', ImageHelper);
    })(backOffice.helpers || (backOffice.helpers = {}));
    var helpers = backOffice.helpers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (helpers) {
        var RandomHelper = (function () {
            function RandomHelper() {
            }
            RandomHelper.prototype.getCharacters = function (quantity) {
                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                for (var i = 0; i < quantity; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                return text;
            };
            return RandomHelper;
        })();
        helpers.RandomHelper = RandomHelper;
    })(backOffice.helpers || (backOffice.helpers = {}));
    var helpers = backOffice.helpers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (common) {
            var BaseController = (function () {
                function BaseController($scope, $state, $localState, load) {
                    this.$scope = $scope;
                    this.$state = $state;
                    this.load = load;
                    $scope.vm = this;
                    this.url = $state.href;
                    if ($localState)
                        this.execStateConfig = $localState.self;
                }
                BaseController.prototype.init = function () {
                    this.load && this.load();
                    this.execStateConfig && this.$scope.vm[this.execStateConfig.action] && this.$scope.vm[this.execStateConfig.action].call(this);
                };
                return BaseController;
            })();
            common.BaseController = BaseController;
        })(controllers.common || (controllers.common = {}));
        var common = controllers.common;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (common) {
            var Controller = (function () {
                function Controller($scope) {
                    $scope.vm = this;
                }
                return Controller;
            })();
            common.Controller = Controller;
        })(controllers.common || (controllers.common = {}));
        var common = controllers.common;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (common) {
            var ListController = (function (_super) {
                __extends(ListController, _super);
                function ListController($scope, configService, reportQueueService, $state) {
                    var _this = this;
                    _super.call(this, $scope);
                    this.configService = configService;
                    this.reportQueueService = reportQueueService;
                    this.$state = $state;
                    this.isRefreshingCache = false;
                    this.isSaveInProgress = false;
                    this.isItemsLoaded = false;
                    this.pageSize = 100;
                    this.currentPage = 1;
                    this.isConfigListLoaded = false;
                    this.itemList = {
                        totalCount: 0,
                        items: [],
                        isLoaded: false
                    };
                    this.appTypes = {
                        '0': 'Other',
                        '1': 'Web',
                        '2': 'Ipad',
                        '3': 'Plasma',
                        '4': 'Glass2'
                    };
                    this.periodTranslation = {
                        "1": "День",
                        "3": "Неделя",
                        "4": "Месяц",
                        "2": "Плазма"
                    };
                    this.configPluralizer = {
                        '1': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
                        '21': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
                        '31': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
                        'other': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурациях',
                        '0': '',
                        '-1': ''
                    };
                    this.verifiedState = $state.params.vs;
                    this.nextVerifiedState = this.verifiedState === "SV" ? "HV" : "SV";
                    this.verifiedHidden = this.verifiedState === "HV";
                    this.ls = new backOffice.services.ListService(function () {
                        return _this.itemList;
                    });
                }
                ListController.prototype.getConfigurations = function () {
                    var _this = this;
                    return this.configService.getConfigurationsByReportId(this.reportId).then(function (items) {
                        _.forEach(items, function (item) {
                            return item.applicationType = _this.appTypes[item.applicationType];
                        });
                        return items;
                    });
                };

                ListController.prototype.loadConfigurations = function (reportId) {
                    var _this = this;
                    this.isConfigListLoaded = false;
                    return this.configService.getConfigurationsByReportId(reportId).then(function (items) {
                        _this.configs = items;
                        _this.isConfigListLoaded = true;
                    });
                };

                ListController.prototype.forceReportRefresh = function () {
                    var _this = this;
                    if (this.report.reportId) {
                        this.isRefreshingCache = true;
                        this.reportQueueService.forceReportRefresh(this.report.reportId).then(function (r) {
                            _this.isRefreshingCache = false;
                        });
                    }
                };

                ListController.prototype.saveButtonText = function () {
                    if (!this.isSaveInProgress) {
                        return "Сохранить отчет";
                    } else {
                        return "Идет сохранение отчета ...";
                    }
                };

                ListController.prototype.saveAndLoadNewButtonText = function () {
                    if (!this.isSaveInProgress) {
                        return "Сохранить отчет и загрузить новый";
                    } else {
                        return "Идет сохранение отчета ...";
                    }
                };

                ListController.prototype.save = function (onSuccess) {
                    var _this = this;
                    if (this.isSaveInProgress || !this.report.isLoaded)
                        return;
                    this.isSaveInProgress = true;
                    this.reportQueueService.saveReport().then(function () {
                        _this.isSaveInProgress = false;
                        onSuccess();
                    }, function (err) {
                        console.log(err);
                    });
                };

                ListController.prototype.verifiedHiddenText = function () {
                    if (this.verifiedHidden) {
                        return "Показать проверенные";
                    } else {
                        return "Скрыть проверенные";
                    }
                };

                ListController.prototype.saveReport = function () {
                    var _this = this;
                    this.save(function () {
                        _this.$state.go('otk.reports');
                    });
                };

                ListController.prototype.saveAndLoadNew = function () {
                    var _this = this;
                    this.save(function () {
                        _this.$state.go('otk.report', {}, { reload: true, inherit: true, notify: true });
                    });
                };
                return ListController;
            })(controllers.common.Controller);
            common.ListController = ListController;
        })(controllers.common || (controllers.common = {}));
        var common = controllers.common;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (common) {
            var ModalController = (function (_super) {
                __extends(ModalController, _super);
                function ModalController($scope, $modalInstance) {
                    _super.call(this, $scope);
                    this.$modalInstance = $modalInstance;
                }
                ModalController.prototype.close = function () {
                    this.$modalInstance.dismiss('cancel');
                };
                return ModalController;
            })(common.Controller);
            common.ModalController = ModalController;
        })(controllers.common || (controllers.common = {}));
        var common = controllers.common;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        var BreadcrumbController = (function () {
            function BreadcrumbController($scope, $rootScope, $stateParams, $state) {
                this.$state = $state;
                $scope.vm = this;
                $scope.reportId = $stateParams.reportId;
                $scope.verifiedState = $stateParams.vs;
                $scope.breadcrumbs = [];

                $scope.$on("$stateChangeSuccess", function () {
                    if ($state.current.breadcrumbs)
                        $scope.breadcrumbs = $state.current.breadcrumbs($stateParams);
                });
            }
            BreadcrumbController.prototype.currentUrl = function (stateName) {
                return this.$state.href(stateName, this.$state.params);
            };
            BreadcrumbController.$inject = ['$scope', '$rootScope', '$stateParams', '$state'];
            return BreadcrumbController;
        })();
        controllers.BreadcrumbController = BreadcrumbController;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        var ConfigController = (function () {
            function ConfigController($scope, $state, $stateParams, configService) {
                this.$scope = $scope;
                this.$state = $state;
                this.$stateParams = $stateParams;
                this.configService = configService;
                this.getConfigurationsByReportId = function () {
                    var _this = this;
                    this.$scope.isConfigListLoaded = false;
                    this.configService.getConfigurationsByReportId(this.$stateParams.reportId).then(function (items) {
                        _this.$scope.configs = items;
                        _this.$scope.isConfigListLoaded = true;
                    });
                };
                $scope.configs = [];
                $scope.isConfigListLoaded = false;
                this.init();
            }
            ConfigController.prototype.init = function () {
                if (this.$state.current.name === "otk.configsByReport")
                    this.getConfigurationsByReportId();
            };
            ConfigController.$inject = ['$scope', '$state', '$stateParams', 'ConfigService'];
            return ConfigController;
        })();
        controllers.ConfigController = ConfigController;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        var DeleteRestoreController = (function (_super) {
            __extends(DeleteRestoreController, _super);
            function DeleteRestoreController($scope, $modalInstance, params) {
                var _this = this;
                _super.call(this, $scope, $modalInstance);
                this.$modalInstance = $modalInstance;
                angular.extend($scope, params);
                $scope.action = function () {
                    var promise = params._action();
                    if (promise) {
                        $scope.isActionActive = true;
                        promise.then(function () {
                            $scope.isActionActive = false;
                            _this.$modalInstance.close('ok');
                        }, function () {
                            $scope.isActionActive = false;
                        });
                    }
                };
                $scope.secondaryAction = function () {
                    var promise = $scope._secondaryAction();
                    if (promise) {
                        $scope.isSecondaryActionActive = true;
                        promise.then(function () {
                            $scope.isSecondaryActionActive = false;
                            _this.$modalInstance.close('ok');
                        }, function () {
                            $scope.isSecondaryActionActive = false;
                        });
                    }
                };
            }
            DeleteRestoreController.$inject = ['$scope', '$modalInstance', 'params'];
            return DeleteRestoreController;
        })(controllers.common.ModalController);
        controllers.DeleteRestoreController = DeleteRestoreController;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        var ModalSecurityController = (function (_super) {
            __extends(ModalSecurityController, _super);
            function ModalSecurityController($scope, $state, securityService, $modalInstance) {
                _super.call(this, $scope, $modalInstance);
                this.$scope = $scope;
                this.$state = $state;
                this.securityService = securityService;
                this.$modalInstance = $modalInstance;
                this.isLoading = false;
            }
            ModalSecurityController.prototype.signIn = function () {
                var _this = this;
                this.isLoading = true;
                this.$scope.$broadcast("autofill:update");
                this.securityService.signIn(this.auth).then(function (identity) {
                    _this.isLoading = false;
                    if (identity.isAuthenticated) {
                        if (_this.$modalInstance) {
                            _this.$modalInstance.close('ok');
                        } else {
                            _this.$state.go('otk.reports');
                        }
                    } else {
                        _this.showError('Логин или пароль не верны');
                    }
                });
            };

            ModalSecurityController.prototype.closeAlert = function (index) {
                this.alerts.splice(index, 1);
            };

            ModalSecurityController.prototype.showError = function (error) {
                this.alerts = [{
                        type: 'danger',
                        msg: error
                    }];
            };
            ModalSecurityController.$inject = ['$scope', '$state', 'SecurityService', '$modalInstance'];
            return ModalSecurityController;
        })(controllers.common.ModalController);
        controllers.ModalSecurityController = ModalSecurityController;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        var NavController = (function (_super) {
            __extends(NavController, _super);
            function NavController($scope, $location, $state, $modal, securityService) {
                _super.call(this, $scope, $state);
                this.$scope = $scope;
                this.$location = $location;
                this.$state = $state;
                this.$modal = $modal;
                this.securityService = securityService;
            }
            NavController.prototype.isLogin = function () {
                return this.$state.current.name === "login";
            };

            NavController.prototype.lastIndex = function (array) {
                return array.length - 1;
            };

            NavController.prototype.isActive = function (path) {
                return _.contains(this.$location.path(), path);
            };

            NavController.prototype.getActive = function (path) {
                if (this.isActive(path)) {
                    return "active";
                } else {
                    return "";
                }
            };

            NavController.prototype.logOff = function (e) {
                var _this = this;
                e.preventDefault();
                var modalInstance = this.$modal.open({
                    templateUrl: 'public/main/partials/modals/confirmation.html',
                    controller: controllers.shared.ModalConfirmationController,
                    resolve: {
                        params: function () {
                            return {
                                message: 'Вы действительно хотите выйти из системы?',
                                okText: 'Выйти',
                                cancelText: 'Отменить'
                            };
                        }
                    }
                });
                modalInstance.result.then(function () {
                    _this.securityService.signOff();
                    _this.$state.go('login');
                });
            };
            NavController.$inject = ['$scope', '$location', '$state', '$modal', 'SecurityService'];
            return NavController;
        })(controllers.common.BaseController);
        controllers.NavController = NavController;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        var ReportController = (function (_super) {
            __extends(ReportController, _super);
            function ReportController($rootScope, $scope, $stateParams, $location, $http, reportService, reportQueueService, $state, $localState, nav, configService) {
                _super.call(this, $scope, $state, $localState, $scope.getReportInfo);
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.$location = $location;
                this.$http = $http;
                this.reportService = reportService;
                this.reportQueueService = reportQueueService;
                this.$state = $state;
                this.$localState = $localState;
                this.nav = nav;
                this.configService = configService;
                this.showInfo = true;
                this.messages = [];
                this.isReportInfoHidden = false;
                this.isSaveInProgress = false;
                this.isConfigListLoaded = false;
                this.isRefreshingCache = false;
                this.isArticlesActive = false;
                this.isClustersActive = false;
                this.periodTranslation = {
                    "1": "День",
                    "3": "Неделя",
                    "4": "Месяц",
                    "2": "Плазма"
                };
                this.configPluralizer = {
                    '1': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
                    '21': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
                    '31': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурации',
                    'other': 'и ещё в {{vm.report.numberOfConfigurations-1}} конфигурациях',
                    '0': '',
                    '-1': ''
                };

                this.verifiedState = $state.params.vs;
                this.nextVerifiedState = $state.params.vs === "SV" ? "HV" : "SV";
                this.verifiedHidden = $stateParams.vs === "HV";

                this.configId = $stateParams.configId;
                this.report = this.defaultReport();

                this.getReportInfo();
            }
            ReportController.prototype.defaultReport = function () {
                return {
                    reportId: 0,
                    numberOfConfigurations: 0,
                    isLoaded: false,
                    'type': 1
                };
            };

            ReportController.prototype.forceReportRefresh = function () {
                var _this = this;
                if (this.report.reportId) {
                    this.isRefreshingCache = true;
                    this.reportQueueService.forceReportRefresh(this.report.reportId).then(function (r) {
                        _this.isRefreshingCache = false;
                    });
                }
            };

            ReportController.prototype.getReportInfo = function () {
                var _this = this;
                this.reportQueueService.getReport().then(function (report) {
                    if (report === "null") {
                        _this.$state.go("otk.reports");
                    } else {
                        _this.report = report;

                        _this.loadConfigurations(report.reportId);
                        if (_this.$state.current.name === 'otk.report') {
                            _this.redirect(report.type);
                        }
                    }
                });
            };

            ReportController.prototype.loadConfigurations = function (reportId) {
                var _this = this;
                this.isConfigListLoaded = false;
                this.configService.getConfigurationsByReportId(reportId).then(function (items) {
                    _this.configs = items;
                    _this.isConfigListLoaded = true;
                });
            };

            ReportController.prototype.redirect = function (type) {
                switch (type) {
                    case 1:
                        this.$state.go('otk.report.articles', { vs: 'HV' });
                        break;
                    case 2:
                        this.$state.go('otk.report.clusters', { vs: 'HV' });
                        break;
                    default:
                        this.$state.go('otk.report.articles', { vs: 'HV' });
                        break;
                }
            };
            ReportController.$inject = ['$rootScope', '$scope', '$stateParams', '$location', '$http', 'ReportService', 'ReportQueueService', '$state', '$$state', 'NavService', 'ConfigService'];
            return ReportController;
        })(controllers.common.BaseController);
        controllers.ReportController = ReportController;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        var ReportQueueController = (function (_super) {
            __extends(ReportQueueController, _super);
            function ReportQueueController($scope, $stateParams, $state, $localState, $location, reportQueueService, configService) {
                _super.call(this, $scope, $state, $localState);
                this.$scope = $scope;
                this.$state = $state;
                this.reportQueueService = reportQueueService;
                this.configService = configService;
                this.showInfo = true;
                this.isReportInfoHidden = true;
                this.isReportLoading = false;
                this.isConfigsLoaded = false;
                this.isInfoLoaded = false;
                this.priority = 1;
                this.configToAdd = { displayName: "", operatorPriority: 1 };
                this.configs = [];
                this.allConfigs = [];
                this.appTypes = {
                    '0': 'Other',
                    '1': 'Web',
                    '2': 'Ipad',
                    '3': 'Plasma',
                    '4': 'Glass2'
                };
                this.alertBlock = {
                    type: 'danger',
                    message: 'Произошла ошибка. Повторите пожалуйста запрос еще раз. Если ошибка будет повторяться - обратитесь в службу поддержки?',
                    show: false
                };

                this.verifiedState = $stateParams["vs"];
                this.nextVerifiedState = $stateParams["vs"] === "SV" ? "HV" : "SV";
                this.verifiedHidden = $stateParams["vs"] === "HV";

                this.init();
            }
            ReportQueueController.prototype.showAlert = function (message, type) {
                if (message) {
                    this.alertBlock.message = message;
                }
                this.alertBlock.type = type;
                this.alertBlock.show = true;
            };

            ReportQueueController.prototype.configFilter = function (item) {
                return !_.any(this.configs, function (config) {
                    return config.id == item.id;
                });
            };

            ReportQueueController.prototype.goodToAdd = function (config) {
                return config && (typeof (config) === "object") && config.displayName && !_.any(this.configs, function (conf) {
                    return conf.id === config.id;
                });
            };

            ReportQueueController.prototype.checkboxClick = function (e) {
                e.stopPropagation();
            };

            ReportQueueController.prototype.loadReportButtonText = function () {
                if (!this.isReportLoading) {
                    return "Загрузить отчет из очереди";
                } else {
                    return "Идет загрузка отчета из очереди ...";
                }
            };

            ReportQueueController.prototype.getTopReport = function () {
                var _this = this;
                this.isReportLoading = true;
                this.reportQueueService.getReport().then(function (report) {
                    _this.isReportLoading = false;
                    if (report == "null") {
                        _this.showAlert("В очереди нет непроверенных отчетов", "danger");
                        return;
                    }
                    var types = ['', 'articles', 'clusters', 'mixed'];
                    switch (types[report.type]) {
                        case 'articles':
                            _this.$state.go('otk.report.articles', { vs: 'HV' });
                            break;
                        case 'clusters':
                            _this.$state.go('otk.report.clusters', { vs: 'HV' });
                            break;
                        default:
                            _this.$state.go('otk.report.articles', { vs: 'HV' });
                            break;
                    }
                }, function (error) {
                    _this.isReportLoading = false;
                });
            };

            ReportQueueController.prototype.deleteConfigurationFromQueue = function () {
                var _this = this;
                _.forEach(_.where(this.configs, { 'selected': true }), function (config) {
                    config.deleted = true;
                    config.selected = false;

                    _this.reportQueueService.deleteConfigurationFromQueue(config.id).then(function () {
                        _.remove(_this.configs, { id: config.id });
                    }, function () {
                        config.deleted = false;
                    });
                });
            };

            ReportQueueController.prototype.deleteConfigurationFromQueueBatch = function () {
                var selectedConfigs = _.where(this.configs, { 'selected': true });
                if (selectedConfigs.length === 0)
                    return;
                var selectedConfigIds = _.map(selectedConfigs, function (config) {
                    return config.id;
                });
                _.forEach(selectedConfigs, function (config) {
                    config.deleted = true;
                    config.selected = false;
                });
                this.reportQueueService.deleteConfigurationFromQueue(selectedConfigIds).then(function success() {
                    _.remove(this.configs, { deleted: true });
                }, function error() {
                    _.forEach(_.where(this.configs, { 'deleted': true }), function (item) {
                        item.deleted = false;
                    });
                });
            };

            ReportQueueController.prototype.addConfigurationToQueue = function (config) {
                var _this = this;
                if (this.goodToAdd(config)) {
                    var configCopy = angular.copy(config);
                    this.configToAdd = { displayName: "", operatorPriority: 1 };
                    this.$scope.form.$setPristine();
                    configCopy.notAdded = true;
                    this.configs.push(configCopy);
                    this.reportQueueService.addConfigurationToQueue(configCopy).then(function () {
                        configCopy.notAdded = false;
                        _.remove(_this.allConfigs, { id: configCopy.id });
                    });
                }
            };

            ReportQueueController.prototype.getAllPublishTerminalConfiguration = function () {
                var _this = this;
                this.configService.getAllPublishTerminalConfiguration().then(function (configs) {
                    _.forEach(configs, function (config) {
                        config.getApplicationType = function () {
                            return _this.appTypes[config.applicationType];
                        };
                    });
                    _this.allConfigs = configs;
                });
            };

            ReportQueueController.prototype.getConfigurationsOnControl = function () {
                var _this = this;
                this.reportQueueService.getConfigurationsOnControl().then(function (configs) {
                    _this.isConfigsLoaded = true;
                    _this.configs = configs;
                    return configs;
                });
            };

            ReportQueueController.prototype.getQueueStatistic = function () {
                var _this = this;
                this.reportQueueService.getQueueStatistic().then(function (queueInfo) {
                    _this.isInfoLoaded = true;
                    _this.reportQueue = queueInfo;
                });
            };

            ReportQueueController.prototype.getSettingsData = function () {
                this.getConfigurationsOnControl();
                this.getAllPublishTerminalConfiguration();
            };
            ReportQueueController.$inject = ['$scope', '$stateParams', '$state', '$$state', '$location', 'ReportQueueService', 'ConfigService'];
            return ReportQueueController;
        })(controllers.common.BaseController);
        controllers.ReportQueueController = ReportQueueController;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (shared) {
            var ModalConfirmationController = (function (_super) {
                __extends(ModalConfirmationController, _super);
                function ModalConfirmationController($scope, $modalInstance, params) {
                    _super.call(this, $scope, $modalInstance);
                    this.$modalInstance = $modalInstance;
                    this.params = params;
                }
                ModalConfirmationController.prototype.ok = function () {
                    this.$modalInstance.close('ok');
                };
                ModalConfirmationController.$inject = ['$scope', '$modalInstance', 'params'];
                return ModalConfirmationController;
            })(controllers.common.ModalController);
            shared.ModalConfirmationController = ModalConfirmationController;
        })(controllers.shared || (controllers.shared = {}));
        var shared = controllers.shared;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        var SecurityController = (function () {
            function SecurityController($scope, $state, securityService, $modal) {
                this.$scope = $scope;
                this.$state = $state;
                this.securityService = securityService;
                this.$modal = $modal;
                this.isLoading = false;
                $scope.vm = this;
            }
            SecurityController.prototype.signIn = function () {
                var _this = this;
                this.isLoading = true;
                this.$scope.$broadcast("autofill:update");
                this.securityService.signIn(this.auth).then(function (identity) {
                    _this.isLoading = false;
                    if (identity.isAuthenticated) {
                        _this.$state.go('otk.reports');
                    } else {
                        _this.showError('Логин или пароль не верны');
                    }
                });
            };

            SecurityController.prototype.closeAlert = function (index) {
                this.alerts.splice(index, 1);
            };

            SecurityController.prototype.showError = function (error) {
                this.alerts = [
                    {
                        type: 'danger',
                        msg: error
                    }
                ];
            };
            SecurityController.$inject = ['$scope', '$state', 'SecurityService', '$modal'];
            return SecurityController;
        })();
        controllers.SecurityController = SecurityController;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        (function (common) {
            var BaseService = (function () {
                function BaseService($http) {
                    this.$http = $http;
                }
                BaseService.prototype.res = function (url) {
                    return this.$http.get(url).then(function (r) {
                        return r.data;
                    });
                };
                BaseService.prototype.req = function (conf) {
                    return this.$http(conf).then(function (r) {
                        return r.data;
                    });
                };
                return BaseService;
            })();
            common.BaseService = BaseService;
        })(services.common || (services.common = {}));
        var common = services.common;
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var ColorService = (function () {
            function ColorService() {
            }
            ColorService.prototype.toRGB = function (num) {
                num >>>= 0;
                var b = num & 0xFF, g = (num & 0xFF00) >>> 8, r = (num & 0xFF0000) >>> 16, a = ((num & 0xFF000000) >>> 24) / 255;
                return {
                    R: r,
                    G: g,
                    B: b
                };
            };

            ColorService.prototype.fontColor = function (color) {
                var d = 0;
                var a = 1 - (0.299 * color.R + 0.587 * color.G + 0.114 * color.B) / 255;

                if (a < 0.5)
                    d = 0;
                else
                    d = 255;
                return { R: d, G: d, B: d };
            };

            ColorService.prototype.toColor = function (color) {
                return "rgb(" + [color.R, color.G, color.B].join(",") + ")";
            };
            return ColorService;
        })();
        services.ColorService = ColorService;
        app.service('ColorService', ColorService);
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var NavService = (function () {
            function NavService($location, $state) {
                this.$location = $location;
                this.$state = $state;
            }
            NavService.prototype.isLogin = function () {
                return this.$state.current.name === "login";
            };

            NavService.prototype.lastIndex = function (array) {
                return array.length - 1;
            };

            NavService.prototype.isActiveState = function (stateName) {
                return stateName === this.$state.current.name;
            };

            NavService.prototype.isActive = function (path) {
                console.log(path);
                var active = _.contains(this.$location.path(), path.replace('#', ''));
                console.log(path + ': isActive? ' + active);
                return active;
            };

            NavService.prototype.getActive = function (path) {
                if (this.isActive(path)) {
                    return "active";
                } else {
                    return "";
                }
            };
            NavService.$inject = ['$location', '$state'];
            return NavService;
        })();
        services.NavService = NavService;
        app.service('NavService', NavService);
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var ListService = (function () {
            function ListService(itemList, params) {
                this.itemList = itemList;
                this.params = params;
                this.deletedCount = 0;
                this.selectedCount = 0;
            }
            ListService.prototype.getSelectedIds = function () {
                return _.map(this.getSelectedItems(), function (item) {
                    return item.id;
                });
            };

            ListService.prototype.deleteSelectedItems = function () {
                this.deleteItemsFromList(this.getSelectedIds());
            };

            ListService.prototype.deleteItemsFromList = function (ids) {
                var _this = this;
                var deleteItems = function () {
                    var deleted = _.remove(_this.itemList().items, function (item) {
                        return _.contains(ids, item.id);
                    });
                    _this.deletedCount += deleted.length;
                    _this.itemList().totalCount -= deleted.length;
                    _this.selectedCount = 0;
                };
                deleteItems();
            };

            ListService.prototype.updateSelectedCount = function () {
                this.selectedCount = this.getSelectedItems().length;
                return this.selectedCount;
            };

            ListService.prototype.getSelectedItems = function () {
                return _.where(this.itemList().items, { 'selected': true });
            };

            ListService.prototype.invertSelected = function (e) {
                e.preventDefault();
                _.forEach(this.itemList().items, function (item) {
                    item.selected = !item.selected;
                });
                this.updateSelectedCount();
            };

            ListService.prototype.selectItem = function (item, e) {
                if (e) {
                    e.preventDefault();
                }
                item.selected = !item.selected;
                this.updateSelectedCount();
            };

            ListService.prototype.toggleSelected = function (e) {
                e.preventDefault();
                this.updateSelectedCount();
                var selected;
                if (this.selectedCount === 0) {
                    selected = true;
                }
                _.forEach(this.itemList().items, function (item) {
                    item.selected = selected;
                });
                this.updateSelectedCount();
            };

            ListService.prototype.toggleSelectButtonText = function () {
                if (this.selectedCount > 0) {
                    return 'Снять выделение';
                } else {
                    return 'Выделить все';
                }
            };
            return ListService;
        })();
        services.ListService = ListService;
        app.service('ListService', ListService);
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var ArticleService = (function (_super) {
            __extends(ArticleService, _super);
            function ArticleService($http, routerService) {
                _super.call(this, $http);
                this.$http = $http;
                this.routerService = routerService;
                this.articleController = "Article";
            }
            ArticleService.prototype.getCounters = function (reportId, period) {
                return this.$http(this.routerService.action('Article', "GetCounters", { reportId: reportId, period: period })).then(function (r) {
                    return r.data;
                });
            };

            ArticleService.prototype.getByReport = function (reportId, period, isVerifiedExcluded, pageSize, pageNumber) {
                return this.$http(this.routerService.action('Article', 'GetByReport', {
                    reportId: reportId,
                    period: period,
                    isVerifiedExcluded: isVerifiedExcluded,
                    pageSize: pageSize,
                    pageNumber: pageNumber
                })).then(function (response) {
                    return response.data;
                });
            };

            ArticleService.prototype.getByReportCount = function (reportId, isVerifiedExcluded, period) {
                return this.$http(Router.action(this.articleController, "GetByReportCount", { reportId: reportId, period: period, isVerifiedExcluded: isVerifiedExcluded })).then(function (response) {
                    return response.data;
                });
            };

            ArticleService.prototype.getDeletedFromReportCount = function (reportId, period) {
                return this.$http(this.routerService.action('Article', 'GetDeletedFromReportCount', { reportId: reportId, period: period })).then(function (response) {
                    return response.data;
                });
            };

            ArticleService.prototype.getDeletedFromReport = function (reportId, period, pageSize, pageNumber) {
                return this.$http(this.routerService.action('Article', 'GetDeletedFromReport', { reportId: reportId, pageSize: pageSize, pageNumber: pageNumber, period: period })).then(function (response) {
                    return response.data;
                });
            };

            ArticleService.prototype.getById = function (articleId, reportId, imgW, imgH) {
                return this.$http(this.routerService.action('Article', "GetById", { articleId: articleId, reportId: reportId })).then(function (response) {
                    return response.data;
                });
            };

            ArticleService.prototype.getItemById = function (articleId, reportId, imgW, imgH) {
                return this.$http(this.routerService.action('Article', "GetItemById", { articleId: articleId, reportId: reportId })).then(function (response) {
                    return response.data;
                });
            };

            ArticleService.prototype.setImage = function (articleId, imageId) {
                return this.$http(angular.extend(this.routerService.action("Article", "SetImage", { articleId: articleId }), { data: { imageId: imageId } })).then(function (response) {
                    return response.data;
                });
            };

            ArticleService.prototype.getMore = function (articleId) {
                return this.$http(Router.action("Article", "GetMore", { articleId: articleId })).then(function (response) {
                    return response.data;
                });
            };

            ArticleService.prototype.restoreToReport = function (articleIds, reportId) {
                return this.$http(angular.extend(this.routerService.action("Article", "RestoreToReport", { reportId: reportId }), { data: articleIds })).then(function (r) {
                    return r.data;
                });
            };

            ArticleService.prototype.restoreEverywhere = function (articleIds) {
                return this.$http(angular.extend(this.routerService.action("Article", "RestoreEverywhere"), { data: articleIds })).then(function (r) {
                    return r.data;
                });
            };

            ArticleService.prototype.deleteEverywhere = function (articleIds) {
                return this.$http(angular.extend(this.routerService.action("Article", "DeleteEverywhere"), {
                    data: articleIds,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })).then(function (r) {
                    return r.data;
                });
            };

            ArticleService.prototype.deleteFromReport = function (articleIds, reportId) {
                return this.$http(angular.extend(this.routerService.action("Article", "DeleteFromReport", { reportId: reportId }), {
                    data: articleIds,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })).then(function (r) {
                    return r.data;
                });
            };
            ArticleService.prototype.getPdfAttachments = function (articleId) {
                var _this = this;
                return this.$http(this.routerService.action("Article", "GetPdfAttachments", { articleId: articleId })).then(function (r) {
                    if (r.data && r.data.length > 0)
                        return _this.getUrls(r.data);
                    return "";
                });
            };

            ArticleService.prototype.getUrls = function (pdfs) {
                return backOffice.Global.pdfUrl + '?name=' + pdfs[0].name.replace('.pdf', '');
            };

            ArticleService.prototype.getByObject = function (objectId, sourceFilter, emotionFilter, query, pageNumber, pageSize) {
                return this.req(this.routerService.action('Article', 'GetByObject', {
                    objectId: objectId,
                    sourceFilter: sourceFilter,
                    emotionFilter: emotionFilter,
                    query: query,
                    pageNumber: pageNumber,
                    pageSize: pageSize
                }));
            };
            ArticleService.$inject = ['$http', 'RouterService'];
            return ArticleService;
        })(services.common.BaseService);
        services.ArticleService = ArticleService;
        app.service('ArticleService', ArticleService);
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var ClusterArticleService = (function (_super) {
            __extends(ClusterArticleService, _super);
            function ClusterArticleService($http, routerService) {
                _super.call(this, $http);
                this.$http = $http;
                this.routerService = routerService;
            }
            ClusterArticleService.prototype.getList = function (clusterId, period, isVerifiedExcluded, pageSize, pageNumber) {
                return this.$http(Router.action("ClusterArticle", "GetList", {
                    clusterId: clusterId,
                    pageSize: pageSize,
                    pageNumber: pageNumber
                })).then(function (r) {
                    return r.data;
                });
            };

            ClusterArticleService.prototype.getRelevantList = function (mainArticleId) {
                return this.$http(this.routerService.action("ClusterArticle", "GetRelevantList", { mainArticleId: mainArticleId })).then(function (r) {
                    return r.data;
                });
            };

            ClusterArticleService.prototype.getCounters = function (clusterId, mainArticleId) {
                return this.$http(this.routerService.action("ClusterArticle", "GetCounters", { clusterId: clusterId, mainArticleId: mainArticleId })).then(function (r) {
                    return r.data;
                });
            };

            ClusterArticleService.prototype.getCountersForMoreArticles = function (articleId) {
                return this.$http(this.routerService.action('Article', "GetCountersForMoreArticles", { articleId: articleId })).then(function (r) {
                    return r.data;
                });
            };

            ClusterArticleService.prototype.getDeletedCount = function (clusterId) {
                return this.$http(this.routerService.action("ClusterArticle", "GetDeletedCount", { clusterId: clusterId })).then(function (r) {
                    return r.data;
                });
            };

            ClusterArticleService.prototype.deleteArticles = function (ids, clusterId) {
                return this.$http(angular.extend(this.routerService.action("ClusterArticle", "DeleteArticles", { clusterId: clusterId }), {
                    data: ids,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })).then(function (r) {
                    return r.data;
                });
            };

            ClusterArticleService.prototype.getDeletedFromCluster = function (clusterId) {
                return this.$http(this.routerService.action("ClusterArticle", "GetDeletedFromCluster", { clusterId: clusterId })).then(function (r) {
                    return r.data;
                });
            };

            ClusterArticleService.prototype.restoreToCluster = function (ids, clusterId) {
                return this.$http(angular.extend(this.routerService.action("ClusterArticle", "RestoreToCluster", { clusterId: clusterId }), { data: ids })).then(function (r) {
                    return r.data;
                });
            };

            ClusterArticleService.prototype.getFilteredList = function (clusterId, period, emotion, source, sort, pageSize, pageNumber) {
                if (typeof pageSize === "undefined") { pageSize = 20; }
                if (typeof pageNumber === "undefined") { pageNumber = 1; }
                return this.$http(Router.action('Article', 'GetFilteredList', {
                    period: period,
                    clusterId: clusterId,
                    emotion: emotion,
                    source: source,
                    sort: sort,
                    pageSize: pageSize,
                    pageNumber: pageNumber
                })).then(function (r) {
                    return r.data;
                });
            };

            ClusterArticleService.prototype.getFilteredListForArticles = function (articleId, period, emotion, source, sort, pageSize, pageNumber) {
                if (typeof pageSize === "undefined") { pageSize = 20; }
                if (typeof pageNumber === "undefined") { pageNumber = 1; }
                return this.$http(this.routerService.action('Article', 'GetFilteredListForArticles', {
                    period: period,
                    articleId: articleId,
                    emotion: emotion,
                    source: source,
                    sort: sort,
                    pageSize: pageSize,
                    pageNumber: pageNumber
                })).then(function (r) {
                    return r.data;
                });
            };

            ClusterArticleService.prototype.getFilteredListForArticlesWithFilter = function (articleId, period, filter, pageSize, pageNumber) {
                return this.getFilteredListForArticles(articleId, period, filter.emotion, filter.source, filter.sort, pageSize, pageNumber);
            };
            ClusterArticleService.$inject = ['$http', 'RouterService'];
            return ClusterArticleService;
        })(services.common.BaseService);
        services.ClusterArticleService = ClusterArticleService;
        app.service('ClusterArticleService', ClusterArticleService);
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var ClusterService = (function (_super) {
            __extends(ClusterService, _super);
            function ClusterService($http, routerService) {
                _super.call(this, $http);
                this.$http = $http;
                this.routerService = routerService;
            }
            ClusterService.prototype.getCounters = function (reportId, period) {
                return this.$http(this.routerService.action("Cluster", "GetCounters", { reportId: reportId, period: period })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.getByReport = function (reportId, period, isVerifiedExcluded, pageSize, pageNumber) {
                return this.$http(this.routerService.action("Cluster", "GetByReport", { reportId: reportId, period: period, isVerifiedExcluded: isVerifiedExcluded, pageSize: pageSize, pageNumber: pageNumber })).then(function (r) {
                    _.forEach(r.data.items, function (item) {
                        item.selected = false;
                    });
                    r.data.isLoaded = true;
                    return r.data;
                });
            };

            ClusterService.prototype.getById = function (clusterId) {
                return this.$http(this.routerService.action("Cluster", "GetById", { clusterId: clusterId })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.getDeletedFromReport = function (reportId, period, pageSize, pageNumber) {
                return this.$http(this.routerService.action("Cluster", "GetDeletedFromReport", { reportId: reportId, pageSize: pageSize, pageNumber: pageNumber, period: period })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.getDeletedFromReportCount = function (reportId, period) {
                return this.$http(this.routerService.action('Cluster', "GetDeletedFromReportCount", { reportId: reportId, period: period })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.getClusterCount = function (reportId, period, isVerifiedExcluded) {
                return this.$http(Router.action("Cluster", "GetClusterCount", { reportId: reportId, period: period, isVerifiedExcluded: isVerifiedExcluded })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.restoreToReport = function (reportId, articleIds) {
                var ids;
                if (articleIds.length === undefined)
                    ids = [articleIds];
                else
                    ids = articleIds;
                return this.$http(angular.extend(this.routerService.action("Cluster", "RestoreToReport", { reportId: reportId }), { data: ids })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.restoreEverywhere = function (articleIds) {
                return this.$http(angular.extend(this.routerService.action("Cluster", "RestoreEverywhere"), { data: articleIds })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.deleteEverywhere = function (articleIds) {
                return this.$http(angular.extend(this.routerService.action("Cluster", "DeleteEverywhere"), {
                    data: articleIds,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.deleteFromReport = function (articleIds, reportId) {
                return this.$http(angular.extend(this.routerService.action("Cluster", "DeleteFromReport", { reportId: reportId }), {
                    data: articleIds,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.setImage = function (clusterId, imageId) {
                return this.$http(angular.extend(this.routerService.action("Cluster", "SetImage", { clusterId: clusterId }), { data: { imageId: imageId } })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.getImages = function (clusterId) {
                return this.$http(this.routerService.action("Cluster", "GetImages", { clusterId: clusterId })).then(function (r) {
                    return r.data;
                });
            };

            ClusterService.prototype.setTitle = function (clusterId, title) {
                return this.$http(angular.extend(this.routerService.action("Cluster", "SetTitle", { clusterId: clusterId }), { data: { title: title } })).then(function (r) {
                    return r.data;
                }, function (error) {
                    return error;
                });
            };

            ClusterService.prototype.setMainArticle = function (clusterId, articleId) {
                return this.$http(angular.extend(this.routerService.action("ClusterArticle", "SetMainArticleId", { clusterId: clusterId }), { data: { 'articleId': articleId } })).then(function (r) {
                    return r.data;
                });
            };
            ClusterService.$inject = ['$http', 'RouterService'];
            return ClusterService;
        })(services.common.BaseService);
        services.ClusterService = ClusterService;
        app.service('ClusterService', ClusterService);
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var ConfigService = (function () {
            function ConfigService($http, routerService) {
                this.$http = $http;
                this.routerService = routerService;
            }
            ConfigService.prototype.getConfigurationsByReportId = function (reportId) {
                return this.$http(this.routerService.action("Configuration", "GetConfigurationsByReportId", { reportId: reportId })).then(function (response) {
                    return response.data;
                });
            };

            ConfigService.prototype.getAllPublishTerminalConfiguration = function () {
                return this.$http(this.routerService.action("Configuration", "GetAllPublishTerminalConfiguration")).then(function (response) {
                    return response.data;
                });
            };
            ConfigService.$inject = ['$http', 'RouterService'];
            return ConfigService;
        })();
        services.ConfigService = ConfigService;
        app.service('ConfigService', ConfigService);
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var ObjectService = (function (_super) {
            __extends(ObjectService, _super);
            function ObjectService($http, routerService) {
                _super.call(this, $http);
                this.$http = $http;
                this.routerService = routerService;
            }
            ObjectService.prototype.getByArticle = function (articleId) {
                return this.req(this.routerService.action("Object", "GetByArticle", { articleId: articleId }));
            };

            ObjectService.prototype.getObjectById = function (objectId) {
                return this.req(angular.extend(this.routerService.action("Object", "GetObjectById", { objectId: objectId }), { cache: true }));
            };

            ObjectService.prototype.getObjectAtomInfo = function (articleId, objectId) {
                return this.req(angular.extend(this.routerService.action("Object", "GetObjectAtomInfo", { objectId: objectId, articleId: articleId }), { cache: true }));
            };
            ObjectService.$inject = ['$http', 'RouterService'];
            return ObjectService;
        })(services.common.BaseService);
        services.ObjectService = ObjectService;
        app.service('ObjectService', ObjectService);
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var ReportQueueService = (function () {
            function ReportQueueService($http, routerService) {
                this.$http = $http;
                this.routerService = routerService;
                this.controller = "ReportQueue";
            }
            ReportQueueService.prototype.getQueueStatistic = function () {
                return this.$http(this.routerService.action("ReportQueue", "GetQueueStatistic")).then(function (response) {
                    return response.data;
                });
            };

            ReportQueueService.prototype.getReport = function () {
                return this.$http(this.routerService.action(this.controller, "GetReport")).then(function (success) {
                    success.data.isLoaded = true;
                    return success.data;
                });
            };

            ReportQueueService.prototype.saveReport = function () {
                return this.$http(this.routerService.action(this.controller, "SaveReport")).then(function (status) {
                    return status.data;
                }, function (error) {
                    return error.data;
                });
            };

            ReportQueueService.prototype.addConfigurationToQueue = function (config) {
                return this.$http(angular.extend(this.routerService.action(this.controller, "AddConfigurationToQueue"), { data: config })).then(function (response) {
                    return response.data;
                });
            };

            ReportQueueService.prototype.getConfigurationsOnControl = function () {
                return this.$http(this.routerService.action(this.controller, "GetConfigurationsOnControl")).then(function (response) {
                    return response.data;
                });
            };

            ReportQueueService.prototype.deleteConfigurationFromQueue = function (configId) {
                return this.$http(this.routerService.action(this.controller, "DeleteConfigurationFromQueue", { id: configId })).then(function (response) {
                    return response.data;
                });
            };

            ReportQueueService.prototype.deleteConfigurationFromQueueBatch = function (configIds) {
                return this.$http(angular.extend(Router.action(this.controller, "DeleteConfigurationFromQueueBatch"), { data: configIds })).then(function (response) {
                    return response.data;
                });
            };

            ReportQueueService.prototype.forceReportRefresh = function (reportId) {
                return this.$http(this.routerService.action(this.controller, "ForceReportRefresh", { reportId: reportId })).then(function (r) {
                    return r.data;
                });
            };
            ReportQueueService.$inject = ['$http', 'RouterService'];
            return ReportQueueService;
        })();
        services.ReportQueueService = ReportQueueService;
        app.service('ReportQueueService', ReportQueueService);
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var ReportService = (function () {
            function ReportService($http) {
                this.$http = $http;
            }
            ReportService.prototype.getReportInfo = function (reportId) {
                return this.$http(Router.action("ReportQueue", "GetReport")).then(function (response) {
                    response.data.isLoaded = true;
                    return response.data;
                });
            };
            ReportService.$inject = ["$http"];
            return ReportService;
        })();
        services.ReportService = ReportService;
        app.service('ReportService', ReportService);
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (services) {
        var RouterService = (function () {
            function RouterService() {
                this.STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
                this.ARGUMENT_NAMES = /([^\s,]+)/g;
            }
            RouterService.prototype.getParamNames = function (func) {
                var fnStr = func.toString().replace(this.STRIP_COMMENTS, '');
                var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(this.ARGUMENT_NAMES);
                if (result === null)
                    result = [];
                return result;
            };
            RouterService.prototype.toCamel = function (str) {
                var monb = str;
                if (monb.length <= 1) {
                    monb = monb.toLowerCase();
                } else {
                    monb = monb.substring(0, 1).toLowerCase() + monb.substring(1);
                }
                return monb;
            };
            RouterService.prototype.action = function (controller, action, params) {
                var obj = jsRoutes.controllers[controller];
                var fn = obj[this.toCamel(action)];
                var paramNames = this.getParamNames(fn);
                var values = [];
                for (var i = 0; i < paramNames.length; i++) {
                    values.push(params[paramNames[i]]);
                }
                return fn.apply(obj, values);
            };
            return RouterService;
        })();
        services.RouterService = RouterService;
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
app.service("RouterService", backOffice.services.RouterService);
var backOffice;
(function (backOffice) {
    (function (services) {
        (function (http) {
            var SecurityService = (function () {
                function SecurityService($http, routerService) {
                    this.$http = $http;
                    this.routerService = routerService;
                }
                SecurityService.prototype.signOff = function () {
                    return this.$http(this.routerService.action('Security', 'SignOut')).then(function (r) {
                        return r.data;
                    });
                };

                SecurityService.prototype.signIn = function (auth) {
                    if (!auth.isPermanent)
                        auth.isPermanent = false;
                    return this.$http(angular.extend(this.routerService.action('Security', 'SignIn'), { data: auth })).then(function (r) {
                        return r.data;
                    });
                };
                SecurityService.$inject = ['$http', 'RouterService'];
                return SecurityService;
            })();
            http.SecurityService = SecurityService;
            app.service('SecurityService', SecurityService);
        })(services.http || (services.http = {}));
        var http = services.http;
    })(backOffice.services || (backOffice.services = {}));
    var services = backOffice.services;
})(backOffice || (backOffice = {}));
angular.module('ui.bootstrap.popover', ['ui.bootstrap.tooltip2']).directive('popoverPopup', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: { ptitle: '@', content: '@', placement: '@', animation: '&', isOpen: '&', isContentLoaded: '=' },
        templateUrl: 'public/main/scripts/directives/template/popover/popover.html'
    };
}).directive('popover', [
    '$compile', '$timeout', '$parse', '$window', '$tooltip2', function ($compile, $timeout, $parse, $window, $tooltip) {
        return $tooltip('popover', 'popover', 'click');
    }]);
angular.module('ui.bootstrap.tooltip2', ['ui.bootstrap.position', 'ui.bootstrap.bindHtml']).provider('$tooltip2', function () {
    var defaultOptions = {
        placement: 'top',
        animation: true,
        popupDelay: 0
    };

    var triggerMap = {
        'mouseenter': 'mouseleave',
        'click': 'click',
        'focus': 'blur'
    };

    var globalOptions = {};

    this.options = function (value) {
        angular.extend(globalOptions, value);
    };

    this.setTriggers = function setTriggers(triggers) {
        angular.extend(triggerMap, triggers);
    };

    function snake_case(name) {
        var regexp = /[A-Z]/g;
        var separator = '-';
        return name.replace(regexp, function (letter, pos) {
            return (pos ? separator : '') + letter.toLowerCase();
        });
    }

    this.$get = [
        '$window', '$compile', '$timeout', '$parse', '$document', '$position', '$interpolate', '$http', '$templateCache',
        function ($window, $compile, $timeout, $parse, $document, $position, $interpolate, $http, $templateCache) {
            return function $tooltip(type, prefix, defaultTriggerShow) {
                var options = angular.extend({}, defaultOptions, globalOptions);

                function getTriggers(trigger) {
                    var show = trigger || options.trigger || defaultTriggerShow;
                    var hide = triggerMap[show] || show;
                    return {
                        show: show,
                        hide: hide
                    };
                }

                var directiveName = snake_case(type);

                var startSym = $interpolate.startSymbol();
                var endSym = $interpolate.endSymbol();
                var template = '<' + directiveName + '-popup ' + 'ptitle="' + startSym + 'tt_title' + endSym + '" ' + 'content="' + startSym + 'tt_content' + endSym + '" ' + 'placement="' + startSym + 'tt_placement' + endSym + '" ' + 'is-content-loaded="isContentLoaded" style="width:300px" ' + 'animation="tt_animation" ' + 'is-open="tt_isOpen"' + '>' + '</' + directiveName + '-popup>';
                function withTemplate(templateUrl, handler) {
                    $http({
                        url: templateUrl,
                        method: 'get',
                        cache: $templateCache
                    }).success(function (html) {
                        $timeout(function () {
                            handler(html);
                        });
                    });
                }
                return {
                    restrict: 'EA',
                    scope: {
                        service: '=',
                        popoverGetContent: "&",
                        popoverTemplate: '@'
                    },
                    link: function link(scope, element, attrs) {
                        scope.isContentLoaded = false;
                        var tooltip = $compile(template)(scope);
                        var hideTimeOut = 200;
                        var transitionTimeout;
                        var popupTimeout;
                        var $body = $document.find('body');
                        var appendToBody = angular.isDefined(options.appendToBody) ? options.appendToBody : false;
                        var triggers = getTriggers(undefined);
                        var hasRegisteredTriggers = false;
                        var hasEnableExp = angular.isDefined(attrs[prefix + 'Enable']);

                        scope.tt_isOpen = false;

                        function toggleTooltipBind() {
                            if (!scope.tt_isOpen) {
                                showTooltipBind();
                            } else {
                                hideTooltipBind();
                            }
                        }

                        function showTooltipBind() {
                            if (hasEnableExp && !scope.$eval(attrs[prefix + 'Enable'])) {
                                return;
                            }
                            if (scope[prefix + "GetContent"]) {
                                scope[prefix + "GetContent"]().then(function (content) {
                                    var templateUrl = scope[prefix + 'Template'];
                                    if (templateUrl) {
                                        withTemplate(templateUrl, function (html) {
                                            var scp = scope.$new();
                                            scp.model = content;
                                            var cnt = $compile(html)(scp);
                                            scp.$digest();
                                            scope.isContentLoaded = true;
                                            scope.tt_content = cnt.get(0).outerHTML;
                                            if (content.name) {
                                                scope.tt_title = content.name;
                                            }
                                            changePosition(tooltip);
                                        });
                                    } else {
                                        scope.isContentLoaded = true;
                                        scope.tt_content = content;
                                    }
                                });
                            }
                            if (scope.tt_popupDelay) {
                                popupTimeout = $timeout(show, scope.tt_popupDelay);
                            } else {
                                scope.$apply(show);
                            }
                        }

                        function hideTooltipBind() {
                            scope.$apply(function () {
                                tooltip.isMouseOut = true;
                                $timeout(hideIfMouseOut, hideTimeOut);
                            });
                        }

                        function hideIfMouseOut() {
                            if (tooltip.hasOwnProperty('isMouseOut')) {
                                if (tooltip.isMouseOut) {
                                    hide();
                                }
                            }
                        }

                        function calculatePosition(tooltip) {
                            var ttPosition;
                            var position = appendToBody ? $position.offset(element) : $position.position(element);

                            var ttWidth = tooltip.prop('offsetWidth');
                            var ttHeight = tooltip.prop('offsetHeight');
                            switch (scope.tt_placement) {
                                case 'right':
                                    ttPosition = {
                                        top: position.top + position.height / 2 - ttHeight / 2,
                                        left: position.left + position.width
                                    };
                                    break;
                                case 'bottom':
                                    ttPosition = {
                                        top: position.top + position.height,
                                        left: position.left + position.width / 2 - ttWidth / 2
                                    };
                                    break;
                                case 'left':
                                    ttPosition = {
                                        top: position.top + position.height / 2 - ttHeight / 2,
                                        left: position.left - ttWidth
                                    };
                                    break;
                                default:
                                    ttPosition = {
                                        top: position.top - ttHeight,
                                        left: position.left + position.width / 2 - ttWidth / 2
                                    };
                                    break;
                            }
                            return ttPosition;
                        }

                        function changePosition(tooltip) {
                            if (tooltip) {
                                var ttPosition = calculatePosition(tooltip);
                                ttPosition.top += 'px';
                                ttPosition.left += 'px';
                                tooltip.css(ttPosition);
                            }
                        }

                        function show() {
                            if (transitionTimeout) {
                                $timeout.cancel(transitionTimeout);
                            }

                            tooltip.css({ top: 0, left: 0, display: 'block', opacity: 0.9 });

                            if (appendToBody) {
                                $body.append(tooltip);
                            } else {
                                element.after(tooltip);
                            }
                            tooltip.bind('mouseenter', function () {
                                tooltip.isMouseOut = false;
                            });

                            tooltip.bind('mouseleave', function () {
                                tooltip.isMouseOut = true;
                                $timeout(hideIfMouseOut, hideTimeOut);
                            });
                            changePosition(tooltip);

                            scope.tt_isOpen = true;
                        }

                        function hide() {
                            tooltip.unbind('mouseleave');
                            tooltip.unbind('mouseenter');

                            scope.tt_isOpen = false;

                            $timeout.cancel(popupTimeout);

                            if (scope.tt_animation) {
                                transitionTimeout = $timeout(function () {
                                    if (tooltip)
                                        tooltip.remove();
                                }, 500);
                            } else {
                                if (tooltip)
                                    tooltip.remove();
                            }
                        }

                        attrs.$observe(type, function (val) {
                            if (val) {
                                scope.tt_content = val;
                            } else {
                                if (scope.tt_isOpen) {
                                    hide();
                                }
                            }
                        });

                        attrs.$observe(prefix + 'Title', function (val) {
                            scope.tt_title = val;
                        });

                        attrs.$observe(prefix + 'Placement', function (val) {
                            scope.tt_placement = angular.isDefined(val) ? val : options.placement;
                        });

                        attrs.$observe(prefix + 'Animation', function (val) {
                            scope.tt_animation = angular.isDefined(val) ? !!val : options.animation;
                        });

                        attrs.$observe(prefix + 'PopupDelay', function (val) {
                            var delay = parseInt(val, 10);
                            scope.tt_popupDelay = !isNaN(delay) ? delay : options.popupDelay;
                        });

                        attrs.$observe(prefix + 'Trigger', function (val) {
                            if (hasRegisteredTriggers) {
                                element.unbind(triggers.show, showTooltipBind);
                                element.unbind(triggers.hide, hideTooltipBind);
                            }

                            triggers = getTriggers(val);

                            if (triggers.show === triggers.hide) {
                                element.bind(triggers.show, toggleTooltipBind);
                            } else {
                                element.bind(triggers.show, showTooltipBind);
                                element.bind(triggers.hide, hideTooltipBind);
                            }

                            hasRegisteredTriggers = true;
                        });

                        attrs.$observe(prefix + 'AppendToBody', function (val) {
                            appendToBody = angular.isDefined(val) ? $parse(val)(scope) : appendToBody;
                        });

                        if (appendToBody) {
                            scope.$on('$locationChangeSuccess', function closeTooltipOnLocationChangeSuccess() {
                                if (scope.tt_isOpen) {
                                    hide();
                                }
                            });
                        }

                        scope.$on('$destroy', function onDestroyTooltip() {
                            $timeout.cancel(popupTimeout);
                            tooltip.remove();
                            tooltip.unbind();
                            tooltip = null;
                            $body = null;
                        });
                    }
                };
            };
        }];
});
var backOffice;
(function (backOffice) {
    (function (directives) {
        function errorAlert($rootScope, $templateCache, $compile, $http) {
            return {
                restrict: "EA",
                scope: true,
                link: function ($scope, element) {
                    var templateUrl = "public/main/scripts/directives/template/errorAlert.html";
                    $rootScope.$watch("error", function (error) {
                        if (error) {
                            $scope.error = error.exceptionMessage || error.message;
                            $scope.other = error;
                            $rootScope["error"] = undefined;
                            $http.get(templateUrl, { cache: $templateCache }).success(function (tplContent) {
                                element.replaceWith($compile(tplContent.trim())($scope));
                            });
                        }
                    });
                }
            };
        }
        directives.errorAlert = errorAlert;
        errorAlert.$inject = ["$rootScope", "$templateCache", "$compile", "$http"];
    })(backOffice.directives || (backOffice.directives = {}));
    var directives = backOffice.directives;
})(backOffice || (backOffice = {}));
'use strict';
var backOffice;
(function (backOffice) {
    (function (directives) {
        function fileUpload($http) {
            return {
                restrict: 'A',
                scope: {
                    progress: '=',
                    onSuccess: '=',
                    onError: '=',
                    onComplete: '=',
                    canceled: '=',
                    fileUpload: '=',
                    onChange: '=',
                    service: '='
                },
                link: function (scope, elem, attr) {
                    elem.bind('change', function () {
                        var file = this.files[0];
                        if (file === undefined)
                            return;
                        scope.$apply(function () {
                            if (scope.onChange)
                                scope.onChange.call(scope.service);
                            if (scope.progress)
                                scope.progress.call(scope.service, 0);
                        });
                        uploadFile(file);
                    });

                    function transformResponse(response) {
                        angular.forEach($http.defaults.transformResponse, function (transformFn) {
                            response = transformFn(response);
                        });
                        return response;
                    }

                    function uploadFile(file) {
                        var fd = new FormData();
                        fd.append('file', file);
                        var xhr = new XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (event) {
                            scope.$apply(function () {
                                if (scope.progress)
                                    scope.progress.call(scope.service, event.lengthComputable ? event.loaded * 100 / event.total : 0);
                            });
                        }, false);
                        xhr.addEventListener("load", function () {
                            scope.$apply(function () {
                                var response = transformResponse(xhr.response);
                                if (scope.onSuccess)
                                    xhr.status === 200 && scope.onSuccess.call(scope.service, response);
                                if (scope.onError)
                                    xhr.status !== 200 && scope.onError.call(scope.service, response);
                                if (scope.onComplete)
                                    scope.onComplete.call(scope.service, response);
                            });
                        }, false);
                        xhr.addEventListener("error", scope.failed, false);
                        xhr.addEventListener("abort", scope.canceled, false);
                        xhr.open("POST", scope.fileUpload);
                        scope.progressVisible = true;
                        xhr.send(fd);
                    }
                }
            };
        }
        directives.fileUpload = fileUpload;

        fileUpload.$inject = ['$http'];
    })(backOffice.directives || (backOffice.directives = {}));
    var directives = backOffice.directives;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (directives) {
        function highlightControl() {
            return {
                scope: {
                    object: "="
                },
                link: function ($scope, element, attributes) {
                    var object = $scope["object"];
                }
            };
        }
        directives.highlightControl = highlightControl;
    })(backOffice.directives || (backOffice.directives = {}));
    var directives = backOffice.directives;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (directives) {
        highlightText.$inject = ['$compile'];
        function highlightText($compile) {
            return {
                scope: {
                    highlightText: "=",
                    hightlightObjects: "="
                },
                link: function ($scope, element, attributes) {
                    element.addClass('ng-binding').data('$binding', attributes.highlightText);
                    $scope.$watch("highlightText", function (value) {
                        var fixedText = "<p>" + value + "</p>";
                        var compiledContent = $compile(fixedText)($scope);
                        element.html(compiledContent || '');
                    });
                }
            };
        }
        directives.highlightText = highlightText;

        objectid.$inject = ['ColorService', '$compile'];
        function objectid(colorService, $compile) {
            return {
                link: function ($scope, element, attributes) {
                    var activeClass = "active";

                    var textColor = element.css("color");
                    var textDecoration = "underline";
                    var hc = element.find(".highlight-control");
                    var click = function () {
                        var modalId = element.parents('.modal-body').attr('data-modal-id');
                        var querySelector = "[objectId='" + attributes["objectid"] + "']";
                        querySelector = "[data-modal-id=" + modalId + "] " + querySelector;
                        if (!element.hasClass(activeClass)) {
                            var rgb = colorService.toRGB(attributes["objectid"] * 100);
                            var bgColor = colorService.toColor(rgb);

                            $(querySelector).addClass(activeClass).css({ backgroundColor: bgColor, color: colorService.toColor(colorService.fontColor(rgb)) });
                        } else {
                            $(querySelector).removeClass(activeClass).css({ backgroundColor: "transparent", color: textColor });
                        }
                    };

                    if (hc.length == 0) {
                        element.bind("click", click);
                        var text = element.text();
                        var id = attributes['objectid'];
                        var el = $('<span popover="" popover-title="Загрузка данных объекта  ..." popover-template="public/main/partials/object/info2.html" popover-trigger="mouseenter" popover-placement="right" popover-get-content="vm.getObjectInfluence(' + id + ')"></span>').text(text);
                        var content = $compile(el)($scope.$parent);

                        element.text('').append(content);
                    } else {
                        hc.bind("click", click);
                    }
                }
            };
        }
        directives.objectid = objectid;
    })(backOffice.directives || (backOffice.directives = {}));
    var directives = backOffice.directives;
})(backOffice || (backOffice = {}));
'use strict';
var backOffice;
(function (backOffice) {
    (function (directives) {
        function infoBar($templateCache, $compile, $http, $state) {
            return {
                restrict: 'A',
                scope: {
                    infoBar: '@',
                    verifiedCount: '=',
                    itemsOnPage: '=',
                    totalCount: '=',
                    deletedCount: '=',
                    reportId: '=',
                    trashUrl: '=',
                    nextVerifiedState: '='
                },
                link: function (scope, elem, attr) {
                    scope.url = $state.href;
                    scope.currentState = $state.current.name;
                    $http.get(scope.infoBar, { cache: $templateCache }).success(function (tplContent) {
                        elem.replaceWith($compile(tplContent.trim())(scope));
                    });
                }
            };
        }
        directives.infoBar = infoBar;
        ;

        infoBar.$inject = ['$templateCache', '$compile', '$http', '$state'];
    })(backOffice.directives || (backOffice.directives = {}));
    var directives = backOffice.directives;
})(backOffice || (backOffice = {}));
'use strict';
var backOffice;
(function (backOffice) {
    (function (directives) {
        itemInfoBar.$inject = ['$templateCache', '$compile', '$http', '$state'];
        function itemInfoBar($templateCache, $compile, $http, $state) {
            return {
                restrict: 'A',
                scope: {
                    itemInfoBar: '=',
                    selectAction: '=',
                    service: '='
                },
                link: function (scope, elem, attr) {
                    scope.url = $state.href;
                    scope.params = $state.params;
                    scope.message = scope.itemInfoBar;
                    scope.$watch("itemInfoBar", function (newValue) {
                        if (newValue) {
                            scope.message = newValue;
                            var relativeUrl = 'public/main/scripts/directives/template/itemInfoBar/';
                            var templateUrl = relativeUrl + (scope.message.hasOwnProperty('articlesCount') ? 'cluster-info-bar.html' : 'article-info-bar.html');
                            $http.get(templateUrl, { cache: $templateCache }).success(function (tplContent) {
                                elem.replaceWith($compile(tplContent.trim())(scope));
                            });
                        }
                    });
                    scope.onChange = function () {
                        scope.selectAction.call(scope.service, undefined);
                    };
                }
            };
        }
        directives.itemInfoBar = itemInfoBar;
    })(backOffice.directives || (backOffice.directives = {}));
    var directives = backOffice.directives;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (directives) {
        lazyImage.$inject = ['$window', 'ImageHelper'];
        function lazyImage($window, imageService) {
            return {
                restrict: 'A',
                scope: {
                    lazyImageSrc: '@',
                    id: '@',
                    containerId: '@'
                },
                link: function (scope, elem, attr) {
                    var width = attr.lazyWidth ? attr.lazyWidth : 100;
                    var height = attr.lazyHeight ? attr.lazyHeight : 100;
                    var resizeType = attr.lazyType ? attr.lazyType : 6;
                    attr.$observe('lazyImageSrc', function (val) {
                        if (imageService.isGuid(val)) {
                            var imgSrc = imageService.getImageUrl(val, width, height, resizeType);
                            if (elem.attr("src") !== imgSrc) {
                                elem.attr("src", imgSrc);
                            }
                            var performLoadAnimation = function () {
                                elem.removeClass("img-default");
                                elem.addClass("img-loaded");
                            };
                            elem.on('load', function () {
                                if (elem.attr("src") === imgSrc) {
                                    performLoadAnimation();
                                }
                            });
                        }
                    });
                }
            };
        }
        directives.lazyImage = lazyImage;
    })(backOffice.directives || (backOffice.directives = {}));
    var directives = backOffice.directives;
})(backOffice || (backOffice = {}));
'use strict';
var backOffice;
(function (backOffice) {
    (function (directives) {
        function listToolbar($window, $location) {
            return {
                restrict: 'A',
                scope: {
                    items: '='
                },
                link: function (scope, elem, attr) {
                    scope.currentUrl = $location.path();
                },
                templateUrl: 'public/main/partials/otk/shared/listToolbar.html'
            };
        }
        directives.listToolbar = listToolbar;
    })(backOffice.directives || (backOffice.directives = {}));
    var directives = backOffice.directives;
})(backOffice || (backOffice = {}));
'use strict';
var backOffice;
(function (backOffice) {
    (function (directives) {
        bsModal.$inject = ['$window', '$http', '$compile', '$location'];
        function bsModal($window, $http, $compile, $location) {
            return {
                restrict: 'AE',
                scope: {
                    templateUrl: '@',
                    title: '@',
                    alert: '@',
                    actionButtonText: '@',
                    secondaryActionButtonText: '@',
                    serviceObject: '=',
                    itemsToShow: '=',
                    action: '=',
                    secondaryAction: '=',
                    noItems: '='
                },
                link: function (scope, elem, attr) {
                    var modal = null;
                    scope.currentUrl = $location.path();
                    scope.settings = {
                        show: false
                    };
                    elem.bind("click", function () {
                        scope.safeApply(function () {
                            openModal();
                        });
                    });
                    scope.safeApply = function (fn) {
                        (scope.$$phase || scope.$root.$$phase) ? fn() : scope.$apply(fn);
                    };
                    scope.$watch('settings.show', function (isOpen) {
                        if (isOpen) {
                            $http.get(attr.templateUrl).then(function (response) {
                                var template = angular.element(response.data);
                                var linkFn = $compile(template);
                                linkFn(scope);
                                modal = $(template).modal('show');
                                modal.on('hidden.bs.modal', function () {
                                    scope.cleanModal();
                                });
                            });
                        } else {
                            if (modal != null) {
                                scope.closeModal();
                            }
                        }
                    });
                    scope.action = (function (initialAction) {
                        return function () {
                            initialAction.call(scope.serviceObject, scope.settings);
                        };
                    })(scope.action);
                    if (scope.secondaryAction) {
                        scope.secondaryAction = (function (initialAction) {
                            return function () {
                                initialAction.call(scope.serviceObject, scope.settings);
                            };
                        })(scope.secondaryAction);
                    }

                    function openModal() {
                        var items = scope.itemsToShow.call(scope.serviceObject);
                        if (items.length > 0 || scope.noItems) {
                            scope.items = items;
                            scope.settings.show = true;
                        }
                    }
                    ;

                    scope.cleanModal = function () {
                        scope.safeApply(function () {
                            $(".modal-backdrop").remove();
                            scope.settings.show = false;
                        });
                    };
                    scope.closeModal = function () {
                        modal.hide();
                        scope.cleanModal();
                    };
                }
            };
        }
        directives.bsModal = bsModal;
    })(backOffice.directives || (backOffice.directives = {}));
    var directives = backOffice.directives;
})(backOffice || (backOffice = {}));
app.directive('boObjectRole', function () {
    return {
        compile: function (e, a) {
            var roles = {
                '0': 'Все',
                '1': 'Главная',
                '2': 'Второстепенная',
                '3': 'Эпизодическая'
            };
            return function (scope, element, attrs) {
                element.html(roles[attrs.boObjectRole]);
            };
        }
    };
});
app.directive('boObjectQuotationType', function () {
    return {
        compile: function (e, a) {
            var qTypes = {
                '2': 'Нет',
                '1': 'Прямая речь',
                '0': 'Все'
            };
            return function (s, element, attrs) {
                element.html(qTypes[attrs.boObjectQuotationType]);
            };
        }
    };
});
app.directive('boObjectEmotion', function () {
    return {
        compile: function (e, a) {
            var data = {
                '0': 'Все',
                '1': 'Позитив',
                '2': 'Негатив',
                '3': 'Риск',
                '4': 'Нейтрал'
            };
            return function (s, element, attrs) {
                element.html(data[attrs.boObjectEmotion]);
            };
        }
    };
});
'use strict';
var backOffice;
(function (backOffice) {
    (function (directives) {
        function stickyBar($window) {
            return {
                restrict: 'A',
                link: function (scope, elem, attr) {
                    var wnd = angular.element($window);
                    wnd.bind("scroll", function (e) {
                        var elementOffset = elem.next().offset().top - elem.get(0).offsetHeight;
                        var stickAtPoint = parseInt(attr.stickyBar);
                        var stickyScrollOffset = elementOffset - stickAtPoint;

                        if (stickyScrollOffset > 0) {
                            if (window.pageYOffset > stickyScrollOffset && !elem.hasClass('navbar-fixed-top')) {
                                elem.next().css({ marginTop: elem.height() });
                                elem.addClass('navbar-fixed-top').css({ top: stickAtPoint });
                                elem.find('.navbar-inner').addClass('container');
                            } else if (window.pageYOffset < stickyScrollOffset && elem.hasClass('navbar-fixed-top')) {
                                elem.next().css({ marginTop: 0 });
                                elem.find('.navbar-inner').removeClass('container');
                                elem.removeClass('navbar-fixed-top').css({ top: 0 });
                            }
                        }
                    });
                }
            };
        }
        directives.stickyBar = stickyBar;

        stickyBar.$inject = ['$window'];
    })(backOffice.directives || (backOffice.directives = {}));
    var directives = backOffice.directives;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (_object) {
            var ModalObjectInfoController = (function (_super) {
                __extends(ModalObjectInfoController, _super);
                function ModalObjectInfoController($scope, $modalInstance, params, objectService, articleService, $modal) {
                    var _this = this;
                    _super.call(this, $scope, $modalInstance);
                    this.$modalInstance = $modalInstance;
                    this.params = params;
                    this.objectService = objectService;
                    this.articleService = articleService;
                    this.$modal = $modal;
                    this.isObjectLoading = false;
                    this.isItemsLoaded = true;
                    this.listMoreFilter = {
                        emotion: 0,
                        source: 0,
                        query: ''
                    };
                    this.ls = new backOffice.services.ListService(function () {
                        return _this.itemList;
                    });
                    this.object = params.object;
                    this.getObjectInfo();
                }
                ModalObjectInfoController.prototype.isNegative = function () {
                    return this.listMoreFilter.emotion === 2;
                };

                ModalObjectInfoController.prototype.changeEmotion = function (e) {
                    e.preventDefault();
                    this.listMoreFilter.emotion = this.listMoreFilter.emotion === 2 ? 0 : 2;
                    this.listMore();
                };

                ModalObjectInfoController.prototype.openArticle = function (articleId, e) {
                    var _this = this;
                    e.preventDefault();
                    var modal = this.$modal.open({
                        templateUrl: 'public/main/partials/article/display-modal.html',
                        controller: controllers.article.ModalArticleDisplayController,
                        size: 'lg',
                        resolve: {
                            params: function () {
                                return {
                                    articleId: articleId
                                };
                            }
                        }
                    });
                    modal.result.then(function (message) {
                        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
                            _this.ls.deleteItemsFromList([articleId]);
                        }
                    }, function () {
                    });
                };

                ModalObjectInfoController.prototype.changeSource = function (source, e) {
                    e.preventDefault();
                    this.listMoreFilter.source = source;
                    this.listMore();
                };

                ModalObjectInfoController.prototype.isSource = function (source) {
                    return source === this.listMoreFilter.source;
                };

                ModalObjectInfoController.prototype.listMore = function () {
                    var _this = this;
                    this.isItemsLoaded = false;
                    this.articleService.getByObject(this.object.id, this.listMoreFilter.source, this.listMoreFilter.emotion, this.listMoreFilter.query, 1, 10).then(function (itemList) {
                        _this.isItemsLoaded = true;
                        _this.itemList = itemList;
                    });
                };

                ModalObjectInfoController.prototype.getObjectInfo = function () {
                    var _this = this;
                    this.isObjectLoading = true;
                    this.objectService.getObjectAtomInfo(this.params.articleId, this.object.id).then(function (object) {
                        _this.isObjectLoading = false;
                        _this.object = object;
                        return object;
                    });
                };
                ModalObjectInfoController.$inject = ['$scope', '$modalInstance', 'params', 'ObjectService', 'ArticleService', '$modal'];
                return ModalObjectInfoController;
            })(controllers.common.ModalController);
            _object.ModalObjectInfoController = ModalObjectInfoController;
        })(controllers.object || (controllers.object = {}));
        var object = controllers.object;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (_article) {
            var ModalArticleDisplayController = (function (_super) {
                __extends(ModalArticleDisplayController, _super);
                function ModalArticleDisplayController($scope, $modalInstance, params, reportQueueService, articleService, objectService, clusterArticleService, $modal, routerService) {
                    var _this = this;
                    _super.call(this, $scope, $modalInstance, params);
                    this.$modalInstance = $modalInstance;
                    this.params = params;
                    this.reportQueueService = reportQueueService;
                    this.articleService = articleService;
                    this.objectService = objectService;
                    this.clusterArticleService = clusterArticleService;
                    this.$modal = $modal;
                    this.routerService = routerService;
                    this.isItemsLoaded = true;
                    this.isDeleting = false;
                    this.counters = {};
                    this.itemList = {
                        totalCount: 0,
                        items: [],
                        isLoaded: false
                    };
                    this.deletedCount = 0;
                    this.verifiedHidden = false;
                    this.pageSize = 100;
                    this.imageSize = { width: 200, height: 200 };
                    this.selectedCount = 0;
                    this.progress = 0;
                    this.hasNewImage = false;
                    this.iconClass = 'icon-square-o';
                    this.isUploading = false;
                    this.isSettingImage = false;
                    this.listMoreFilter = {
                        emotion: 0,
                        source: 0,
                        sort: 0
                    };
                    this.modalId = new backOffice.helpers.RandomHelper().getCharacters(8);
                    this.article = params.article;
                    this.getById(params.articleId);
                    this.listMore();

                    this.jsAction = routerService.action("Image", "UploadImage").url;
                    this.ls = new backOffice.services.ListService(function () {
                        return _this.itemList;
                    });
                    $scope.$watch("vm.imageData", function (newVal) {
                        if (newVal)
                            _this.article.imageId = newVal.id;
                    });
                }
                ModalArticleDisplayController.prototype.refresh = function (e) {
                    e.preventDefault();
                    this.listMore();
                };

                ModalArticleDisplayController.prototype.setImage = function () {
                    var _this = this;
                    this.isSettingImage = true;
                    this.articleService.setImage(this.params['articleId'], this.imageData.id).then(function () {
                        _this.progress = 0;
                        _this.isSettingImage = false;
                        _this.hasNewImage = false;
                    });
                };

                ModalArticleDisplayController.prototype.setImageButtonText = function () {
                    return this.isSettingImage ? "Сохраняется" : "Сохранить изменения";
                };

                ModalArticleDisplayController.prototype.onFileChange = function () {
                    this.isUploading = true;
                };

                ModalArticleDisplayController.prototype.uploadSuccess = function (response) {
                    this.hasNewImage = true;
                    this.imageData = response;
                    this.isUploading = false;
                };

                ModalArticleDisplayController.prototype.changePictureText = function () {
                    return this.isUploading ? 'Идет загрузка ...' : 'Сменить картинку';
                };

                ModalArticleDisplayController.prototype.isNegative = function () {
                    return this.listMoreFilter.emotion === 2;
                };

                ModalArticleDisplayController.prototype.changeEmotion = function (e) {
                    e.preventDefault();
                    this.listMoreFilter.emotion = this.listMoreFilter.emotion === 2 ? 0 : 2;
                    this.listMore();
                };

                ModalArticleDisplayController.prototype.changeSource = function (source, e) {
                    e.preventDefault();
                    this.listMoreFilter.source = source;
                    this.listMore();
                };

                ModalArticleDisplayController.prototype.isSource = function (source) {
                    return source === this.listMoreFilter.source;
                };

                ModalArticleDisplayController.prototype.getById = function (articleId) {
                    var _this = this;
                    var reportId = this.params.report.reportId;
                    this.isArticleLoaded = false;
                    this.articleService.getById(articleId, reportId, this.imageSize.width, this.imageSize.height).then(function (article) {
                        _this.isArticleLoaded = true;
                        _this.article = article;
                        return article;
                    }).then(function (article) {
                        if (!article.url) {
                            _this.articleService.getPdfAttachments(article.id).then(function (url) {
                                _this.article.url = url;
                            });
                        }
                    });
                    this.objectService.getByArticle(articleId).then(function (objects) {
                        _this.objects = objects;
                    });
                };

                ModalArticleDisplayController.prototype.getObjectInfluence = function (objectId) {
                    return this.objectService.getObjectAtomInfo(this.params.articleId, objectId).then(function (atomInfo) {
                        return atomInfo;
                    });
                };

                ModalArticleDisplayController.prototype.getObjectInfo = function (objectId) {
                    return this.objectService.getObjectById(objectId).then(function (objectInfo) {
                        return objectInfo.dossier;
                    });
                };

                ModalArticleDisplayController.prototype.isSortedBy = function (sort) {
                    return this.listMoreFilter.sort === sort;
                };

                ModalArticleDisplayController.prototype.changeSort = function (sort, e) {
                    e.preventDefault();
                    this.listMoreFilter.sort = sort;
                    this.listMore();
                };

                ModalArticleDisplayController.prototype.openObjectInfo = function (object, e) {
                    var _this = this;
                    e.preventDefault();
                    this.$modal.open({
                        templateUrl: 'public/main/partials/object/display-modal.html',
                        controller: backOffice.controllers.object.ModalObjectInfoController,
                        size: 'sm',
                        resolve: {
                            params: function () {
                                return {
                                    object: object,
                                    articleId: _this.article.id
                                };
                            }
                        }
                    });
                };

                ModalArticleDisplayController.prototype.openArticle = function (articleId, article, e) {
                    var _this = this;
                    e.preventDefault();
                    var modal = this.$modal.open({
                        templateUrl: 'public/main/partials/article/display-modal.html',
                        controller: controllers.article.ModalArticleDisplayController,
                        size: 'lg',
                        resolve: {
                            params: function () {
                                return {
                                    articleId: articleId,
                                    article: article,
                                    report: _this.params.report
                                };
                            }
                        }
                    });
                    modal.result.then(function (message) {
                        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
                            _this.ls.deleteItemsFromList([articleId]);
                        }
                    }, function () {
                    });
                };

                ModalArticleDisplayController.prototype.listMore = function () {
                    var _this = this;
                    this.isItemsLoaded = false;
                    this.report = this.params.report;
                    var period = this.report.period;
                    this.clusterArticleService.getFilteredListForArticlesWithFilter(this.params['articleId'], period, this.listMoreFilter, this.pageSize, 1).then(function (itemList) {
                        _this.isItemsLoaded = true;
                        _this.itemList = itemList;
                        _this.clusterArticleService.getCountersForMoreArticles(_this.params['articleId']).then(function (counters) {
                            _this.counters = counters;
                        });
                    });
                };
                ModalArticleDisplayController.$inject = [
                    '$scope', '$modalInstance', 'params', 'ReportQueueService', 'ArticleService',
                    'ObjectService', 'ClusterArticleService', '$modal', 'RouterService'];
                return ModalArticleDisplayController;
            })(controllers.DeleteRestoreController);
            _article.ModalArticleDisplayController = ModalArticleDisplayController;
        })(controllers.article || (controllers.article = {}));
        var article = controllers.article;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (_article) {
            var ArticleListController = (function (_super) {
                __extends(ArticleListController, _super);
                function ArticleListController($modal, $scope, articleService, reportQueueService, $state, configService, nav, confirmation) {
                    _super.call(this, $scope, configService, reportQueueService, $state);
                    this.$modal = $modal;
                    this.$scope = $scope;
                    this.articleService = articleService;
                    this.reportQueueService = reportQueueService;
                    this.$state = $state;
                    this.configService = configService;
                    this.nav = nav;
                    this.confirmation = confirmation;
                    this.isDeleting = false;
                    this.counters = { totalCount: 0, verifiedCount: 0 };
                    this.getArticleList();
                }
                ArticleListController.prototype.isClustersDisabled = function () {
                    if (!this.report || !this.report.type)
                        return false;
                    return this.report.type === 1;
                };

                ArticleListController.prototype.isArticlesDisabled = function () {
                    if (!this.report || !this.report.type)
                        return false;
                    return this.report.type === 2;
                };

                ArticleListController.prototype.deleteConfirmation = function (e) {
                    var _this = this;
                    e.preventDefault();
                    if (this.ls.selectedCount > 0) {
                        this.$modal.open({
                            templateUrl: 'public/main/partials/modals/delete-restore.html',
                            controller: controllers.DeleteRestoreController,
                            resolve: {
                                params: function () {
                                    return {
                                        title: 'Удалить',
                                        items: _this.ls.getSelectedItems(),
                                        _action: function () {
                                            return _this.deleteFromReport(_this.ls.getSelectedIds());
                                        },
                                        _secondaryAction: function () {
                                            return _this.deleteEverywhere(_this.ls.getSelectedIds());
                                        },
                                        actionButtonText: 'Удалить',
                                        secondaryActionButtonText: 'Удалить везде',
                                        alert: 'Вы действительно хотите удалить эти статьи?'
                                    };
                                }
                            }
                        });
                    }
                };

                ArticleListController.prototype.openTrash = function (e) {
                    var _this = this;
                    e.preventDefault();
                    var modal = this.$modal.open({
                        templateUrl: 'public/main/partials/article/deleted-list-modal.html',
                        controller: controllers.article.DeletedArticleListController,
                        size: 'lg',
                        resolve: {
                            params: function () {
                                return {
                                    reportId: _this.reportId,
                                    report: _this.report
                                };
                            }
                        }
                    });
                };

                ArticleListController.prototype.deleteSingleFromReport = function (articleId) {
                    var _this = this;
                    return this.confirmation('Вы действительно хотите удалить эту статью из отчета?', 'Удалить', function () {
                        return _this.deleteFromReport([articleId]);
                    });
                };

                ArticleListController.prototype.deleteSingleEverywhere = function (articleId) {
                    var _this = this;
                    return this.confirmation('Вы действительно хотите удалить эту статью везде?', 'Удалить', function () {
                        return _this.deleteEverywhere([articleId]);
                    });
                };

                ArticleListController.prototype.openArticle = function (articleId, article, e) {
                    var _this = this;
                    e.preventDefault();
                    var modal = this.$modal.open({
                        templateUrl: 'public/main/partials/article/display-modal.html',
                        controller: controllers.article.ModalArticleDisplayController,
                        size: 'lg',
                        resolve: {
                            params: function () {
                                return {
                                    articleId: articleId,
                                    article: article,
                                    report: _this.report,
                                    _action: function () {
                                        return _this.deleteSingleFromReport(articleId);
                                    },
                                    _secondaryAction: function () {
                                        return _this.deleteSingleEverywhere(articleId);
                                    },
                                    actionButtonText: 'Удалить в отчете',
                                    secondaryActionButtonText: 'Удалить везде'
                                };
                            }
                        }
                    });
                    modal.result.then(function (message) {
                        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
                            _this.ls.deleteItemsFromList([articleId]);
                        }
                    }, function () {
                    });
                };

                ArticleListController.prototype.getArticleList = function () {
                    var _this = this;
                    this.reportQueueService.getReport().then(function (report) {
                        _this.isItemsLoaded = false;
                        _this.report = report;
                        _this.reportId = report.reportId;
                        _this.period = _this.report.period;
                        return report;
                    }).then(function (report) {
                        _this.loadConfigurations(report.reportId);
                        return _this.articleService.getByReport(report.reportId, report.period, _this.verifiedHidden, _this.pageSize, 1).then(function (itemList) {
                            _this.isItemsLoaded = true;
                            _this.itemList = itemList;
                            _this.ls.updateSelectedCount();
                            return itemList;
                        });
                    }).then(function (itemList) {
                        return _this.articleService.getCounters(_this.reportId, _this.period).then(function (counters) {
                            _this.counters = counters;
                            return counters;
                        });
                    }).then(function (counters) {
                        _this.articleService.getDeletedFromReportCount(_this.reportId, _this.period).then(function (deletedCount) {
                            _this.ls.deletedCount = +deletedCount;
                            return _this.ls.deletedCount;
                        });
                    });
                };

                ArticleListController.prototype.deleteEverywhere = function (selectedIds) {
                    var _this = this;
                    return this.articleService.deleteEverywhere(selectedIds).then(function () {
                        _this.ls.deleteSelectedItems();
                    });
                };

                ArticleListController.prototype.deleteFromReport = function (selectedIds) {
                    var _this = this;
                    return this.articleService.deleteFromReport(selectedIds, this.reportId).then(function () {
                        _this.ls.deleteSelectedItems();
                    });
                };
                ArticleListController.prototype.refresh = function (e) {
                    e.preventDefault();
                    if (this.isItemsLoaded) {
                        this.getArticleList();
                    }
                };

                ArticleListController.prototype.loadMore = function () {
                    var _this = this;
                    this.isItemsLoaded = false;
                    this.articleService.getByReport(this.reportId, this.report.period, this.verifiedHidden, this.pageSize, ++this.currentPage).then(function (itemList) {
                        _this.itemList.items.push.apply(_this.itemList.items, itemList.items);
                        _this.isItemsLoaded = true;
                    });
                };

                ArticleListController.prototype.hasMore = function () {
                    return this.itemList.totalCount > this.itemList.items.length;
                };

                ArticleListController.prototype.showGotoClustersButton = function () {
                    return this.report && (this.report.type === 3);
                };
                ArticleListController.$inject = [
                    '$modal', '$scope', 'ArticleService',
                    'ReportQueueService', '$state', 'ConfigService', 'NavService', 'Confirmation'];
                return ArticleListController;
            })(controllers.common.ListController);
            _article.ArticleListController = ArticleListController;
        })(controllers.article || (controllers.article = {}));
        var article = controllers.article;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (_cluster) {
            var ModalDeletedClusterListController = (function (_super) {
                __extends(ModalDeletedClusterListController, _super);
                function ModalDeletedClusterListController($scope, params, clusterService, $modal, $modalInstance, confirmation) {
                    var _this = this;
                    _super.call(this, $scope, $modalInstance);
                    this.params = params;
                    this.clusterService = clusterService;
                    this.$modal = $modal;
                    this.$modalInstance = $modalInstance;
                    this.confirmation = confirmation;
                    this.itemList = {
                        totalCount: 0,
                        items: [],
                        isLoaded: false
                    };
                    $scope.vm = this;
                    this.ls = new backOffice.services.ListService(function () {
                        return _this.itemList;
                    });
                    this.getClustersTrash();
                }
                ModalDeletedClusterListController.prototype.getClustersTrash = function () {
                    var _this = this;
                    return this.clusterService.getDeletedFromReport(this.params.report.reportId, this.params.report.period, 100, 1).then(function (itemList) {
                        itemList.isLoaded = true;
                        _this.itemList = itemList;
                    });
                };

                ModalDeletedClusterListController.prototype.openArticle = function (clusterId, cluster, e) {
                    var _this = this;
                    e.preventDefault();
                    this.$modal.open({
                        templateUrl: 'public/main/partials/cluster/display-modal.html',
                        controller: backOffice.controllers.cluster.ModalClusterDisplayController,
                        size: 'lg',
                        resolve: {
                            params: function () {
                                return {
                                    clusterId: clusterId,
                                    report: _this.params.report,
                                    cluster: cluster,
                                    _action: function () {
                                        return _this.restoreSingleToReport(clusterId);
                                    },
                                    _secondaryAction: function () {
                                        return _this.restoreSingleEverywhere(clusterId);
                                    },
                                    actionButtonText: 'Восстановить',
                                    secondaryActionButtonText: 'Восстановить везде'
                                };
                            }
                        }
                    });
                };

                ModalDeletedClusterListController.prototype.openRestoreConfirmation = function (e) {
                    var _this = this;
                    e.preventDefault();
                    this.ls.updateSelectedCount();
                    if (this.ls.selectedCount) {
                        this.$modal.open({
                            templateUrl: 'public/main/partials/modals/delete-restore.html',
                            controller: backOffice.controllers.DeleteRestoreController,
                            resolve: {
                                params: function () {
                                    return {
                                        title: 'Восстановить',
                                        items: _this.ls.getSelectedItems(),
                                        _action: function () {
                                            return _this.restoreToReport(_this.ls.getSelectedIds());
                                        },
                                        _secondaryAction: function () {
                                            return _this.restoreEverywhere(_this.ls.getSelectedIds());
                                        },
                                        actionButtonText: 'Восстановить',
                                        secondaryActionButtonText: 'Восстановить везде',
                                        alert: 'Вы действительно хотите восстановить эти статьи?'
                                    };
                                }
                            }
                        });
                    }
                };

                ModalDeletedClusterListController.prototype.restoreSingleToReport = function (clusterId) {
                    var _this = this;
                    return this.confirmation('Вы действительно хотите восстановить этот кластер в отчете?', 'Восстановить', function () {
                        return _this.restoreToReport([clusterId]);
                    });
                };

                ModalDeletedClusterListController.prototype.restoreSingleEverywhere = function (clusterId) {
                    var _this = this;
                    return this.confirmation('Вы действительно хотите восстановить этот кластер в отчете?', 'Восстановить', function () {
                        return _this.restoreEverywhere([clusterId]);
                    });
                };

                ModalDeletedClusterListController.prototype.restoreEverywhere = function (ids) {
                    var _this = this;
                    return this.clusterService.restoreEverywhere(ids).then(function () {
                        _this.ls.deleteSelectedItems();
                    });
                };

                ModalDeletedClusterListController.prototype.restoreToReport = function (ids) {
                    var _this = this;
                    return this.clusterService.restoreToReport(this.params.report.reportId, ids).then(function () {
                        _this.ls.deleteSelectedItems();
                    });
                };
                ModalDeletedClusterListController.$inject = ['$scope', 'params', 'ClusterService', '$modal', '$modalInstance', 'Confirmation'];
                return ModalDeletedClusterListController;
            })(controllers.common.ModalController);
            _cluster.ModalDeletedClusterListController = ModalDeletedClusterListController;
        })(controllers.cluster || (controllers.cluster = {}));
        var cluster = controllers.cluster;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (_article) {
            var DeletedInClusterArticleListController = (function (_super) {
                __extends(DeletedInClusterArticleListController, _super);
                function DeletedInClusterArticleListController($scope, $modalInstance, clusterArticleService, params, $modal, confirmation) {
                    var _this = this;
                    _super.call(this, $scope, $modalInstance);
                    this.clusterArticleService = clusterArticleService;
                    this.params = params;
                    this.$modal = $modal;
                    this.confirmation = confirmation;
                    this.itemList = {
                        totalCount: 0,
                        items: [],
                        isLoaded: false
                    };
                    this.ls = new backOffice.services.ListService(function () {
                        return _this.itemList;
                    });
                    this.getList(params.clusterId);
                }
                DeletedInClusterArticleListController.prototype.getList = function (clusterId) {
                    var _this = this;
                    this.clusterArticleService.getDeletedFromCluster(clusterId).then(function (itemList) {
                        itemList.isLoaded = true;
                        _this.itemList = itemList;
                    });
                };

                DeletedInClusterArticleListController.prototype.openArticle = function (articleId, article, e) {
                    var _this = this;
                    e.preventDefault();
                    var modal = this.$modal.open({
                        templateUrl: 'public/main/partials/article/display-modal.html',
                        controller: controllers.article.ModalArticleDisplayController,
                        size: 'lg',
                        resolve: {
                            params: function () {
                                return {
                                    articleId: articleId,
                                    article: article,
                                    report: _this.params.report,
                                    _action: function () {
                                        return _this.restoreSingleToCluster(articleId);
                                    },
                                    actionButtonText: 'Восстановить в кластере'
                                };
                            }
                        }
                    });
                    modal.result.then(function (message) {
                        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
                            _this.ls.deleteItemsFromList([articleId]);
                        }
                    }, function () {
                    });
                };

                DeletedInClusterArticleListController.prototype.openRestoreConfirmation = function (e) {
                    var _this = this;
                    e.preventDefault();
                    this.ls.updateSelectedCount();
                    if (this.ls.selectedCount) {
                        this.$modal.open({
                            templateUrl: 'public/main/partials/modals/delete-restore.html',
                            controller: backOffice.controllers.DeleteRestoreController,
                            resolve: {
                                params: function () {
                                    return {
                                        title: 'Восстановить',
                                        items: _this.ls.getSelectedItems(),
                                        _action: function () {
                                            return _this.restoreToCluster(_this.ls.getSelectedIds());
                                        },
                                        actionButtonText: 'Восстановить',
                                        alert: 'Вы действительно хотите восстановить эти статьи?'
                                    };
                                }
                            }
                        });
                    }
                };

                DeletedInClusterArticleListController.prototype.restoreSingleToCluster = function (articleId) {
                    var _this = this;
                    return this.confirmation('Вы действительно хотите восстановить эту статью в кластере?', 'Восстановить', function () {
                        return _this.restoreToCluster([articleId]);
                    });
                };
                DeletedInClusterArticleListController.prototype.restoreToCluster = function (ids) {
                    var _this = this;
                    return this.clusterArticleService.restoreToCluster(ids, this.params.clusterId).then(function () {
                        _this.ls.deleteSelectedItems();
                    });
                };
                DeletedInClusterArticleListController.$inject = ['$scope', '$modalInstance', 'ClusterArticleService', 'params', '$modal', 'Confirmation'];
                return DeletedInClusterArticleListController;
            })(controllers.common.ModalController);
            _article.DeletedInClusterArticleListController = DeletedInClusterArticleListController;
        })(controllers.article || (controllers.article = {}));
        var article = controllers.article;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (_cluster) {
            var ModalClusterDisplayController = (function (_super) {
                __extends(ModalClusterDisplayController, _super);
                function ModalClusterDisplayController($q, $scope, $modalInstance, $modal, clusterService, params, articleService, clusterArticleService, confirmation) {
                    var _this = this;
                    _super.call(this, $scope, $modalInstance, params);
                    this.$q = $q;
                    this.$modalInstance = $modalInstance;
                    this.$modal = $modal;
                    this.clusterService = clusterService;
                    this.params = params;
                    this.articleService = articleService;
                    this.clusterArticleService = clusterArticleService;
                    this.confirmation = confirmation;
                    this.isItemsLoaded = true;
                    this.pageSize = 100;
                    this.verifiedHidden = false;
                    this.itemList = {
                        totalCount: 0,
                        items: [],
                        isLoaded: false
                    };
                    this.listMoreFilter = {
                        emotion: 0,
                        source: 0,
                        sort: 0
                    };
                    this.isDeleting = false;
                    $scope.vm = this;
                    this.cluster = params.cluster;
                    this.ls = new backOffice.services.ListService(function () {
                        return _this.itemList;
                    });

                    this.getMainArticle(this.cluster.mainArticleId).then(function () {
                        _this.list(_this.cluster.mainArticleId);
                    });
                }
                ModalClusterDisplayController.prototype.refresh = function (e) {
                    e.preventDefault();
                    this.list(this.cluster.mainArticleId);
                };

                ModalClusterDisplayController.prototype.showError = function (message) {
                    this.fatalError = {
                        type: 'danger',
                        message: message
                    };
                };

                ModalClusterDisplayController.prototype.openTrash = function (e) {
                    var _this = this;
                    e.preventDefault();
                    this.$modal.open({
                        templateUrl: 'public/main/partials/article/deleted-list-modal.html',
                        controller: controllers.article.DeletedInClusterArticleListController,
                        size: 'lg',
                        resolve: {
                            params: function () {
                                return {
                                    clusterId: _this.cluster.id,
                                    report: _this.params.report
                                };
                            }
                        }
                    });
                };
                ModalClusterDisplayController.prototype.isSortedBy = function (sort) {
                    return this.listMoreFilter.sort === sort;
                };

                ModalClusterDisplayController.prototype.changeSort = function (sort, e) {
                    e.preventDefault();
                    this.listMoreFilter.sort = sort;
                    this.list(this.cluster.mainArticleId);
                };

                ModalClusterDisplayController.prototype.deleteConfirmation = function (e) {
                    var _this = this;
                    e.preventDefault();
                    this.$modal.open({
                        templateUrl: 'public/main/partials/modals/delete-restore.html',
                        controller: backOffice.controllers.DeleteRestoreController,
                        resolve: {
                            params: function () {
                                return {
                                    title: 'Удалить',
                                    items: _this.ls.getSelectedItems(),
                                    _action: function () {
                                        return _this.deleteFromCluster(_this.ls.getSelectedIds());
                                    },
                                    actionButtonText: 'Удалить',
                                    alert: 'Вы действительно хотите удалить эти статьи?'
                                };
                            }
                        }
                    });
                };

                ModalClusterDisplayController.prototype.deleteSingleArticleFromCluster = function (articleId) {
                    var _this = this;
                    return this.confirmation('Вы действительно хотите удалить эту статью из кластера?', 'Удалить', function () {
                        return _this.deleteFromCluster([articleId]);
                    });
                };

                ModalClusterDisplayController.prototype.deleteFromCluster = function (ids) {
                    var _this = this;
                    return this.clusterArticleService.deleteArticles(ids, this.cluster.id).then(function () {
                        _this.ls.deleteSelectedItems();
                    });
                };

                ModalClusterDisplayController.prototype.display = function () {
                    var _this = this;
                    return this.clusterService.getById(this.params.clusterId).then(function (cluster) {
                        if (!cluster || cluster === "null") {
                            var errorText = 'Невозможно загрузить данные кластера';
                            _this.showError(errorText);
                            return _this.$q.reject(errorText);
                        }
                        _this.cluster = cluster;
                        _this.getMainArticle(cluster.mainArticleId);
                        return cluster.mainArticleId;
                    });
                };

                ModalClusterDisplayController.prototype.getMainArticle = function (id) {
                    var _this = this;
                    return this.articleService.getItemById(id).then(function (article) {
                        _this.mainArticle = article;
                    });
                };

                ModalClusterDisplayController.prototype.deleteSingleInReport = function () {
                    var _this = this;
                    if (confirm('Вы действительно хотите удалить эту статью в отчете?')) {
                        this.isDeleting = true;
                        return this.clusterService.deleteFromReport([this.params.clusterId], this.params.report.reportId).then(function () {
                            _this.isDeleting = false;
                            _this.$modalInstance.close('deleteSingleInReport');
                        });
                    }
                };

                ModalClusterDisplayController.prototype.deleteSingleEverywhere = function () {
                    var _this = this;
                    if (confirm('Вы действительно хотите удалить эту статью везде?')) {
                        this.isDeleting = true;
                        this.clusterService.deleteEverywhere([this.params.clusterId]).then(function () {
                            _this.isDeleting = false;
                            _this.$modalInstance.close('deleteSingleEverywhere');
                        });
                    }
                };

                ModalClusterDisplayController.prototype.isNegative = function () {
                    return this.listMoreFilter.emotion === 2;
                };

                ModalClusterDisplayController.prototype.changeEmotion = function (e) {
                    e.preventDefault();
                    this.listMoreFilter.emotion = this.listMoreFilter.emotion === 2 ? 0 : 2;
                    this.list(this.cluster.mainArticleId);
                };

                ModalClusterDisplayController.prototype.changeSource = function (source, e) {
                    e.preventDefault();
                    this.listMoreFilter.source = source;
                    this.list(this.cluster.mainArticleId);
                };

                ModalClusterDisplayController.prototype.isSource = function (source) {
                    return source === this.listMoreFilter.source;
                };

                ModalClusterDisplayController.prototype.openEdit = function (clusterId, cluster, e) {
                    var _this = this;
                    e.preventDefault();
                    this.$modal.open({
                        templateUrl: 'public/main/partials/cluster/edit-modal.html',
                        controller: backOffice.controllers.cluster.ModalClusterEditController,
                        size: 'lg',
                        resolve: {
                            params: function () {
                                return {
                                    clusterId: clusterId,
                                    report: _this.params.report,
                                    cluster: cluster
                                };
                            }
                        }
                    });
                };

                ModalClusterDisplayController.prototype.openArticle = function (articleId, article, e) {
                    var _this = this;
                    e.preventDefault();
                    var modal = this.$modal.open({
                        templateUrl: 'public/main/partials/article/display-modal.html',
                        controller: controllers.article.ModalArticleDisplayController,
                        size: 'lg',
                        resolve: {
                            params: function () {
                                return {
                                    articleId: articleId,
                                    article: article,
                                    _action: function () {
                                        return _this.deleteSingleArticleFromCluster(articleId);
                                    },
                                    actionButtonText: 'Удалить в кластере',
                                    report: _this.params.report
                                };
                            }
                        }
                    });
                    modal.result.then(function (message) {
                        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
                            _this.ls.deleteItemsFromList([articleId]);
                        }
                    }, function () {
                    });
                };

                ModalClusterDisplayController.prototype.list = function (mainArticleId) {
                    var _this = this;
                    this.isItemsLoaded = false;
                    var period = this.params.report.period;
                    var emotion = this.listMoreFilter.emotion;
                    var source = this.listMoreFilter.source;
                    var sort = this.listMoreFilter.sort;
                    return this.clusterArticleService.getFilteredListForArticles(mainArticleId, period, emotion, source, sort, this.pageSize, 1).then(function (itemList) {
                        _this.isItemsLoaded = true;
                        _this.itemList = itemList;
                        _this.clusterArticleService.getCounters(_this.params.clusterId, mainArticleId, period, true).then(function (counters) {
                            _this.counters = counters;
                        });
                        _this.clusterArticleService.getDeletedCount(_this.params.clusterId, period).then(function (deletedCount) {
                            _this.ls.deletedCount = parseInt(deletedCount);
                        });
                    });
                };
                ModalClusterDisplayController.$inject = ['$q', '$scope', '$modalInstance', '$modal', 'ClusterService', 'params', 'ArticleService', 'ClusterArticleService', 'Confirmation'];
                return ModalClusterDisplayController;
            })(controllers.DeleteRestoreController);
            _cluster.ModalClusterDisplayController = ModalClusterDisplayController;
        })(controllers.cluster || (controllers.cluster = {}));
        var cluster = controllers.cluster;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (_cluster) {
            var ModalClusterEditController = (function (_super) {
                __extends(ModalClusterEditController, _super);
                function ModalClusterEditController($q, $scope, $modalInstance, clusterService, params, imageHelper, clusterArticleService, routerService) {
                    var _this = this;
                    _super.call(this, $scope, $modalInstance);
                    this.$q = $q;
                    this.$modalInstance = $modalInstance;
                    this.clusterService = clusterService;
                    this.params = params;
                    this.imageHelper = imageHelper;
                    this.clusterArticleService = clusterArticleService;
                    this.routerService = routerService;
                    this.isItemsLoaded = true;
                    this.itemList = {
                        totalCount: 0,
                        items: [],
                        isLoaded: false
                    };
                    this.progress = 0;
                    this.isSettingMain = false;
                    this.isSettingImage = false;
                    this.imagelistPage = 1;
                    this.isDeleting = false;
                    this.isUploading = false;
                    this.hasNewImage = false;
                    $scope.vm = this;
                    $scope.$watch("vm.imageData", function (newVal) {
                        if (newVal)
                            _this.cluster.imageId = newVal.id;
                    });
                    this.jsAction = routerService.action("Image", "UploadImage").url;
                    this.originalCluster = angular.copy(params.cluster);
                    this.cluster = params.cluster;

                    this.initCluster(this.cluster);
                    this.getRelevantList(this.cluster);
                }
                ModalClusterEditController.prototype.initCluster = function (cluster) {
                    var _this = this;
                    if (!cluster || (cluster === "null")) {
                        var errorText = 'Невозможно загрузить данные кластера';
                        this.showError(errorText);
                        return this.$q.reject(errorText);
                    }
                    this.cluster = cluster;

                    this.originalCluster = angular.copy(this.cluster);
                    this.imageData = { id: cluster.imageId, url: cluster.imageUrl };
                    this.clusterService.getImages(this.params.clusterId).then(function (imageList) {
                        _this.imageList = imageList;
                        _this.imageList.pages = _this.getPages(imageList.items, 7);
                        var currentImage = _.find(_this.imageList.items, function (item) {
                            return _this.imageHelper.getIdFromUrl(_this.cluster.imageUrl) === _this.imageHelper.getIdFromUrl(item.url);
                        });
                        if (currentImage)
                            currentImage.selected = true;
                    });
                    return cluster;
                };

                ModalClusterEditController.prototype.edit = function () {
                    return this.clusterService.getById(this.params.clusterId).then(this.initCluster);
                };

                ModalClusterEditController.prototype.onFileChange = function () {
                    this.isUploading = true;
                };

                ModalClusterEditController.prototype.uploadSuccess = function (response) {
                    this.hasNewImage = true;
                    this.imageData = response;
                    this.isUploading = false;
                };

                ModalClusterEditController.prototype.isPrevImageListPageDisabled = function () {
                    if (this.imageList)
                        return this.imagelistPage === 0;
                    return false;
                };

                ModalClusterEditController.prototype.isNextImageListPageDisabled = function () {
                    if (this.imageList && this.imageList.pages)
                        return this.imagelistPage === (this.imageList.pages.length - 1);
                    return false;
                };

                ModalClusterEditController.prototype.prevImgListPage = function () {
                    if (!this.isPrevImageListPageDisabled())
                        this.imagelistPage = this.imagelistPage - 1;
                };

                ModalClusterEditController.prototype.nextImgListPage = function () {
                    if (!this.isNextImageListPageDisabled())
                        this.imagelistPage = this.imagelistPage + 1;
                };

                ModalClusterEditController.prototype.setImageButtonText = function () {
                    return this.isSettingImage ? "Изображение обновляется" : "Сохранить изменения";
                };

                ModalClusterEditController.prototype.changePictureText = function () {
                    return this.isUploading ? 'Идет загрузка ...' : 'Сменить картинку';
                };

                ModalClusterEditController.prototype.getPages = function (items, pageSize) {
                    var pages = [];
                    var itms = items;
                    var pagesCount = Math.floor(items.length / pageSize) + ((items.length % pageSize) > 0 ? 1 : 0);
                    for (var i = 0; i < pagesCount; i++) {
                        var page = {
                            index: i,
                            images: _.take(itms, pageSize)
                        };
                        itms = _.drop(itms, pageSize);
                        pages.push(page);
                    }
                    return pages;
                };

                ModalClusterEditController.prototype.selectImage = function (img) {
                    _.forEach(this.imageList.items, function (item) {
                        item.selected = false;
                    });
                    img.selected = !img.selected;
                    this.imageData = { id: img.imageId, url: img.url };
                };

                ModalClusterEditController.prototype.showError = function (message) {
                    this.fatalError = {
                        type: 'danger',
                        message: message
                    };
                };

                ModalClusterEditController.prototype.isSaveButtonDisabled = function () {
                    return !this.imageData || ((this.cluster.imageId === this.originalCluster.imageId) && (this.originalCluster.title === this.cluster.title)) || this.isSettingImage;
                };

                ModalClusterEditController.prototype.getRelevantList = function (cluster) {
                    var _this = this;
                    this.isItemsLoaded = false;
                    this.clusterArticleService.getRelevantList(this.cluster.mainArticleId).then(function (itemsList) {
                        _this.isItemsLoaded = true;
                        _this.itemList = itemsList;
                        var mainArticle = _.find(_this.itemList.items, function (item) {
                            return item.id === cluster.mainArticleId;
                        });
                        if (mainArticle)
                            mainArticle.selected = true;
                    });
                };

                ModalClusterEditController.prototype.setAsMain = function (article) {
                    var _this = this;
                    article.isSettingMain = true;
                    _.forEach(this.itemList.items, function (item) {
                        item.selected = false;
                    });
                    article.selected = true;
                    this.clusterService.setMainArticle(this.params.clusterId, article.id).then(function () {
                        _this.isSettingMain = false;
                        article.isSettingMain = false;
                    });
                };

                ModalClusterEditController.prototype.deleteSingleInReport = function () {
                    var _this = this;
                    if (confirm('Вы действительно хотите удалить эту статью в отчете?')) {
                        this.isDeleting = true;
                        return this.clusterService.deleteFromReport([this.params.clusterId], this.params.report.reportId).then(function () {
                            _this.isDeleting = false;
                            _this.$modalInstance.close('deleteSingleInReport');
                        });
                    }
                };

                ModalClusterEditController.prototype.deleteSingleEverywhere = function () {
                    var _this = this;
                    if (confirm('Вы действительно хотите удалить эту статью везде?')) {
                        this.isDeleting = true;
                        this.clusterService.deleteEverywhere([this.params.clusterId]).then(function () {
                            _this.isDeleting = false;
                            _this.$modalInstance.close('deleteSingleEverywhere');
                        });
                    }
                };

                ModalClusterEditController.prototype.setImage = function () {
                    var _this = this;
                    this.cluster.imageId = this.imageData.id;

                    if (this.imageData.id !== this.originalCluster.imageId)
                        this.setImageBase({ action: this.clusterService.setImage, ref: this.clusterService }, this.cluster.id, this.imageData.id);
                    this.clusterService.setTitle(this.cluster.id, this.cluster.title).then(function () {
                        _this.originalCluster.title = _this.cluster.title;
                    });
                };
                ModalClusterEditController.prototype.setImageBase = function (saveImageFn, id, imageId) {
                    var _this = this;
                    this.isSettingImage = true;
                    saveImageFn.action.call(saveImageFn.ref, id, imageId).then(function () {
                        _this.progress = 0;
                        _this.originalCluster.imageId = _this.cluster.imageId;
                        _this.isSettingImage = false;
                        _this.hasNewImage = false;
                    });
                };
                ModalClusterEditController.$inject = ['$q', '$scope', '$modalInstance', 'ClusterService', 'params', 'ImageHelper', 'ClusterArticleService', 'RouterService'];
                return ModalClusterEditController;
            })(controllers.common.ModalController);
            _cluster.ModalClusterEditController = ModalClusterEditController;
        })(controllers.cluster || (controllers.cluster = {}));
        var cluster = controllers.cluster;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        var ClusterListController = (function (_super) {
            __extends(ClusterListController, _super);
            function ClusterListController($scope, clusterService, reportQueueService, $state, imageHelper, configService, $modal, $window, nav, confirmation) {
                _super.call(this, $scope, configService, reportQueueService, $state);
                this.$scope = $scope;
                this.clusterService = clusterService;
                this.reportQueueService = reportQueueService;
                this.$state = $state;
                this.imageHelper = imageHelper;
                this.configService = configService;
                this.$modal = $modal;
                this.$window = $window;
                this.nav = nav;
                this.confirmation = confirmation;
                this.collapsed = false;
                this.counters = { totalCount: 0, verifiedCount: 0 };
                this.getClusters();
            }
            ClusterListController.prototype.isClustersDisabled = function () {
                if (!this.report || !this.report.type)
                    return false;
                return this.report.type === 1;
            };

            ClusterListController.prototype.isArticlesDisabled = function () {
                if (!this.report || !this.report.type)
                    return false;
                return this.report.type === 2;
            };

            ClusterListController.prototype.showError = function (message) {
                this.$window.alert(message);
            };

            ClusterListController.prototype.openTrash = function (e) {
                var _this = this;
                e.preventDefault();
                var modal = this.$modal.open({
                    templateUrl: 'public/main/partials/cluster/deleted-list-modal.html',
                    controller: controllers.cluster.ModalDeletedClusterListController,
                    size: 'lg',
                    resolve: {
                        params: function () {
                            return {
                                report: _this.report
                            };
                        }
                    }
                });
            };

            ClusterListController.prototype.getClusters = function () {
                var _this = this;
                this.reportQueueService.getReport().then(function (report) {
                    _this.reportId = report.reportId;
                    _this.report = report;
                    _this.isItemsLoaded = false;
                    var period = _this.report.period;
                    _this.clusterService.getByReport(_this.report.reportId, period, _this.verifiedHidden, _this.pageSize, 1).then(function (itemList) {
                        _this.isItemsLoaded = true;
                        _this.itemList = itemList;
                        _this.ls.updateSelectedCount();
                        _this.clusterService.getCounters(_this.report.reportId, period).then(function (counters) {
                            _this.counters = counters;
                        });
                        _this.clusterService.getDeletedFromReportCount(_this.report.reportId, period).then(function (deletedCount) {
                            _this.ls.deletedCount = parseInt(deletedCount);
                        });
                    });
                    return report;
                }).then(function (report) {
                    return _this.loadConfigurations(report.reportId);
                });
            };

            ClusterListController.prototype.toggleCollapsed = function () {
                this.collapsed = !this.collapsed;
                console.log("collapsed: " + this.collapsed);
            };

            ClusterListController.prototype.img = function (src) {
                return this.imageHelper.getImageUrl(src, 100, 100, 6);
            };

            ClusterListController.prototype.isClusterListEmpty = function () {
                return this.itemList.totalCount === 0;
            };

            ClusterListController.prototype.openArticle = function (clusterId, cluster, e) {
                var _this = this;
                e.preventDefault();
                this.$modal.open({
                    templateUrl: 'public/main/partials/cluster/display-modal.html',
                    controller: backOffice.controllers.cluster.ModalClusterDisplayController,
                    size: 'lg',
                    resolve: {
                        params: function () {
                            return {
                                clusterId: clusterId,
                                report: _this.report,
                                cluster: cluster,
                                _action: function () {
                                    return _this.deleteSingleFromReport(clusterId);
                                },
                                _secondaryAction: function () {
                                    return _this.deleteSingleEverywhere(clusterId);
                                },
                                actionButtonText: 'Удалить в отчете',
                                secondaryActionButtonText: 'Удалить везде'
                            };
                        }
                    }
                });
            };

            ClusterListController.prototype.deleteSingleFromReport = function (clusterId) {
                var _this = this;
                return this.confirmation('Вы действительно хотите удалить этот кластер из отчета?', 'Удалить', function () {
                    return _this.deleteFromReport([clusterId]);
                });
            };

            ClusterListController.prototype.deleteSingleEverywhere = function (clusterId) {
                var _this = this;
                return this.confirmation('Вы действительно хотите удалить этот кластер везде?', 'Удалить', function () {
                    return _this.deleteEverywhere([clusterId]);
                });
            };

            ClusterListController.prototype.openEdit = function (clusterId, cluster, e) {
                var _this = this;
                e.preventDefault();
                this.$modal.open({
                    templateUrl: 'public/main/partials/cluster/edit-modal.html',
                    controller: backOffice.controllers.cluster.ModalClusterEditController,
                    size: 'lg',
                    resolve: {
                        params: function () {
                            return {
                                clusterId: clusterId,
                                report: _this.report,
                                cluster: cluster
                            };
                        }
                    }
                });
            };

            ClusterListController.prototype.deleteConfirmation = function (e) {
                var _this = this;
                e.preventDefault();
                this.$modal.open({
                    templateUrl: 'public/main/partials/modals/delete-restore.html',
                    controller: backOffice.controllers.DeleteRestoreController,
                    resolve: {
                        params: function () {
                            return {
                                title: 'Удалить события',
                                items: _this.ls.getSelectedItems(),
                                _action: function () {
                                    return _this.deleteFromReport(_this.ls.getSelectedIds());
                                },
                                _secondaryAction: function () {
                                    return _this.deleteEverywhere(_this.ls.getSelectedIds());
                                },
                                actionButtonText: 'Удалить',
                                secondaryActionButtonText: 'Удалить везде',
                                alert: 'Вы действительно хотите удалить эти события?'
                            };
                        }
                    }
                });
            };
            ClusterListController.prototype.loadMore = function () {
                this.loadMoreBase(this.reportId, { action: this.clusterService.getByReport, ref: this.clusterService });
            };

            ClusterListController.prototype.loadMoreBase = function (id, serviceFunction) {
                var _this = this;
                this.isItemsLoaded = false;
                var action = function (period) {
                    serviceFunction.action.call(serviceFunction.ref, id, period, _this.verifiedHidden, _this.pageSize, ++_this.currentPage).then(function (itemList) {
                        _this.itemList.items.push.apply(_this.itemList.items, itemList.items);
                        _this.isItemsLoaded = true;
                    });
                };
                if (this.report) {
                    action(this.report.period);
                } else {
                    this.reportQueueService.getReport().then(function (report) {
                        _this.report = report;
                        action(_this.report.period);
                    });
                }
            };

            ClusterListController.prototype.deleteEverywhere = function (selectedIds) {
                var _this = this;
                return this.clusterService.deleteEverywhere(selectedIds).then(function () {
                    _this.ls.deleteSelectedItems();
                });
            };

            ClusterListController.prototype.deleteFromReport = function (selectedIds) {
                var _this = this;
                return this.clusterService.deleteFromReport(selectedIds, this.reportId).then(function () {
                    _this.ls.deleteSelectedItems();
                });
            };

            ClusterListController.prototype.refresh = function (e) {
                e.preventDefault();
                this.getClusters();
            };
            ClusterListController.$inject = [
                '$scope', 'ClusterService',
                'ReportQueueService', '$state', 'ImageHelper', 'ConfigService', '$modal', '$window', 'NavService', 'Confirmation'];
            return ClusterListController;
        })(controllers.common.ListController);
        controllers.ClusterListController = ClusterListController;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
var backOffice;
(function (backOffice) {
    (function (controllers) {
        (function (_article) {
            var DeletedArticleListController = (function (_super) {
                __extends(DeletedArticleListController, _super);
                function DeletedArticleListController($scope, reportQueueService, articleService, $modal, params, $modalInstance, confirmation) {
                    var _this = this;
                    _super.call(this, $scope, $modalInstance);
                    this.$scope = $scope;
                    this.reportQueueService = reportQueueService;
                    this.articleService = articleService;
                    this.$modal = $modal;
                    this.params = params;
                    this.$modalInstance = $modalInstance;
                    this.confirmation = confirmation;
                    this.itemList = {
                        totalCount: 0,
                        items: [],
                        isLoaded: false
                    };
                    this.deletedCount = 0;
                    this.selectedCount = 0;
                    this.ls = new backOffice.services.ListService(function () {
                        return _this.itemList;
                    });
                    this.getList();
                }
                DeletedArticleListController.prototype.getList = function () {
                    var _this = this;
                    this.reportQueueService.getReport().then(function (report) {
                        _this.articleService.getDeletedFromReport(report.reportId, report.period, 100, 1).then(function (itemList) {
                            itemList.isLoaded = true;
                            _this.itemList = itemList;
                        });
                    });
                };

                DeletedArticleListController.prototype.openArticle = function (articleId, article, e) {
                    var _this = this;
                    e.preventDefault();
                    var modal = this.$modal.open({
                        templateUrl: 'public/main/partials/article/display-modal.html',
                        controller: controllers.article.ModalArticleDisplayController,
                        size: 'lg',
                        resolve: {
                            params: function () {
                                return {
                                    articleId: articleId,
                                    article: article,
                                    report: _this.params.report,
                                    _action: function () {
                                        return _this.restoreSingleToReport(articleId);
                                    },
                                    _secondaryAction: function () {
                                        return _this.restoreSingleEverywhere(articleId);
                                    },
                                    actionButtonText: 'Восстановить',
                                    secondaryActionButtonText: 'Восстановить везде'
                                };
                            }
                        }
                    });
                    modal.result.then(function (message) {
                        if (_.contains(['deleteSingleInReport', 'deleteSingleEverywhere'], message)) {
                            _this.ls.deleteItemsFromList([articleId]);
                        }
                    }, function () {
                    });
                };

                DeletedArticleListController.prototype.openRestoreConfirmation = function (e) {
                    var _this = this;
                    e.preventDefault();
                    this.ls.updateSelectedCount();
                    if (this.ls.selectedCount) {
                        this.$modal.open({
                            templateUrl: 'public/main/partials/modals/delete-restore.html',
                            controller: backOffice.controllers.DeleteRestoreController,
                            resolve: {
                                params: function () {
                                    return {
                                        title: 'Восстановить',
                                        items: _this.ls.getSelectedItems(),
                                        _action: function () {
                                            return _this.restoreToReport(_this.ls.getSelectedIds());
                                        },
                                        _secondaryAction: function () {
                                            return _this.restoreEverywhere(_this.ls.getSelectedIds());
                                        },
                                        actionButtonText: 'Восстановить',
                                        secondaryActionButtonText: 'Восстановить везде',
                                        alert: 'Вы действительно хотите восстановить эти статьи?'
                                    };
                                }
                            }
                        });
                    }
                };

                DeletedArticleListController.prototype.restoreSingleToReport = function (articleId) {
                    var _this = this;
                    return this.confirmation('Вы действительно хотите восстановить эту статью в отчете?', 'Восстановить', function () {
                        return _this.restoreToReport([articleId]);
                    });
                };
                DeletedArticleListController.prototype.restoreSingleEverywhere = function (articleId) {
                    var _this = this;
                    return this.confirmation('Вы действительно хотите восстановить эту статью везде?', 'Восстановить', function () {
                        return _this.restoreEverywhere([articleId]);
                    });
                };
                DeletedArticleListController.prototype.restoreEverywhere = function (selectedIds) {
                    var _this = this;
                    return this.articleService.restoreEverywhere(selectedIds).then(function () {
                        _this.ls.deleteSelectedItems();
                    });
                };

                DeletedArticleListController.prototype.restoreToReport = function (selectedIds) {
                    var _this = this;
                    return this.articleService.restoreToReport(selectedIds, this.params.reportId).then(function () {
                        _this.ls.deleteSelectedItems();
                    });
                };
                DeletedArticleListController.$inject = ['$scope', 'ReportQueueService', 'ArticleService', '$modal', 'params', '$modalInstance', 'Confirmation'];
                return DeletedArticleListController;
            })(controllers.common.ModalController);
            _article.DeletedArticleListController = DeletedArticleListController;
        })(controllers.article || (controllers.article = {}));
        var article = controllers.article;
    })(backOffice.controllers || (backOffice.controllers = {}));
    var controllers = backOffice.controllers;
})(backOffice || (backOffice = {}));
app.controller('BreadcrumbController', backOffice.controllers.BreadcrumbController);
app.controller('NavController', backOffice.controllers.NavController);

app.directive('highlightText', backOffice.directives.highlightText);
app.directive('objectid', backOffice.directives.objectid);
app.directive('highlightControl', backOffice.directives.highlightControl);
app.directive('errorAlert', backOffice.directives.errorAlert);
app.directive('fileUpload', ['$http', backOffice.directives.fileUpload]);
app.directive('infoBar', ['$templateCache', '$compile', '$http', '$state', backOffice.directives.infoBar]);
app.directive('itemInfoBar', ['$templateCache', '$compile', '$http', '$state', backOffice.directives.itemInfoBar]);
app.directive('lazyImageSrc', ['$window', 'ImageHelper', backOffice.directives.lazyImage]);
app.directive('listToolbar', ['$window', '$location', backOffice.directives.listToolbar]);
app.directive('stickyBar', ['$window', backOffice.directives.stickyBar]);

app.directive('autoFillSync', [
    '$timeout', function ($timeout) {
        return {
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                scope.$on("autofill:update", function () {
                    ngModel.$setViewValue(element.val());
                });
            }
        };
    }]);
app.service('Confirmation', [
    '$modal', function ($modal) {
        return function (message, okText, action) {
            var modalInstance = $modal.open({
                templateUrl: 'public/main/partials/modals/confirmation.html',
                controller: backOffice.controllers.shared.ModalConfirmationController,
                resolve: {
                    params: function () {
                        return {
                            message: message,
                            okText: okText,
                            cancelText: 'Отменить'
                        };
                    }
                }
            });
            return modalInstance.result.then(function () {
                return action();
            });
        };
    }]);
app.factory('myHttpResponseInterceptor', [
    '$q', '$location', '$injector', function ($q, $location, $injector) {
        return {
            'responseError': function (rejection) {
                if (rejection.status === 401) {
                    if (!backOffice.Global.isLoginOpen) {
                        var $modal = $injector.get('$modal');
                        backOffice.Global.isLoginOpen = true;
                        var modalInstance = $modal.open({
                            templateUrl: 'public/main/partials/modals/login.html',
                            controller: backOffice.controllers.ModalSecurityController,
                            size: 'sm'
                        });
                        modalInstance.result.then(function () {
                            var $state = $injector.get('$state');
                            $state.transitionTo($state.current, $state.current.params, { reload: true, inherit: true, notify: true });
                            backOffice.Global.isLoginOpen = false;
                        }, function () {
                            backOffice.Global.isLoginOpen = false;
                        });
                    }
                }
                return $q.reject(rejection);
            }
        };
    }]);

app.config([
    '$stateProvider', '$locationProvider', '$httpProvider', '$parseProvider', 'stateHelperProvider',
    function ($stateProvider, $locationProvider, $httpProvider, $parseProvider, stateHelper) {
        $parseProvider.unwrapPromises(true);
        $httpProvider.interceptors.push('myHttpResponseInterceptor');
        var vw = globalFunctions.vw;
        var stateConfig = function () {
            var breadcrumbsHelper = backOffice.helpers.BreadcrumbHelper;
            $stateProvider.state('home', {
                url: '',
                templateUrl: vw('partials/home.html'),
                controller: [
                    '$state', function ($state) {
                        $state.go('otk.reports');
                    }],
                breadcrumbs: function () {
                    return [];
                }
            }).state('home2', {
                url: '/',
                controller: [
                    '$state', function ($state) {
                        $state.go('otk.reports');
                    }],
                templateUrl: vw('partials/home.html'),
                breadcrumbs: function () {
                    return [];
                }
            }).state('clustersTrash', {
                url: '/reports/{reportId}/deleted-clusters',
                controller: backOffice.controllers.ClusterListController,
                action: 'getClustersTrash',
                templateUrl: vw('partials/shared/trash-list.html'),
                breadcrumbs: function (stateParams) {
                    var bh = new breadcrumbsHelper(stateParams);
                    return [bh.clustersTrash(true)];
                }
            }).state('pod', {
                url: '/pod',
                controller: backOffice.controllers.ReportController,
                templateUrl: vw('partials/pod/main.html'),
                breadcrumbs: function (stateParams) {
                    var bh = new breadcrumbsHelper(stateParams);
                    return [bh.pod(true)];
                }
            }).state('logOff', {
                url: '/logoff',
                controller: backOffice.controllers.SecurityController,
                action: 'logOff'
            }).state('login', {
                url: '/login',
                controller: backOffice.controllers.SecurityController,
                templateUrl: vw('partials/security/login.html')
            }).state('monitor', {
                url: '/monitor',
                templateUrl: vw('partials/monitor/main.html'),
                breadcrumbs: function (stateParams) {
                    var bh = new breadcrumbsHelper(stateParams);
                    return [bh.monitor(true)];
                }
            });

            stateHelper.setNestedState({
                name: 'otk',
                url: '/otk',
                templateUrl: vw('partials/otk/main.html'),
                breadcrumbs: function (stateParams) {
                    var bh = new breadcrumbsHelper(stateParams);
                    return [bh.otk(true)];
                },
                children: [
                    {
                        name: 'configMessages',
                        url: '/configs/{configId}/R{reportId}',
                        controller: backOffice.controllers.article.ArticleListController,
                        templateUrl: vw('partials/otk/configs/messages.html'),
                        breadcrumbs: function (stateParams) {
                            var bh = new breadcrumbsHelper(stateParams);
                            return [bh.otk(false), bh.otkConfigs(false), bh.otkConfigReports(false), bh.otkConfigArticles(true)];
                        }
                    },
                    {
                        name: 'configsByReport',
                        url: '/configs/by-report/{reportId}',
                        controller: backOffice.controllers.ConfigController,
                        templateUrl: vw('partials/otk/configs/configs.html'),
                        breadcrumbs: function (stateParams) {
                            var bh = new breadcrumbsHelper(stateParams);
                            return [bh.otk(false), bh.otkConfigs(false), bh.otConfigsByReport(true)];
                        }
                    },
                    {
                        name: 'configReports',
                        url: '/configs/all/{configId}',
                        controller: backOffice.controllers.ReportController,
                        templateUrl: vw('partials/otk/configs/reports.html'),
                        breadcrumbs: function (stateParams) {
                            var bh = new breadcrumbsHelper(stateParams);
                            return [bh.otk(false), bh.otkConfigs(false), bh.otkConfigReports(true)];
                        }
                    },
                    {
                        name: 'configs',
                        url: '/configs/all',
                        controller: backOffice.controllers.ConfigController,
                        templateUrl: vw('partials/otk/configs/configs.html'),
                        breadcrumbs: function (stateParams) {
                            var bh = new breadcrumbsHelper(stateParams);
                            return [bh.otk(false), bh.otkConfigs(true)];
                        }
                    },
                    {
                        name: 'reports',
                        url: '/queue',
                        controller: backOffice.controllers.ReportQueueController,
                        action: 'getQueueStatistic',
                        templateUrl: vw('partials/otk/queue/reportsQueue.html'),
                        breadcrumbs: function (stateParams) {
                            var bh = new breadcrumbsHelper(stateParams);
                            return [bh.otk(false), bh.otkReports(true)];
                        }
                    },
                    {
                        name: 'report',
                        url: '/queue/top',
                        controller: backOffice.controllers.ReportController,
                        action: 'getReportInfo',
                        templateUrl: vw('partials/otk/queue/report.html'),
                        breadcrumbs: function (stateParams) {
                            var bh = new breadcrumbsHelper(stateParams);
                            return [bh.otk(false), bh.otkReports(false), bh.otkReport(true)];
                        },
                        children: [
                            {
                                name: 'articles',
                                url: '/articles-{vs}',
                                controller: backOffice.controllers.article.ArticleListController,
                                action: 'getArticleList',
                                templateUrl: vw('partials/article/list.html'),
                                breadcrumbs: function (stateParams) {
                                    var bh = new breadcrumbsHelper(stateParams);
                                    return [bh.otk(false), bh.otkReports(false), bh.otkReport(false), bh.otkReportArticles(true)];
                                }
                            },
                            {
                                name: 'clusters',
                                url: '/clusters-{vs}',
                                controller: backOffice.controllers.ClusterListController,
                                action: 'getClusters',
                                templateUrl: vw('partials/cluster/list.html'),
                                breadcrumbs: function (stateParams) {
                                    var bh = new breadcrumbsHelper(stateParams);
                                    return [bh.otk(false), bh.otkReports(false), bh.otkReport(false), bh.otkReportClusters(true)];
                                }
                            }
                        ]
                    },
                    {
                        "name": 'reportConfigs',
                        url: '/queue/R{reportId}-{vs}/configs',
                        controller: backOffice.controllers.ConfigController,
                        templateUrl: vw('partials/otk/queue/configs.html'),
                        breadcrumbs: function (stateParams) {
                            var bh = new breadcrumbsHelper(stateParams);
                            return [bh.otk(false), bh.otkReports(false), bh.otkReport(false), bh.otkReportConfigs(true)];
                        }
                    }
                ]
            });
            stateHelper.setNestedState({
                name: 'settings',
                url: '/settings',
                templateUrl: vw('partials/settings/main.html'),
                breadcrumbs: function (stateParams) {
                    var bh = new breadcrumbsHelper(stateParams);
                    return [bh.settings(true)];
                },
                children: [
                    {
                        name: 'general',
                        url: '/general',
                        templateUrl: vw('partials/settings/general/main.html'),
                        breadcrumbs: function (stateParams) {
                            var bh = new breadcrumbsHelper(stateParams);
                            return [bh.settings(false), bh['settings.queue'](true)];
                        }
                    },
                    {
                        name: 'queue',
                        url: '/queue',
                        controller: backOffice.controllers.ReportQueueController,
                        action: 'getSettingsData',
                        templateUrl: vw('partials/settings/queue/main.html'),
                        breadcrumbs: function (stateParams) {
                            var bh = new breadcrumbsHelper(stateParams);
                            return [bh.settings(false), bh['settings.queue'](true)];
                        }
                    }
                ]
            });
        };
        stateConfig();
        $httpProvider.interceptors.push([
            "$q", "$injector", function ($q, $injector) {
                return {
                    'responseError': function (rejection) {
                        var rootScope = $injector.get('$rootScope');
                        rootScope.error = rejection.data;
                        return $q.reject(rejection);
                    }
                };
            }]);
        $locationProvider.html5Mode(false);
    }]);
//# sourceMappingURL=app.js.map
